#include "stdafx.h"
#include "Project.h"
//#include "Player.h"

namespace basedx11{


	Enemy::Enemy(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
		) :
		GameObject(StagePtr),
		m_StartPos(Position),
		m_StartRot(Rotation),
		m_StartScal(Position),
		E_velo(1.0f, 0.0f, 1.0f)
	{
	}

	Enemy::~Enemy(){}

	void Enemy::OnCreate()
	{

		EnemyHP = 1;
		Interval = 0;

		auto PtrTransform = AddComponent<Transform>();

		PtrTransform->SetRotation(m_StartPos);
		PtrTransform->SetScale(0.3f, 0.3f, 0.3f);
		PtrTransform->SetPosition(m_StartPos);

		//操舵系のコンポーネントをつける場合はRigidbodyをつける
		auto PtrRegid = AddComponent<Rigidbody>();
		auto PtrObb = AddComponent<CollisionObb>();

		
		//重力をつける
		auto PtrGravity = AddComponent<Gravity>();
		//最下地点
		PtrGravity->SetBaseY(0.27f);

		auto SeekPtr = AddComponent<SeekSteering>();
		//SeekPtr->SetUpdateActive(false);
		SeekPtr->SetUpdateActive(true);

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_DODECAHEDRON");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_DODECAHEDRON");
		PtrDraw->SetTextureResource(L"ENEMY_TX");
		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetDrawActive(true);
		SetDrawLayer(-2);
		SetAlphaActive(true);

		//エネミーの行動予約(周期)
		TimeTable_Enemy = { 0, 1, 1, 1 };

		auto Group = GetStage()->GetSharedObjectGroup(L"EnemyGroup");
		Group->IntoGroup(GetThis<Enemy>());
		

		//BGMの登録
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"Dameg");
		pMultiSoundEffect->AddAudioResource(L"Lightning");
		pMultiSoundEffect->AddAudioResource(L"Soner");

		//ステートマシンの構築
		m_StateMachine = make_shared< StateMachine<Enemy> >(GetThis<Enemy>());
		//最初のステートをDefaultStateに設定
		m_StateMachine->SetCurrentState(Enemy_DefaultState::Instance());
		//DefaultStateの初期化実行を行う
		m_StateMachine->GetCurrentState()->Enter(GetThis<Enemy>());

	}

	void Enemy::OnUpdate()
	{
		m_StateMachine->Update();
	}
	//波に当たったかどうかの判定を行う
	bool Enemy::HitCheck()
	{
		auto PtrTransform = GetComponent<Transform>();
		auto PtrScale = GetComponent<Transform>()->GetScale();
		//[Enemy]自身のポジション
		Vector3 pos = PtrTransform->GetPosition();
		/*ここから波とエネミーのあたり判定*/
		//EnVecをすべて見てvの中に入れる
		//Waveに関するグループを取ってくる
		auto WaveGroup = GetStage()->GetSharedObjectGroup(L"UnevenGroundGroup");
		auto& EnVec = WaveGroup->GetGroupVector();
		/*ここから波とエネミーのあたり判定*/
		//EnVecをすべて見てvの中に入れる
		for (auto v : EnVec)
		{
			//vの中身が終了していなかったら
			if (!v.expired())
			{
				auto ShPtr = dynamic_pointer_cast<UnevenGround>(v.lock());
				//もしShPtrだったら
				if (ShPtr)
				{
					vector<WaveData> RetVec;
					ShPtr->CheckAllActiveWave(RetVec);
					//WaveData（RetVec）のSizeが0以上だったら
					if (RetVec.size() > 0)
					{
						for (auto& wv : RetVec)
						{
							//中心点とエネミーの距離をはかる
							float span = Vector3EX::Length(pos - wv.Pos);
							//はかった距離と半径をくらべ半径の中に入っていたら通る
							if (span + PtrScale.x >= wv.r && span - PtrScale.x <= wv.r)
							{
								SetW_Pos(pos);

								return true;
							}
						}
					}
				}
			}
		}
		return false;
	}

	bool Enemy::CrossHitCheck(){

		auto UnevenGroundPtr = GetStage()->GetSharedGameObject<UnevenGround>(L"Uneven", false);
		auto CrossPosVec = UnevenGroundPtr->m_CrossVec3Vec;
		Vector3 Pos = GetComponent<Transform>()->GetPosition();
 		for (auto CrossPos : CrossPosVec){
			float span = Vector3EX::Length(Pos - CrossPos);
			if (span < 0.5f){
				return true;
			}
		}

		return false;
	
	}

	//あたり判定が入ったら
	void Enemy::DamageMotion(){

		//[Enemy]自身のポジションを取得
		auto PtrTransform = GetComponent<Transform>();
		Vector3 pos = PtrTransform->GetPosition();

		if (Interval >= 1.0f){

			SetUpdateActive(false);
			SetDrawActive(false);

			//SEの再生
			auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
			pMultiSoundEffect->Start(L"Dameg", 0, 0.5f);

			//エネミーの撃破数をカウントするための処理を呼ぶ。名前は仮なので気にしない
			auto aaa = dynamic_pointer_cast<GameStage>(GetStage());
			aaa->CounterDownEnemy();

			//撃破時エフェクトをはじき出す
			EnemyDameageEffect();

			float Respawn = aaa->ENEMYCOUNTER;
			if (Respawn > 0){
				auto EnemyManagerPtr = GetStage()->GetSharedGameObject<EnemyManager>(L"EnemyManager", false);
				auto& HitListPtr = EnemyManagerPtr->m_ManageEnemyList;
				HitListPtr.push_back(GetThis<Enemy>());
			}
		}
		GetStateMachine()->ChangeState(Enemy_DefaultState::Instance());


	};


	void Enemy::StanMotion()
	{

		//カウントアップ
		StanTime += ElapsedTime;
		Interval += ElapsedTime;
		if (StanTime >= 7.0f)
		{
			StanTime = 0.0f;
			GetStateMachine()->ChangeState(Enemy_DefaultState::Instance());
			E_velo = Vector3(1.0f, 1.0f, 1.0f);
			auto SeekPtr = GetComponent<SeekSteering>();
			SeekPtr->SetUpdateActive(false);
			auto Ptr = GetComponent<Rigidbody>();
			Ptr->SetVelocity(E_velo);
			
		}

		auto Motion = GetStage()->GetSharedGameObject<MotionManager>(L"MotionManager");
		Motion->CallMotion(0, GetThis<Enemy>());
	};

	//衝突&操作
	void Enemy::EnemyAction(const shared_ptr<Player>& ply){
		ply->ActionWithEnemy(GetThis<Enemy>());
	};

	//麻痺時間のRefreshを行う
	void Enemy::RefreshStan()
	{

		//[Enemy]自身のポジションを取得
		auto PtrTransform = GetComponent<Transform>();
		Vector3 pos = PtrTransform->GetPosition();


		auto PtrSpark = GetStage()->GetSharedGameObject<MultiEfect>(L"MultiEfect", false);
		PtrSpark->InsertSpark(pos);

		auto pMultiSoundEffect = GetComponent<MultiSoundEffect>();
		pMultiSoundEffect->Start(L"Lightning", 0, 0.9f);


	};

	//Timerカウントしてその値で自身が持つVetor配列の要素番号をMotionManagerへ出荷
	void Enemy::Call_Motion()
	{

		//時間を加算
		CALL_TIMER += ElapsedTime;
		if (CALL_TIMER >16.0)
		{
			CALL_TIMER = 0;
		}
		//加算した時間を1Motion当たりの持ち時間で割る
		CALL_MOTION = (int)CALL_TIMER / 4;

		auto Motion = GetStage()->GetSharedGameObject<MotionManager>(L"MotionManager");
		Motion->CallMotion(TimeTable_Enemy[CALL_MOTION], GetThis<Enemy>());

	}

	//撤退関数
	void Enemy::Enemy_Escape()
	{
		//エネミーの撃破数をカウントするための処理を呼ぶ。名前は仮なので気にしない
		auto aaa = dynamic_pointer_cast<GameStage>(GetStage());
		float Respawn = aaa->ENEMYCOUNTER;
		if (Respawn <= 0)
		{
			SetDrawActive(false);
			SetUpdateActive(false);

		}
	}

	//Enemy撃破時に表示されるエフェクト
	void	Enemy::EnemyDameageEffect()
	{
		//[Enemy01]自身のポジションを取得
		auto PtrTransform = GetComponent<Transform>();
		Vector3 pos = PtrTransform->GetPosition();
		auto PtrDead = GetStage()->GetSharedGameObject<MultiSpark_jr>(L"MultiSpark_jr", false);
		PtrDead->InsertSpark(pos,10);
	}

	//----------------------------------------------------
	//class Enemy_DefaultState : public ObjState<Enemy>
	//用途: Enemyの通常移動
	//----------------------------------------------------

	//ステートのインスタンス収得
	shared_ptr<Enemy_DefaultState> Enemy_DefaultState::Instance(){
		static shared_ptr<Enemy_DefaultState> instance;
		if (!instance){

			instance = shared_ptr<Enemy_DefaultState>(new Enemy_DefaultState);
		}
		return instance;
	}
	//ステートに入った時に呼ばれる関数
	void Enemy_DefaultState::Enter(const shared_ptr<Enemy>& obj)
	{
		//エネミーのステートネームを「デフォルトステート」に書き換える
		obj->Enemy_SteatName = L"DefaultState";
	}
	//ステート中呼ばれ続ける関数
	void Enemy_DefaultState::Execute(const shared_ptr<Enemy>& obj)
	{
		obj->Call_Motion();
		if (obj->HitCheck())
		{
			obj->GetStateMachine()->ChangeState(Enemy_StanState::Instance());
		}
		obj->Enemy_Escape();
	}
	void Enemy_DefaultState::Exit(const shared_ptr<Enemy>& obj){
		//何もしない
	}
	//------------------------------------------------------
	//class Enemy_StanState : public ObjState<Enemy>;
	//用途：麻痺ステート
	//------------------------------------------------------
	//ステートのインスタンス収得
	shared_ptr<Enemy_StanState> Enemy_StanState::Instance(){
		static shared_ptr<Enemy_StanState> instance;
		if (!instance){

			instance = shared_ptr<Enemy_StanState>(new Enemy_StanState);
		}
		return instance;
	}
	//ステートに入った時に呼ばれる関数
	void Enemy_StanState::Enter(const shared_ptr<Enemy>& obj)
	{
		//エネミーのステートネームを「スタンステート」に書き換える
		obj->Enemy_SteatName = L"StanState";
		obj->RefreshStan();
	}
	//ステート中呼ばれ続ける関数
	void Enemy_StanState::Execute(const shared_ptr<Enemy>& obj)
	{
		obj->StanMotion();
		if (obj->CrossHitCheck())
		{
			obj->DamageMotion();
		}
		obj->Enemy_Escape();
	}
	void Enemy_StanState::Exit(const shared_ptr<Enemy>& obj){
		//何もしない

	}

	//--------------------------------------------------------------------------------------
	//	class EnemyManager;
	//	用途: 更新と描画が無効になっているエネミーの更新と描画を再有効かする
	//--------------------------------------------------------------------------------------
	EnemyManager::EnemyManager(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr)
	{}

	//エネミーマネージャー
	void EnemyManager::OnUpdate()
	{
		float ElapsedTime_ItemWatcher = App::GetApp()->GetElapsedTime();//時間を取得

		//エネミーの撃破数をカウントするための処理を呼ぶ。名前は仮なので気にしない
		auto aaa = dynamic_pointer_cast<GameStage>(GetStage());
		float Respawn = aaa->ENEMYCOUNTER;

		if (m_ManageEnemyList.size() != 0 && Respawn > 0)
		{

			//リストの中身があった場合
			m_RePopTime += ElapsedTime_ItemWatcher;//タイマーに時間を加算



			////一定時間になったらItemGroupの中のオブジェクトをチェックする
			if (m_RePopTime > 7.0f)
			{
				auto a = m_ManageEnemyList.front();
				//ロック解除
				auto ShBox = dynamic_pointer_cast<GameObject>(a.lock());
				if (!ShBox){ return; }
				//リストの先頭を取得
				if (!ShBox->GetUpdateActive() && !ShBox->GetDrawActive())
				{
					//更新と描画を有効化
					ShBox->SetUpdateActive(true);
					ShBox->SetDrawActive(true);
					//最初の要素を削除
					m_ManageEnemyList.pop_front();
					//タイマーを初期化
					m_RePopTime = 0;
				}
			}
		}
	}
	//--------------------------------------------------------------------------------------
	//用途 追いかけてくるBossエネミー
	//--------------------------------------------------------------------------------------

	EnemyBoss::EnemyBoss(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
		) :
		GameObject(StagePtr),
		m_StartPos(Position),
		m_StartRot(Rotation),
		m_StartScal(Position),
		E_velo(0.0f, 0.0f, 0.0f)
	{
	}

	EnemyBoss::~EnemyBoss(){}

	void EnemyBoss::OnCreate()
	{
		EnemyBossHP = 3;
		m_EnemyBossFlg = false;
		m_EnemyDraw = false;
		Defaultflg = false;
		BossInterval = 0.0f;

		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos.x,20.0f,m_StartPos.z);
		PtrTransform->SetRotation(0, XM_PIDIV4, 0);
		PtrTransform->SetScale(1.5f, 1.5f, 1.5f);

		//操舵系のコンポーネントをつける場合はRigidbodyをつける
		auto PtrRegid = AddComponent<Rigidbody>();
		auto PtrObb = AddComponent<CollisionObb>();

		auto Grope = GetStage()->GetSharedObjectGroup(L"ItemGroup");
		PtrObb->SetExcludeCollisionGroup(Grope);

		//中略

		auto SeekPtr = AddComponent<SeekSteering>();
		SeekPtr->SetUpdateActive(false);

		//BGMの登録
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"Dameg");
		pMultiSoundEffect->AddAudioResource(L"Lightning");
		pMultiSoundEffect->AddAudioResource(L"Boss");



		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_OCTAHEDRON");
		PtrDraw->SetTextureResource(L"ENEMYBOSS_TX");
		PtrDraw->SetOwnShadowActive(true);

		Matrix4X4 mat;
		mat.DefTransformation(
			Vector3(0.74f,0.74f,0.74f),
			Vector3(0.0f,XM_PIDIV4,0.0f),
			Vector3(0.0f,0.0f,0.0f)
			);

		PtrDraw->SetMeshToTransformMatrix(mat);

		SetDrawLayer(-2);

		//ステートマシンの構築
		mb_stateMAchine = make_shared< StateMachine<EnemyBoss> >(GetThis<EnemyBoss>());
		//最初のステートをDefaultStateに設定
		mb_stateMAchine->SetCurrentState(WaitingState::Instance());
		//DefaultStateの初期化実行を行う
		mb_stateMAchine->GetCurrentState()->Enter(GetThis<EnemyBoss>());

		SetDrawActive(false);
		SetAlphaActive(true);

		setDEFAULT_BOSS_COLOR();

	}


	void EnemyBoss::BossActive()
	{
		//Enemyの数が0になった時表示するようにする
		auto Ptr = dynamic_pointer_cast<GameStage>(GetStage());
		if (Ptr->ENEMYCOUNTER <= 0 && !m_EnemyBossFlg)
		{
			auto PtrTransform = AddComponent<Transform>();
			PtrTransform->SetPosition(m_StartPos);
			auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
			pMultiSoundEffect->Start(L"Boss", XAUDIO2_LOOP_INFINITE, 0.5f);
			m_EnemyDraw = true;
			SetDrawActive(true);
			GetStateMachine()->ChangeState(EB_DefaultState::Instance());

		}
	}

	void EnemyBoss::OnUpdate()
	{
		mb_stateMAchine->Update();

	}

	void EnemyBoss::Movemoton(){

		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player", false);
		auto PlayerPos = PlayerPtr->GetComponent<Transform>()->GetPosition();
		auto PtrGravity = GetComponent<Gravity>();
		auto PtrTransform = GetComponent<Transform>();
		auto PtrScale = GetComponent<Transform>()->GetScale();

		auto SeekPtr = GetComponent<SeekSteering>();
		PlayerPos.y = PtrGravity->GetBaseY();
		SeekPtr->SetTargetPosition(PlayerPos);
		SeekPtr->SetUpdateActive(true);

	}

	bool EnemyBoss::DamegeMoton(){
		auto PtrTransform = GetComponent<Transform>();
		auto PtrScale = GetComponent<Transform>()->GetScale();
		//[Enemy]自身のポジション
		Vector3 pos = PtrTransform->GetPosition();
		/*ここから波とエネミーのあたり判定*/
		//EnVecをすべて見てvの中に入れる
		//Waveに関するグループを取ってくる
		auto WaveGroup = GetStage()->GetSharedObjectGroup(L"UnevenGroundGroup");
		auto& EnVec = WaveGroup->GetGroupVector();
		/*ここから波とエネミーのあたり判定*/
		//EnVecをすべて見てvの中に入れる
		for (auto v : EnVec)
		{
			//vの中身が終了していなかったら
			if (!v.expired())
			{
				auto ShPtr = dynamic_pointer_cast<UnevenGround>(v.lock());
				//もしShPtrだったら
				if (ShPtr)
				{
					vector<WaveData> RetVec;
					ShPtr->CheckAllActiveWave(RetVec);
					//WaveData（RetVec）のSizeが0以上だったら
					if (RetVec.size() > 0)
					{
						for (auto& wv : RetVec)
						{
	
							//中心点とエネミーの距離をはかる
							float span = Vector3EX::Length(pos - wv.Pos);
							//はかった距離と半径をくらべ半径の中に入っていたら通る
							if (span + PtrScale.x >= wv.r && span - PtrScale.x <= wv.r)
							{
								SetW_Pos(pos);

								return true;
							}
						}
					}
				}
			}
		}
		return false;
	}

	//干渉点判定
	bool EnemyBoss::CrossCheck(){

		auto UnevenGroundPtr = GetStage()->GetSharedGameObject<UnevenGround>(L"Uneven", false);
		auto CrossPosVec = UnevenGroundPtr->m_CrossVec3Vec;
		Vector3 bosPos = GetComponent<Transform>()->GetPosition();
		for (auto CrossPos : CrossPosVec){
			float span = Vector3EX::Length(bosPos - CrossPos);
			if (span < 1.0f){
				return true;
			}
		}

		return false;
	}

	//ダメージ
	void EnemyBoss::Damege(){

		//[EnemyBoss]自身のポジションを取得
		auto PtrTransform = GetComponent<Transform>();
		Vector3 pos = PtrTransform->GetPosition();

		//ここをBOSSがダメージを受けた時用に変更
		auto PtrSpark = GetStage()->GetSharedGameObject<BOSS_DamageEffect>(L"BOSS_DamageEffect", false);
		PtrSpark->InsertSpark(pos);
		//SEの再生
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->Start(L"Dameg", 0, 0.5f);
		Crosshit = true;

		if (EnemyBossHP > 0)
		{
			GetStateMachine()->ChangeState(MUTEKI_BOSS_State::Instance());
		}
		if (EnemyBossHP <= 0)
		{
			SetDrawActive(false);
			SetUpdateActive(false);
		}
		
	}

	//行動待機(動かない状態)
	void EnemyBoss::Wait()
	{
		//動き(主に慣性)を止める
		auto SeekPtr = GetComponent<SeekSteering>();
		SeekPtr->SetUpdateActive(false);
		auto Ptr = GetComponent<Rigidbody>();
		Ptr->SetVelocity(0, 0, 0);
	}

	//エフェクトの生成
	void EnemyBoss::StartEfect(){

		auto PtrSpark = GetStage()->GetSharedGameObject<MultiOrb>(L"MultiOrb", false);
		PtrSpark->InsertSpark(GetComponent<Transform>()->GetPosition(), 3000);
	
	}

	void EnemyBoss::SetGravity(){
		//重力をつける
		auto PtrGravity = AddComponent<Gravity>();
		//最下地点
		PtrGravity->SetBaseY(1.0f);
	}

	//衝突&操作
	void EnemyBoss::EnemyAction(const shared_ptr<Player>& ply)
	{
		ply->ActionWithEnemyBoss(GetThis<EnemyBoss>());
	}

	//麻痺時間のRefreshを行う
	void EnemyBoss::RefStan()
	{

		//[Enemy]自身のポジションを取得
		auto PtrTransform = GetComponent<Transform>();
		Vector3 pos = PtrTransform->GetPosition();

		auto PtrSpark = GetStage()->GetSharedGameObject<MultiEfect>(L"MultiEfect", false);
		PtrSpark->InsertSpark(pos);

		auto pMultiSoundEffect = GetComponent<MultiSoundEffect>();
		pMultiSoundEffect->Start(L"Lightning", 0, 0.9f);


	};

	void EnemyBoss::PalsyMotion()
	{
		//カウントアップ
		PalsyTime += ElapsedTime;
		if (PalsyTime >= 4.0f)
		{
			PalsyTime = 0.0f;
			GetStateMachine()->ChangeState(EB_DefaultState::Instance());
			E_velo = Vector3(10.0f, 1.0f, 10.0f);
			auto SeekPtr = GetComponent<SeekSteering>();
			SeekPtr->SetUpdateActive(false);
			auto Ptr = GetComponent<Rigidbody>();
			Ptr->SetVelocity(E_velo);
		}
	};

	void EnemyBoss::hitrif(){
		if (Crosshit){
			Crosshit = false;
			EnemyBossHP -= 1;
		}
		if (EnemyBossHP <= 0){
			auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
			pMultiSoundEffect->Stop(L"Boss");
			StartEfect();
			m_EnemyBossFlg = true;
			SetUpdateActive(false);
			SetDrawActive(false);
		}
	}

	void EnemyBoss::StopSound(){
				auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
				pMultiSoundEffect->Stop(L"Boss");

	}

	void EnemyBoss::ChangMusic(){
	
		BossMusic = true;
	}
	//無敵TIME
	void EnemyBoss::MUTEKI_TIME()
	{
		muteki_time += ElapsedTime;
		if (muteki_time > 3)
		{
			auto PtrDraw = GetComponent<PNTStaticDraw>();
			PtrDraw->SetDiffuse(DEFAULT_DIFFUSE);
			PtrDraw->SetEmissive(DEFAULT_EMISSIVE);
			GetStateMachine()->ChangeState(EB_DefaultState::Instance());
			return;
		}
		FLASH_BOSS();
	}

	//ボスの色を通常に戻す
	void EnemyBoss::setDEFAULT_BOSS_COLOR()
	{
		auto PtrDraw = GetComponent<PNTStaticDraw>();
		DEFAULT_DIFFUSE = PtrDraw->GetDiffuse();
		DEFAULT_EMISSIVE = PtrDraw->GetEmissive();
	}

	//BOSS点滅
	void EnemyBoss::FLASH_BOSS()
	{
		FLASH_TIME_BOSS++;

		if ((int)FLASH_TIME_BOSS<10)
		{
			SetDrawActive(false);
		}
		else
		{
			SetDrawActive(true);
		}
		if ((int)FLASH_TIME_BOSS > 15)
		{
			FLASH_TIME_BOSS = 0;
		}
	}

	//
	void EnemyBoss::BOSS_Reflesh()
	{
		if (EnemyBossHP >0)
		{
			muteki_time = 0;
			FLASH_TIME_BOSS = 0;
			SetDrawActive(true);
		}
		else
		{
			SetDrawActive(false);
		}
	}

	//BOSS出現時呼び出される、Effect
	void EnemyBoss::Spawn_Effect()
	{
		//[Enemy01]自身のポジションを取得
		auto PtrTransform = GetComponent<Transform>();
		Vector3 pos = PtrTransform->GetPosition();
		auto PtrSpawn = GetStage()->GetSharedGameObject<MultiSpark_Square>(L"MultiSpark_Square", false);
		PtrSpawn->InsertSpark(pos, 100);
	}

	//Effect生成
	void EnemyBoss::STAN_Effect()
	{
		auto PtrTransform = GetComponent<Transform>();
		Vector3 pos = PtrTransform->GetPosition();
		auto PtrHeart = GetStage()->GetSharedGameObject<MultiSpark_STAN>(L"MultiSpark_STAN", false);
		PtrHeart->InsertSpark(pos, 1);
	}

	//----------------------------------------------------
	//class WaitingState : public ObjState<Enemy>
	//用途: EnemyBossの待機状態。ザコが一定数倒されるとdefaultステートへ移行
	//----------------------------------------------------

	//ステートのインスタンス収得
	shared_ptr<WaitingState> WaitingState::Instance(){
		static shared_ptr<WaitingState> instance;
		if (!instance){

			instance = shared_ptr<WaitingState>(new WaitingState);
		}
		return instance;
	}
	//ステートに入った時に呼ばれる関数
	void WaitingState::Enter(const shared_ptr<EnemyBoss>& obj){
		//何もしない
	}
	void WaitingState::Execute(const shared_ptr<EnemyBoss>& obj){
		obj->BossActive();
	}
	void WaitingState::Exit(const shared_ptr<EnemyBoss>& obj){
	
		obj->ChangMusic();
		obj->SetGravity();
		obj->Spawn_Effect();
	}

	//----------------------------------------------------
	//class EB_DefaultState : public ObjState<EnemyBoss>
	//用途: EnemyBossの基本行動。波に当たると麻痺ステートへ移行
	//----------------------------------------------------

	//ステートのインスタンス収得
	shared_ptr<EB_DefaultState> EB_DefaultState::Instance(){
		static shared_ptr<EB_DefaultState> instance;
		if (!instance){

			instance = shared_ptr<EB_DefaultState>(new EB_DefaultState);
		}
		return instance;
	}
	//ステートに入った時に呼ばれる関数
	void EB_DefaultState::Enter(const shared_ptr<EnemyBoss>& obj){
		obj->BOSS_Reflesh();
		obj->setDEFAULT_BOSS_COLOR();
	}
	//
	void EB_DefaultState::Execute(const shared_ptr<EnemyBoss>& obj){
		obj->Movemoton();
		if (obj->DamegeMoton()){
			obj->GetStateMachine()->ChangeState(DamegeState_B::Instance());
		}
	}
	void EB_DefaultState::Exit(const shared_ptr<EnemyBoss>& obj){

		obj->Wait();
	}

	//------------------------------------------------------
	//class DamegeState : public ObjState<Enemy>;
	//用途：EnemyBossが麻痺した状態。干渉点に当たるとダメージを負い、MUTEKIステートへ移行
	//------------------------------------------------------
	//ステートのインスタンス収得
	shared_ptr<DamegeState_B> DamegeState_B::Instance(){
		static shared_ptr<DamegeState_B> instance;
		if (!instance){

			instance = shared_ptr<DamegeState_B>(new DamegeState_B);
		}
		return instance;
	}
	//ステートに入った時に呼ばれる関数
	void DamegeState_B::Enter(const shared_ptr<EnemyBoss>& obj){
		obj->RefStan();
	}
	//
	void DamegeState_B::Execute(const shared_ptr<EnemyBoss>& obj){
		obj->PalsyMotion();
		if (obj->CrossCheck()){
			obj->Damege();

		}
		obj->STAN_Effect();


	}
	void DamegeState_B::Exit(const shared_ptr<EnemyBoss>& obj){
		obj->hitrif();
	}

	//------------------------------------------------------
	//class MUTEKI_BOSS_State : public ObjState<Enemy>;
	//用途：Bossがダメージを受けた後の無敵状態。一定時間後にdefaultステートへ移行
	//------------------------------------------------------
	//ステートのインスタンス収得
	shared_ptr<MUTEKI_BOSS_State> MUTEKI_BOSS_State::Instance(){
		static shared_ptr<MUTEKI_BOSS_State> instance;
		if (!instance){

			instance = shared_ptr<MUTEKI_BOSS_State>(new MUTEKI_BOSS_State);
		}
		return instance;
	}
	//ステートに入った時に呼ばれる関数
	void MUTEKI_BOSS_State::Enter(const shared_ptr<EnemyBoss>& obj){
		obj->RefStan();
	}
	//
	void MUTEKI_BOSS_State::Execute(const shared_ptr<EnemyBoss>& obj){
		obj->Movemoton();
		obj->MUTEKI_TIME();

	}
	void MUTEKI_BOSS_State::Exit(const shared_ptr<EnemyBoss>& obj){
		obj->hitrif();
	}
}