#include "stdafx.h"
#include "Project.h"

namespace basedx11{

	//--------------------------------------------------------------------------------------
	//  class MultiOrb : public MultiParticle;
	//  用途: 複数のオーブ。人が満足しきった時に発生し、名声ゲージに飛んでいきます。
	//--------------------------------------------------------------------------------------
	//構築と破棄
	MultiOrb::MultiOrb(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr)
	{}
	MultiOrb::~MultiOrb(){}

	//ハートを生成します。
	void MultiOrb::InsertSpark(const Vector3& Pos, UINT num){
		auto ParticlePtr = InsertParticle(num);
		ParticlePtr->SetEmitterPos(Vector3(0, 0, 0));
		ParticlePtr->SetTextureResource(L"Dameage_Enemy_TX");
		ParticlePtr->SetMaxTime(4.5f);
		vector<ParticleSprite>& pSpriteVec = ParticlePtr->GetParticleSpriteVec();
		for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()){
			//各パーティクルの初期座標を少しずらす
			rParticleSprite.m_LocalPos.x = Util::RandZeroToOne() * 0.1f - 0.05f;
			rParticleSprite.m_LocalPos.y = Util::RandZeroToOne() * 0.1f;
			rParticleSprite.m_LocalPos.z = Util::RandZeroToOne() * 0.1f - 0.05f;
			//各パーティクルの移動速度を指定
			rParticleSprite.m_Velocity = Vector3(
				rParticleSprite.m_LocalPos.x * 20.0f,
				rParticleSprite.m_LocalPos.y * 20.0f,
				rParticleSprite.m_LocalPos.z * 20.0f
				);
			//各パーティクルの座標を、渡された座標からの相対位置にする。
			rParticleSprite.m_LocalPos += Pos;
			//各パーティクルの大きさを指定
			rParticleSprite.m_LocalScale = Vector3(1, 1, 1) * 0.25f;
			//色の指定
			Color4 color(1, 1, 1, 1);
			//float angle = Util::RandZeroToOne() * XM_PI * 2.0f;
			//color.x = cos(angle + 0 * XM_PIDIV4 * 1.39f);
			//color.y = cos(angle + 1 * XM_PIDIV4 * 1.39f);
			//color.z = cos(angle + 2 * XM_PIDIV4 * 1.39f);
			rParticleSprite.m_Color = color;
		}
	}

	void MultiOrb::OnUpdate(){
		//前回のターンからの時間
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		//カメラの座標を取得
		auto PtrCamera = GetStage()->GetCamera(0);
		auto CameraPos = PtrCamera->GetEye();
		auto player = GetStage()->GetSharedGameObject<EnemyBoss>(L"EnemyBoss");
		CameraPos = player->GetComponent<Transform>()->GetPosition();

		bool isExplosion = false;
		auto CntrVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntrVec[0].wButtons & XINPUT_GAMEPAD_B){
			isExplosion = true;
		}

		for (auto ParticlePtr : GetParticleVec()){
			//トータル時間に加算
			ParticlePtr->AddTotalTime(ElapsedTime);
			for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()){
				//アクティブで無かったら、次のループへ進む
				if (!rParticleSprite.m_Active){
					continue;
				}

				//移動速度（目的の位置とパーティクルの位置の差）
				Vector3 Vel = CameraPos - rParticleSprite.m_LocalPos;
				if (Vector3EX::Length(Vel) > 10.0f){
					rParticleSprite.m_Active = false;
					continue;
				}
				if (Vector3EX::Length(Vel) > 4.0f){
					Vel.Zero();
				}
				if (isExplosion){
					Vel += CameraPos - rParticleSprite.m_LocalPos;
				}
				Vel.Normalize();
				Vel *= -1;
				Vel += (Vel - rParticleSprite.m_Velocity) * ElapsedTime;
				//移動速度の更新
				rParticleSprite.m_Velocity += Vel;
				//移動速度に従って移動させる
				rParticleSprite.m_LocalPos += rParticleSprite.m_Velocity * ElapsedTime;
				////色を変化させる(だんだん透明になる)
				rParticleSprite.m_Color.w *= 0.94f;
				if (ParticlePtr->GetTotalTime() >= ParticlePtr->GetMaxTime()){
					//制限時間になったら、フラグを下げる
					rParticleSprite.m_Active = false;
				}
			}
		}

	}


	//--------------------------------------------------------------------------------------
	//  class PowerUpEffect : public MultiParticle;
	//  用途: Playerがパワーアップ状態の間、出続けるエフェクト
	//--------------------------------------------------------------------------------------
	//構築と破棄
	PowerUpEffect::PowerUpEffect(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr)
	{}
	PowerUpEffect::~PowerUpEffect(){}

	//ハートを生成します。
	void PowerUpEffect::InsertSpark(const Vector3& Pos, UINT num){
		auto ParticlePtr = InsertParticle(num);
		ParticlePtr->SetEmitterPos(Vector3(0, 0, 0));
		ParticlePtr->SetTextureResource(L"WAVE_TX");//パーティクルのテクスチャを設定
		ParticlePtr->SetMaxTime(5.0f);//パーティクルの表示時間？を設定
		ParticlePtr->SetDrawOption(Particle::DrawOption::Normal);
		vector<ParticleSprite>& pSpriteVec = ParticlePtr->GetParticleSpriteVec();
		for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()){
			//各パーティクルの初期座標を少しずらす
			rParticleSprite.m_LocalPos.x = Util::RandZeroToOne() * 0.5f;
			rParticleSprite.m_LocalPos.y = Util::RandZeroToOne() * 0.5f;
			rParticleSprite.m_LocalPos.z = Util::RandZeroToOne() * 0.5f;


			Quaternion Qt;
			/*
			角度を変える場合、↑でParticlePtr->SetDrawOption(Particle::DrawOption::Normal);と宣言し
			描画オプションを標準に切り替える必要がある。
			また、XYZでは無く、ロールピッチヨーで角度を指定する必要がある。その際、角度はディグリーではなく
			ラジアン角なので、<「目的の角度」/2π>で角度の変換を行わなければならない
			*/
			float Pitch = (90.0f / 180.0f * XM_PI);
			//	rParticleSprite.m_LocalQt.RotationRollPitchYaw(90.0f/180.0f * XM_PI, 0.0f, 0.0f);
			rParticleSprite.m_LocalQt.RotationRollPitchYaw(Pitch, 0.0f, 0.0f);
			//各パーティクルの移動速度を指定
			rParticleSprite.m_Velocity = Vector3(
				rParticleSprite.m_LocalPos.x,
				rParticleSprite.m_LocalPos.y,// * 5.0f,
				rParticleSprite.m_LocalPos.z
				);
			//各パーティクルの座標を、渡された座標からの相対位置にする。
			rParticleSprite.m_LocalPos += Pos;
			//各パーティクルの大きさを指定
			rParticleSprite.m_LocalScale = Vector3(1, 1, 1) * 1.0f;
			//色の指定
			Color4 color(1, 1, 1, 1);
			//float angle = Util::RandZeroToOne() * XM_PI * 2.0f;
			//color.x = cos(angle + 0 * XM_PIDIV4 * 1.39f);
			//color.y = cos(angle + 1 * XM_PIDIV4 * 1.39f);
			//color.z = cos(angle + 2 * XM_PIDIV4 * 1.39f);
			rParticleSprite.m_Color = color;
		}
	}

	void PowerUpEffect::OnUpdate(){
		//前回のターンからの時間
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		//カメラの座標を取得
		auto PtrCamera = GetStage()->GetCamera(0);
		auto CameraPos = PtrCamera->GetEye();
		auto player = GetStage()->GetSharedGameObject<Player>(L"Player");
		CameraPos = player->GetComponent<Transform>()->GetPosition();

		auto PlayerPos = player->GetComponent<Transform>()->GetPosition();

		bool isExplosion = false;
		auto CntrVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntrVec[0].wButtons & XINPUT_GAMEPAD_B){
			isExplosion = true;
		}

		for (auto ParticlePtr : GetParticleVec()){
			//トータル時間に加算
			ParticlePtr->AddTotalTime(ElapsedTime);
			for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()){
				//アクティブで無かったら、次のループへ進む
				if (!rParticleSprite.m_Active){
					continue;
				}

				//移動速度（目的の位置とパーティクルの位置の差）
				Vector3 Vel = CameraPos - rParticleSprite.m_LocalPos;
				if (Vector3EX::Length(Vel) > 10.0f){
					rParticleSprite.m_Active = false;
					continue;
				}
				if (Vector3EX::Length(Vel) > 5.0f){
					Vel.Zero();
				}
				if (isExplosion){
					Vel += CameraPos - rParticleSprite.m_LocalPos;
				}
				Vel.Normalize();
				Vel *= -1;
				Vel += (Vel - rParticleSprite.m_Velocity) * ElapsedTime;
				//移動速度の更新
				rParticleSprite.m_Velocity += Vel;
				//移動速度に従って移動させる
				//rParticleSprite.m_LocalPos += rParticleSprite.m_Velocity * ElapsedTime;


				//回転
				/*AddRotation(Vector3(0.5f, 0.0f, 0.0f));*/
				//各パーティクルの角度を変更する


				rParticleSprite.m_LocalPos = PlayerPos;
				auto PlayerState = player->SteatName;

				if (PlayerState == L"PowerUpState")
				{
					Timer += ElapsedTime;//時間で拡大処理を切り替える
					if (Timer < 3)
					{
						//	m_Radius += ElapsedTime;
						m_Up += ElapsedTime*0.1;
					}
					else if (3 < Timer)
					{
						//	m_Radius -= ElapsedTime;
						m_Up += ElapsedTime * 0.1;
					}
					if (Timer >= 5)
					{
						//	m_Radius = 0.5;
						m_Up = 0.0;
						Timer = 0;
						//	AddScaleflag = false;
					}
				}
				//	m_Radius = 1.5;
				//	rParticleSprite.m_LocalScale = Vector3(m_Radius, m_Radius, m_Radius);
				rParticleSprite.m_LocalPos = Vector3(PlayerPos.x, m_Up, PlayerPos.z);

				////色を変化させる(だんだん透明になる)
				//rParticleSprite.m_Color.w *= 0.94f;
				if (ParticlePtr->GetTotalTime() >= ParticlePtr->GetMaxTime()){
					//制限時間になったら、フラグを下げる
					rParticleSprite.m_Active = false;
				}
			}
		}
	}

	//--------------------------------------------------------------------------------------
	//  class HeelEffect : public MultiParticle;
	//  用途: Playerが回復したときに出るエフェクト
	//--------------------------------------------------------------------------------------
	//構築と破棄
	HeelEffect::HeelEffect(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr)
	{}
	HeelEffect::~HeelEffect(){}

	//ハートを生成します。
	void HeelEffect::InsertSpark(const Vector3& Pos, UINT num){
		auto ParticlePtr = InsertParticle(num);
		ParticlePtr->SetEmitterPos(Vector3(0, 0, 0));
		ParticlePtr->SetTextureResource(L"HeelEffect_TX");//パーティクルのテクスチャを設定
		ParticlePtr->SetMaxTime(1.0f);//パーティクルの表示時間？を設定
		vector<ParticleSprite>& pSpriteVec = ParticlePtr->GetParticleSpriteVec();
		for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()){
			//各パーティクルの初期座標を少しずらす
			float HalfOne_x = Util::DivProbability(2);// 1/2の確率でtrueを返す
			float HalfOne_z = Util::DivProbability(2);// 1/2の確率でtrueを返す
			//各パーティクルの初期座標を少しずらす
			if (HalfOne_x){ rParticleSprite.m_LocalPos.x = 0 - Util::RandZeroToOne() * 0.5f; }
			else{
				rParticleSprite.m_LocalPos.x = Util::RandZeroToOne() * 0.5f;
			}

			rParticleSprite.m_LocalPos.y = Util::RandZeroToOne() * 0.5f;


			if (HalfOne_z){ rParticleSprite.m_LocalPos.z = 0 - Util::RandZeroToOne() * 0.5f; }
			else{
				rParticleSprite.m_LocalPos.z = Util::RandZeroToOne() * 0.5f;
			}
			//各パーティクルの移動速度を指定
			rParticleSprite.m_Velocity = Vector3(
				//rParticleSprite.m_LocalPos.x * 5.0f,
				rParticleSprite.m_LocalPos.x,
				rParticleSprite.m_LocalPos.y * 5.0f,
				//rParticleSprite.m_LocalPos.z * 5.0f
				rParticleSprite.m_LocalPos.z
				);
			//各パーティクルの座標を、渡された座標からの相対位置にする。
			rParticleSprite.m_LocalPos += Pos;
			//各パーティクルの大きさを指定
			rParticleSprite.m_LocalScale = Vector3(1, 1, 1) * 0.5f;
			//色の指定
			Color4 color(1, 1, 1, 1);
			//float angle = Util::RandZeroToOne() * XM_PI * 2.0f;
			//color.x = cos(angle + 0 * XM_PIDIV4 * 1.39f);
			//color.y = cos(angle + 1 * XM_PIDIV4 * 1.39f);
			//color.z = cos(angle + 2 * XM_PIDIV4 * 1.39f);
			rParticleSprite.m_Color = color;
		}
	}

	void HeelEffect::OnUpdate(){
		//前回のターンからの時間
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		//カメラの座標を取得
		auto PtrCamera = GetStage()->GetCamera(0);
		auto CameraPos = PtrCamera->GetEye();
		auto player = GetStage()->GetSharedGameObject<Player>(L"Player");
		CameraPos = player->GetComponent<Transform>()->GetPosition();

		bool isExplosion = false;
		auto CntrVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntrVec[0].wButtons & XINPUT_GAMEPAD_B){
			isExplosion = true;
		}

		for (auto ParticlePtr : GetParticleVec()){
			//トータル時間に加算
			ParticlePtr->AddTotalTime(ElapsedTime);
			for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()){
				//アクティブで無かったら、次のループへ進む
				if (!rParticleSprite.m_Active){
					continue;
				}

				//移動速度（目的の位置とパーティクルの位置の差）
				Vector3 Vel = CameraPos - rParticleSprite.m_LocalPos;
				if (Vector3EX::Length(Vel) > 10.0f){
					rParticleSprite.m_Active = false;
					continue;
				}
				if (Vector3EX::Length(Vel) > 5.0f){
					Vel.Zero();
				}
				if (isExplosion){
					Vel += CameraPos - rParticleSprite.m_LocalPos;
				}
				Vel.Normalize();
				Vel *= -1;
				Vel += (Vel - rParticleSprite.m_Velocity) * ElapsedTime;
				//移動速度の更新
				rParticleSprite.m_Velocity += Vel;
				//移動速度に従って移動させる
				rParticleSprite.m_LocalPos += rParticleSprite.m_Velocity * ElapsedTime;
				////色を変化させる(だんだん透明になる)
				//rParticleSprite.m_Color.w *= 0.94f;
				if (ParticlePtr->GetTotalTime() >= ParticlePtr->GetMaxTime()){
					//制限時間になったら、フラグを下げる
					rParticleSprite.m_Active = false;
				}
			}
		}

	}

	//--------------------------------------------------------------------------------------
	//  class Player_DamageEffect : public MultiParticle;
	//  用途: Playerが回復したときに出るエフェクト
	//--------------------------------------------------------------------------------------
	//構築と破棄
	Player_DamageEffect::Player_DamageEffect(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr)
	{}
	Player_DamageEffect::~Player_DamageEffect(){}

	//ハートを生成します。
	void Player_DamageEffect::InsertSpark(const Vector3& Pos, UINT num){
		auto ParticlePtr = InsertParticle(num);
		ParticlePtr->SetEmitterPos(Vector3(0, 0, 0));
		ParticlePtr->SetTextureResource(L"SPARK_TX");//パーティクルのテクスチャを設定
		ParticlePtr->SetMaxTime(1.0f);//パーティクルの表示時間？を設定
		vector<ParticleSprite>& pSpriteVec = ParticlePtr->GetParticleSpriteVec();
		for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()){
			//各パーティクルの初期座標を少しずらす
			float HalfOne_x = Util::DivProbability(2);// 1/2の確率でtrueを返す
			float HalfOne_z = Util::DivProbability(2);// 1/2の確率でtrueを返す
			//各パーティクルの初期座標を少しずらす
			if (HalfOne_x){ rParticleSprite.m_LocalPos.x = 0 - Util::RandZeroToOne() * 0.5f; }
			else{
				rParticleSprite.m_LocalPos.x = Util::RandZeroToOne() * 0.5f;
			}

			rParticleSprite.m_LocalPos.y = Util::RandZeroToOne() * 0.5f;


			if (HalfOne_z){ rParticleSprite.m_LocalPos.z = 0 - Util::RandZeroToOne() * 0.5f; }
			else{
				rParticleSprite.m_LocalPos.z = Util::RandZeroToOne() * 0.5f;
			}
			//各パーティクルの移動速度を指定
			rParticleSprite.m_Velocity = Vector3(
				//rParticleSprite.m_LocalPos.x * 5.0f,
				rParticleSprite.m_LocalPos.x,
				rParticleSprite.m_LocalPos.y * 5.0f,
				//rParticleSprite.m_LocalPos.z * 5.0f
				rParticleSprite.m_LocalPos.z
				);
			//各パーティクルの座標を、渡された座標からの相対位置にする。
			rParticleSprite.m_LocalPos += Pos;
			//各パーティクルの大きさを指定
			rParticleSprite.m_LocalScale = Vector3(1, 1, 1) * 0.4f;
			//色の指定
			Color4 color(1, 1, 1, 1);
			//float angle = Util::RandZeroToOne() * XM_PI * 2.0f;
			//color.x = cos(angle + 0 * XM_PIDIV4 * 1.39f);
			//color.y = cos(angle + 1 * XM_PIDIV4 * 1.39f);
			//color.z = cos(angle + 2 * XM_PIDIV4 * 1.39f);
			rParticleSprite.m_Color = color;
		}
	}

	void Player_DamageEffect::OnUpdate(){
		//前回のターンからの時間
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		//カメラの座標を取得
		auto PtrCamera = GetStage()->GetCamera(0);
		auto CameraPos = PtrCamera->GetEye();
		auto player = GetStage()->GetSharedGameObject<Player>(L"Player");
		CameraPos = player->GetComponent<Transform>()->GetPosition();

		bool isExplosion = false;
		auto CntrVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntrVec[0].wButtons & XINPUT_GAMEPAD_B){
			isExplosion = true;
		}

		for (auto ParticlePtr : GetParticleVec()){
			//トータル時間に加算
			ParticlePtr->AddTotalTime(ElapsedTime);
			for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()){
				//アクティブで無かったら、次のループへ進む
				if (!rParticleSprite.m_Active){
					continue;
				}

				//移動速度（目的の位置とパーティクルの位置の差）
				Vector3 Vel = CameraPos - rParticleSprite.m_LocalPos;
				if (Vector3EX::Length(Vel) > 10.0f){
					rParticleSprite.m_Active = false;
					continue;
				}
				if (Vector3EX::Length(Vel) > 5.0f){
					Vel.Zero();
				}
				if (isExplosion){
					Vel += CameraPos - rParticleSprite.m_LocalPos;
				}
				Vel.Normalize();
				Vel *= -1;
				Vel += (Vel - rParticleSprite.m_Velocity) * ElapsedTime;
				//移動速度の更新
				rParticleSprite.m_Velocity += Vel;
				//移動速度に従って移動させる
				rParticleSprite.m_LocalPos += rParticleSprite.m_Velocity * ElapsedTime;
				////色を変化させる(だんだん透明になる)
				rParticleSprite.m_Color.w *= 0.94f;
				if (ParticlePtr->GetTotalTime() >= ParticlePtr->GetMaxTime()){
					//制限時間になったら、フラグを下げる
					rParticleSprite.m_Active = false;
				}
			}
		}

	}

	//--------------------------------------------------------------------------------------
	//class BOSS_DamageEffect : public MultiParticle;
	//用途: 複数の小規模なスパーククラス
	//--------------------------------------------------------------------------------------

	//構築と破棄
	BOSS_DamageEffect::BOSS_DamageEffect(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr)
	{}
	BOSS_DamageEffect::~BOSS_DamageEffect(){}

	//パーティクルを生成します。
	void BOSS_DamageEffect::InsertSpark(const Vector3& Pos){
		auto ParticlePtr = InsertParticle(15);
		ParticlePtr->SetEmitterPos(Vector3(0, 0, 0));
		ParticlePtr->SetTextureResource(L"Dameage_Enemy_TX");
		ParticlePtr->SetMaxTime(3.0f);
		vector<ParticleSprite>& pSpriteVec = ParticlePtr->GetParticleSpriteVec();
		for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()){
			//各パーティクルの初期座標を少しずらす
			//rParticleSprite.m_LocalPos.x = Util::RandZeroToOne() * 2.0f;
			//rParticleSprite.m_LocalPos.z = Util::RandZeroToOne() * 2.0f;

			float HalfOne_x = Util::DivProbability(2);// 1/2の確率でtrueを返す
			float HalfOne_y = Util::DivProbability(2);// 1/2の確率でtrueを返す
			float HalfOne_z = Util::DivProbability(2);// 1/2の確率でtrueを返す

			//パーティクルの初期位置X
			if (HalfOne_x){ rParticleSprite.m_LocalPos.x = 0 - Util::RandZeroToOne() * 1.0f; }
			else{
				rParticleSprite.m_LocalPos.x = Util::RandZeroToOne() * 1.0f;
			}
			//パーティクルの初期位置Y
			if (HalfOne_y){ rParticleSprite.m_LocalPos.y = 0 - Util::RandZeroToOne() *1.0f; }
			else{
				rParticleSprite.m_LocalPos.y = Util::RandZeroToOne() * 1.0f;
			}
			//パーティクルの初期位置Z
			if (HalfOne_z){ rParticleSprite.m_LocalPos.z = 0 - Util::RandZeroToOne() * 1.0f; }
			else{
				rParticleSprite.m_LocalPos.z = Util::RandZeroToOne() * 1.0f;
			}

			//各パーティクルの移動速度を指定
			rParticleSprite.m_Velocity = Vector3(
				rParticleSprite.m_LocalPos.x * 0.5f,
				//rParticleSprite.m_LocalPos.y * 0.5f,
				0,
				rParticleSprite.m_LocalPos.z * 0.5f
				);
			//各パーティクルの座標を、渡された座標からの相対位置にする。
			rParticleSprite.m_LocalPos += Pos;
			//各パーティクルの大きさを指定
			rParticleSprite.m_LocalScale = Vector3(1, 1, 1) * 0.75f;
			//色の指定
			Color4 color(1, 1, 1, 1);
			//float angle = Util::RandZeroToOne() * XM_PI * 2.0f;
			//color.x = cos(angle + 0 * XM_PIDIV4 * 1.39f);
			//color.y = cos(angle + 1 * XM_PIDIV4 * 1.39f);
			//color.z = cos(angle + 2 * XM_PIDIV4 * 1.39f);
			rParticleSprite.m_Color = color;
		}
	}

	void BOSS_DamageEffect::OnUpdate(){
		//前回のターンからの時間
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		//カメラの座標を取得
		auto PtrCamera = GetStage()->GetCamera(0);
		auto CameraPos = PtrCamera->GetEye();
		auto player = GetStage()->GetSharedGameObject<EnemyBoss>(L"EnemyBoss");
		CameraPos = player->GetComponent<Transform>()->GetPosition();

		bool isExplosion = false;
		auto CntrVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntrVec[0].wButtons & XINPUT_GAMEPAD_B){
			isExplosion = true;
		}

		for (auto ParticlePtr : GetParticleVec()){
			//トータル時間に加算
			ParticlePtr->AddTotalTime(ElapsedTime);
			for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()){
				//アクティブで無かったら、次のループへ進む
				if (!rParticleSprite.m_Active){
					continue;
				}

				//移動速度（目的の位置とパーティクルの位置の差）
				Vector3 Vel = CameraPos - rParticleSprite.m_LocalPos;
				if (Vector3EX::Length(Vel) > 10.0f){
					rParticleSprite.m_Active = false;
					continue;
				}
				if (Vector3EX::Length(Vel) > 4.0f){
					Vel.Zero();
				}
				if (isExplosion){
					Vel += CameraPos - rParticleSprite.m_LocalPos;
				}
				Vel.Normalize();
				Vel *= -1;
				Vel += (Vel - rParticleSprite.m_Velocity) * ElapsedTime;
				//移動速度の更新
				rParticleSprite.m_Velocity += Vel;
				//移動速度に従って移動させる
				rParticleSprite.m_LocalPos += rParticleSprite.m_Velocity * ElapsedTime;
				////色を変化させる(だんだん透明になる)
				rParticleSprite.m_Color.w *= 0.95f;
				//rParticleSprite.m_Color.w *= 1.1f;
				//
				//rParticleSprite.m_LocalScale *= 0.97;
				if (ParticlePtr->GetTotalTime() >= ParticlePtr->GetMaxTime()){
					//制限時間になったら、フラグを下げる
					rParticleSprite.m_Active = false;
				}
			}
		}

	}

	

	//--------------------------------------------------------------------------------------
	//class MultiSpark_Square : public MultiParticle;
	//用途: 複数の小規模なスパーククラス
	//--------------------------------------------------------------------------------------

	//構築と破棄
	MultiSpark_Square::MultiSpark_Square(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr)
	{}
	MultiSpark_Square::~MultiSpark_Square(){}

	//パーティクルを生成します。
	void MultiSpark_Square::InsertSpark(const Vector3& Pos, UINT num){
		auto ParticlePtr = InsertParticle(num);
		ParticlePtr->SetEmitterPos(Vector3(0, 0, 0));
		ParticlePtr->SetTextureResource(L"Square_B_TX");
		ParticlePtr->SetMaxTime(10.0f);
		vector<ParticleSprite>& pSpriteVec = ParticlePtr->GetParticleSpriteVec();
		for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()){
			//各パーティクルの初期座標を少しずらす
			//rParticleSprite.m_LocalPos.x = Util::RandZeroToOne() * 2.0f;
			//rParticleSprite.m_LocalPos.z = Util::RandZeroToOne() * 2.0f;

			float HalfOne_x = Util::DivProbability(2);// 1/2の確率でtrueを返す
			float HalfOne_y = Util::DivProbability(2);// 1/2の確率でtrueを返す
			float HalfOne_z = Util::DivProbability(2);// 1/2の確率でtrueを返す

			//パーティクルの初期位置X
			if (HalfOne_x){ rParticleSprite.m_LocalPos.x = 0 - Util::RandZeroToOne() * 1.0f; }
			else{
				rParticleSprite.m_LocalPos.x = Util::RandZeroToOne() * 1.0f;
			}
			//パーティクルの初期位置Y
			if (HalfOne_y){ rParticleSprite.m_LocalPos.y = 0 - Util::RandZeroToOne() *1.0f; }
			else{
				rParticleSprite.m_LocalPos.y = Util::RandZeroToOne() * 1.0f;
			}
			//パーティクルの初期位置Z
			if (HalfOne_z){ rParticleSprite.m_LocalPos.z = 0 - Util::RandZeroToOne() * 1.0f; }
			else{
				rParticleSprite.m_LocalPos.z = Util::RandZeroToOne() * 1.0f;
			}

			//各パーティクルの移動速度を指定
			rParticleSprite.m_Velocity = Vector3(
				rParticleSprite.m_LocalPos.x * 0.5f,
				//rParticleSprite.m_LocalPos.y * 0.5f,
				0,
				rParticleSprite.m_LocalPos.z * 0.5f
				);
			//各パーティクルの座標を、渡された座標からの相対位置にする。
			rParticleSprite.m_LocalPos += Pos;
			//各パーティクルの大きさを指定
			rParticleSprite.m_LocalScale = Vector3(1, 1, 1) * 1.0f;
			//色の指定
			Color4 color(1, 1, 1, 1);
			//float angle = Util::RandZeroToOne() * XM_PI * 2.0f;
			//color.x = cos(angle + 0 * XM_PIDIV4 * 1.39f);
			//color.y = cos(angle + 1 * XM_PIDIV4 * 1.39f);
			//color.z = cos(angle + 2 * XM_PIDIV4 * 1.39f);
			rParticleSprite.m_Color = color;
		}
	}

	void MultiSpark_Square::OnUpdate(){
		//前回のターンからの時間
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		//カメラの座標を取得
		auto PtrCamera = GetStage()->GetCamera(0);
		auto CameraPos = PtrCamera->GetEye();
		auto player = GetStage()->GetSharedGameObject<EnemyBoss>(L"EnemyBoss");
		CameraPos = player->GetComponent<Transform>()->GetPosition();

		bool isExplosion = false;
		auto CntrVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntrVec[0].wButtons & XINPUT_GAMEPAD_B){
			isExplosion = true;
		}

		for (auto ParticlePtr : GetParticleVec()){
			//トータル時間に加算
			ParticlePtr->AddTotalTime(ElapsedTime);
			for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()){
				//アクティブで無かったら、次のループへ進む
				if (!rParticleSprite.m_Active){
					continue;
				}

				//移動速度（目的の位置とパーティクルの位置の差）
				Vector3 Vel = CameraPos - rParticleSprite.m_LocalPos;
				if (Vector3EX::Length(Vel) > 10.0f){
					rParticleSprite.m_Active = false;
					continue;
				}
				if (Vector3EX::Length(Vel) > 4.0f){
					Vel.Zero();
				}
				if (isExplosion){
					Vel += CameraPos - rParticleSprite.m_LocalPos;
				}
				Vel.Normalize();
				Vel *= -1;
				Vel += (Vel - rParticleSprite.m_Velocity) * ElapsedTime;
				//移動速度の更新
				rParticleSprite.m_Velocity += Vel;
				//移動速度に従って移動させる
				rParticleSprite.m_LocalPos += rParticleSprite.m_Velocity * ElapsedTime;
				////色を変化させる(だんだん透明になる)
				rParticleSprite.m_Color.w *= 0.99f;
				//rParticleSprite.m_Color.w *= 1.1f;
				//
				//rParticleSprite.m_LocalScale *= 0.97;
				if (ParticlePtr->GetTotalTime() >= ParticlePtr->GetMaxTime()){
					//制限時間になったら、フラグを下げる
					rParticleSprite.m_Active = false;
				}
			}
		}

	}
}
//endof  basedx11
