#pragma once

#include "stdafx.h"

namespace basedx11{
	//プレイヤーの宣言
	class Player;
	//--------------------------------------------------------------------------------------
	//	class ItemInterface;
	//	用途: 振り分けするインターフェイス
	//--------------------------------------------------------------------------------------
	class ItemInterface{
	protected:
		ItemInterface(){}
		virtual ~ItemInterface(){}
	public:
		virtual void ItemAction(const shared_ptr<Player>& ply) = 0;
		//再初期化
		virtual void Refresh(const Vector3& Scale, const Vector3& Rotation, const Vector3& Position) = 0;

	};

	

	//--------------------------------------------------------------------------------------
	//	class ItemTest : public GameObject,public AssignInterface;
	//	用途: 出現後待機、取得するとライフが回復するアイテム
	//--------------------------------------------------------------------------------------
	class ItemTest : public GameObject, public ItemInterface{
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
		float CountTime;
	public:
		//構築と破棄
		ItemTest(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
			);
		virtual ~ItemTest();
		//初期化
		virtual void OnCreate() override;
		//再初期化
		void Refresh(const Vector3& Scale, const Vector3& Rotation, const Vector3& Position)override;
		//衝突&操作
		virtual void ItemAction(const shared_ptr<Player>& ply)override;

		//時間経過測定用
		float ElapsedTime_ReDraw;
		//再描画
		float ReDraw;
		
		//Effect生成
		void Effect_Heart();
		float Timer;

		//ターンの最終更新時
		virtual void OnLastUpdate() override;
	};
	//--------------------------------------------------------------------------------------
	//	class Item_PowerUP : public GameObject,public AssignInterface;
	//	用途: 出現後待機、取得すると音波の設置数が増加するアイテム
	//--------------------------------------------------------------------------------------
	class Item_PowerUP : public GameObject, public ItemInterface{
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
		float CountTime;
	public:
		//構築と破棄
		Item_PowerUP(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
			);
		virtual ~Item_PowerUP();
		//初期化
		virtual void OnCreate() override;
		//再初期化
		void Refresh(const Vector3& Scale, const Vector3& Rotation, const Vector3& Position)override;
		//衝突&操作
		virtual void ItemAction(const shared_ptr<Player>& ply)override;

		//Effect生成
		void Effect_Circle();

		//ターンの最終更新時
		virtual void OnLastUpdate() override;
	};
	//--------------------------------------------------------------------------------------
	//	class Item_RandomBox : public GameObject,public AssignInterface;
	//	用途: 出現後待機、取得すると回復・パワーアップのどちらかが行われる
	//--------------------------------------------------------------------------------------
	class Item_RandomBox : public GameObject, public ItemInterface{
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
		float CountTime;
	public:
		//構築と破棄
		Item_RandomBox(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
			);
		virtual ~Item_RandomBox();
		//初期化
		virtual void OnCreate() override;
		//再初期化
		void Refresh(const Vector3& Scale, const Vector3& Rotation, const Vector3& Position)override;
		//衝突&操作
		virtual void ItemAction(const shared_ptr<Player>& ply)override;

		//時間経過測定用
		float ElapsedTime_ReDraw;
		//再描画
		float ReDraw;

	};
	//--------------------------------------------------------------------------------------
	//	class ItemManager
	//	用途: 描画を切ったアイテムを再描画する
	//--------------------------------------------------------------------------------------
	class ItemManager : public GameObject{

	public:
		ItemManager(const shared_ptr<Stage>& StagePtr);
		//構築と破棄
		virtual ~ItemManager(){}
		//タイムカウンター
		float m_RePopTime;
		//Itemの更新と描画が切れていないか監視
		void OnUpdate();

		//各アイテムリスト
		list<weak_ptr<GameObject>> m_ManageItemList;
		//当たったboxのリスト返すよ with ALLItem
		list<weak_ptr<GameObject>>& GetList_ManageItemList(){
			return m_ManageItemList;
		}
	};

}
//endof  basedx11