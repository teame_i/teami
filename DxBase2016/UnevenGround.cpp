#include "stdafx.h"
#include "Project.h"


namespace basedx11{

	//--------------------------------------------------------------------------------------
	//	class UnevenGround : public GameObject;
	//	用途: でこぼこのグランド
	//--------------------------------------------------------------------------------------

	//メッシュリソースの作成
	void UnevenGround::CreateMeshResource(){
		vector<VertexPositionNormalTexture> vertices;
		vector<uint16_t> indices;

		vector<vector<Vector3>> PosBase;

		float StartZ = -((float)m_Height / 2.0f);
		float EndZ = (float)m_Height / 2.0f;
		float SpanZ = (float)m_Height / (float)m_Division;

		float StartX = -((float)m_Width / 2.0f);
		float EndX = (float)m_Width / 2.0f;
		float SpanX = (float)m_Width / (float)m_Division;


		for (float z = StartZ; z <= EndZ; z += SpanZ){
			vector<Vector3> PosBaseSub;
			PosBaseSub.clear();
			for (float x = StartX; x <= EndX; x += SpanX){
				PosBaseSub.push_back(Vector3(x, 0.0f, z));
			}
			PosBase.push_back(PosBaseSub);
		}

		uint16_t index = 0;
		for (size_t z = 0; z < PosBase.size() - 1; z++){
			for (size_t x = 0; x < PosBase[z].size() - 1; x++){
				VertexPositionNormalTexture v;
				//0
				v.position = PosBase[z][x];
				v.normal = Vector3(0, 1.0f, 0);
				v.textureCoordinate = Vector2(0, 1.0f);
				vertices.push_back(v);
				indices.push_back(index);
				index++;

				//1
				v.position = PosBase[z + 1][x];
				v.normal = Vector3(0, 1.0f, 0);
				v.textureCoordinate = Vector2(0, 0);
				vertices.push_back(v);
				indices.push_back(index);
				index++;

				//2
				v.position = PosBase[z + 1][x + 1];
				v.normal = Vector3(0, 1.0f, 0);
				v.textureCoordinate = Vector2(1.0f, 0);
				vertices.push_back(v);
				indices.push_back(index);
				index++;

				//3
				v.position = PosBase[z][x];
				v.normal = Vector3(0, 1.0f, 0);
				v.textureCoordinate = Vector2(0, 1.0f);
				vertices.push_back(v);
				indices.push_back(index);
				index++;

				//4
				v.position = PosBase[z + 1][x + 1];
				v.normal = Vector3(0, 1.0f, 0);
				v.textureCoordinate = Vector2(1.0f, 0);
				vertices.push_back(v);
				indices.push_back(index);
				index++;

				//5
				v.position = PosBase[z][x + 1];
				v.normal = Vector3(0, 1.0f, 0);
				v.textureCoordinate = Vector2(1.0f, 1.0f);
				vertices.push_back(v);
				indices.push_back(index);
				index++;

			}
		}

		//
		//頂点とインデックスの配列からメッシュリソースを作成（頂点を変更できる）
		m_MeshResource = MeshResource::CreateMeshResource(vertices, indices, true);

	}

	//メッシュの更新
	void UnevenGround::UpdateMeshResource(){

		//波の情報の配列
		vector<WaveData> WaveVec;
		//現在ゲーム上で動いている全ての波の情報を集める（これによって、WaveVec に波の情報が入ります）。
		CheckAllActiveWave(WaveVec);

		//床の行列を得る
		auto Mat = GetComponent<Transform>()->GetWorldMatrix();

		//座標を変更する
		auto Dev = App::GetApp()->GetDeviceResources();
		ID3D11Device* pDx11Device = Dev->GetD3DDevice();
		ID3D11DeviceContext* pID3D11DeviceContext = Dev->GetD3DDeviceContext();
		//頂点バッファをリソースから取り出す
		auto pVertexBuffer = m_MeshResource->GetVertexBuffer().Get();
		//バックアップの頂点を取り出す
		auto& BacukVertices = m_MeshResource->GetBackupVerteces<VertexPositionNormalTexture>();

		//D3D11_MAP_WRITE_DISCARDは重要。この処理により、GPUに邪魔されない
		D3D11_MAP mapType = D3D11_MAP_WRITE_DISCARD;
		D3D11_MAPPED_SUBRESOURCE mappedBuffer;
		//頂点のマップ
		if (FAILED(pID3D11DeviceContext->Map(pVertexBuffer, 0, mapType, 0, &mappedBuffer))){
			// Map失敗
			throw BaseException(
				L"頂点のMapに失敗しました。",
				L"if(FAILED(pID3D11DeviceContext->Map()))",
				L"UnevenGround::UpdateMeshResource()"
				);
		}
		//頂点の変更
		VertexPositionNormalTexture* vertices
			= (VertexPositionNormalTexture*)mappedBuffer.pData;

		//-----------------
		static float num = 0.0f;
		for (size_t i = 0; i < m_MeshResource->GetNumVertices(); i++){
			Vector3 Pos = BacukVertices[i].position;
			//頂点のワールド上の位置を得る
			Vector3 RealPos = Vector3EX::Transform(Pos, Mat);
			RealPos.y = 0;

			//頂点に、どれだけの波の力が加わったかを計算する
			float power = CalcWavePower(RealPos, WaveVec);

			//Ｙ座標（波の高さ）を計算する
			Pos.y = power;

			vertices[i] = VertexPositionNormalTexture(
				Pos,
				BacukVertices[i].normal,
				BacukVertices[i].textureCoordinate
				);

		}
		//アンマップ
		pID3D11DeviceContext->Unmap(pVertexBuffer, 0);
	}

	//構築と破棄
	UnevenGround::UnevenGround(const shared_ptr<Stage>& StagePtr, UINT Width, UINT Height, UINT Division) :
		GameObject(StagePtr),
		m_Width(Width), m_Height(Height), m_Division(Division)
	{}
	//初期化
	void UnevenGround::OnCreate(){
		//でこぼこ床のグループを得る
		auto Group = GetStage()->GetSharedObjectGroup(L"UnevenGroundGroup");
		//グループに自分自身を追加
		Group->IntoGroup(GetThis<UnevenGround>());

		auto Ptr = AddComponent<Transform>();
		Ptr->SetScale(Vector3(1.0f, 1.0f, 1.0f));
		Ptr->SetPosition(Vector3(10.0f, -0.1f, 10.0f));
		Ptr->SetRotation(Vector3(0.0f, 0.0f, 0.0f));
		//頂点バッファとインデックスバッファを作成
		CreateMeshResource();
		//描画コンポーネントの追加
		auto DrawComp = AddComponent<PNTStaticDraw>();
		//描画コンポーネントに形状（メッシュ）を設定
		DrawComp->SetMeshResource(m_MeshResource);

		//描画コンポーネントテクスチャの設定
		DrawComp->SetTextureResource(L"SoundWave_TX");
		DrawComp->SetDiffuse(Color4(1.0f, 1.0f, 1.0f, 0.9f));
		SetAlphaActive(true);

		SetDrawLayer(-1);

		//SEの設定
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"Lightning");

	}
	
	void UnevenGround::OnUpdate(){
		//メッシュの頂点を更新する（波を表現する）。
		UpdateMeshResource();
	}

	void UnevenGround::OnLastUpdate(){
		m_CrossVec3Vec.clear();
		vector<WaveData> ActVec;
		CheckAllActiveWave(ActVec);
		if (ActVec.size() > 1){
			for (UINT i = 0; i < ActVec.size() - 1; i++){
				for (UINT j = i + 1; j < ActVec.size(); j++){ 
					vector<Vector3> TempCrossVec3Vec;
					TempCrossVec3Vec.clear();
					if (CollitionWaveWave(ActVec[i], ActVec[j], TempCrossVec3Vec)){
						for (auto v : TempCrossVec3Vec){
							m_CrossVec3Vec.push_back(v);
						}
					}
				} 
			}
		}
		//干渉点の場所にエフェクトを生成する
		for (auto CrossPos : m_CrossVec3Vec){
			auto Group = GetStage()->GetSharedObjectGroup(L"Wave");
			auto WaveVec = Group->GetGroupVector();
			for (auto Ptr : WaveVec){
				if (Ptr.expired()){
					continue;
				}
				auto WavePtr = dynamic_pointer_cast<Wave>(Ptr.lock());

				if (WavePtr){
					if (WavePtr->WaveUpde){
						auto PtrSpark = GetStage()->GetSharedGameObject<MultiCross>(L"MultiCross", false);
						PtrSpark->InsertSpark(CrossPos);
					}
				}
			}
		}
	}

	//このでこぼこ床の範囲内ならtrue
	//戻り値がtrueならPositionとY軸で交差する三角形を返す
	bool UnevenGround::GetTriangle(const Vector3& Position, vector<Vector3>& RetVec){
		return false;

	}

	//受け取った座標と、受け取った波の情報で、座標にどれだけの力が加わったかを計算する
	float UnevenGround::CalcWavePower(const Vector3& Pos, const vector<WaveData>& WaveVec){
		/*--------------------------------------------------
		1. 「頂点」と「波の中心」の距離を測る。
		2. 「頂点と中心の距離」から、「波の半径」を引く。
		3. 「波の強い部分」との距離が分かる。
		--------------------------------------------------*/
		//最終的に加わりきった力。
		float resultPower = 0.0f;

		//全ての波と計算を行う。
		for (auto& data : WaveVec){
			//「頂点」と「波の中心」の距離を測る。
			float span = Vector3EX::Length(Pos - data.Pos);
			//「頂点と中心の距離」から、「波の半径」を引く。
			 float power = span - data.r;

			//数値をプラスに固定する（波の内側だとマイナスになってしまうため）。
			 power = abs(power);

			//力が距離に反比例するように計算する。
			 power *= 6.0f;
			 if (power < XM_PI){
				 power = (1 + cos(power)) * 0.5f * data.Hight;//最後の値を波から取得する
			}
			else{
				power = 0.0f;
			}

			//力を加える
			resultPower += power;
		}

		return resultPower;
	}

	//現在ゲーム上で動いている波を全て調べ、その波を返す
	void UnevenGround::CheckAllActiveWave(vector<WaveData>& RetVec){
		//現在ゲーム上で動いている波紋オブジェクトを全て取得する。
		auto Group = GetStage()->GetSharedObjectGroup(L"Wave");
		auto WaveVec = Group->GetGroupVector();
		for (auto Ptr : WaveVec){
			//Ptrはweak_ptrなので有効性チェックが必要。無効なら次のループへ。
			if (Ptr.expired()){
				continue;
			}
			//波クラスに動的キャスト。失敗したら次のループへ。
			auto WavePtr = dynamic_pointer_cast<Wave>(Ptr.lock());
			if (!WavePtr){
				continue;
			}

			//UpdateとDrawを行っている（ゲーム上で動いている）波があったら、座標と半径の情報を取得する。
			if (WavePtr->IsUpdateActive() && WavePtr->IsDrawActive()){
				auto PtrTrans = Ptr.lock()->GetComponent<Transform>();
				auto Pos = PtrTrans->GetPosition();
				auto Hight = WavePtr->GetWavePower();
				RetVec.push_back(WaveData(Pos, WavePtr->GetRadius() * 0.5f, Hight));
			}
		}
	}

	//波と波が重なったら
	bool UnevenGround::CollitionWaveWave(const WaveData& d1, const WaveData& d2, vector<Vector3>& RetVec){

		//波の座標と半径を取得
		Vector3 D1_pos = d1.Pos;
		Vector3 D2_pos = d2.Pos;
		float	D1_r = d1.r;
		float	D2_r = d2.r;

		//2つの円の距離を測る
		float Length = Vector3EX::Length(D1_pos - D2_pos);
		//角度を求める
		float a = atan2(D2_pos.z - D1_pos.z, D2_pos.x - D1_pos.x);

		float seed_s = (Length * Length + D2_r * D2_r - D1_r * D1_r) / (2 * Length * D2_r);

		//余弦定理
		float s = acos(seed_s);

		if (Length < D1_r + D2_r) {

			//円は交わっている
			//交点は2つ

			//交点の座標をもとめる
			Vector3 P = Vector3(D1_r*cos(a + s), 0.125, D1_r*sin(a + s));
			P += D1_pos;
			Vector3 I = Vector3(D1_r*cos(a - s), 0.125, D1_r*sin(a - s));
			I += D1_pos;

			RetVec.push_back(P);
			RetVec.push_back(I);
			return true;

		}
		else if (Length == D1_r + D2_r) {
			//円は外接か内接し一致せず
			//交点は1つ	
			Vector3 P = Vector3(D1_r*cos(a + s), 0.125, D1_r*sin(a + s));
			RetVec.push_back(P);
			return true;

		}
		return false;
	}
}
//endof  basedx11
