#pragma once

#include"stdafx.h"

namespace basedx11{

	class Enemy;
	//-----------------------------------------------------------------------
	//  class MotionManager
	// 用途　エネミーの行動関数を管理する。
	//-----------------------------------------------------------------------
	class MotionManager : public GameObject{
	protected:
		//MotionManager(){}
		//virtual ~MotionManager(){}
	public:
		MotionManager(const shared_ptr<Stage>& StagePtr);
		virtual ~MotionManager();

		//時間加算用変数エラプスドタイム
		float ElapsedTime = App::GetApp()->GetElapsedTime();

		//モーション関数呼び出し
		void CallMotion(int num, const shared_ptr<GameObject>& obj);

		float AttackWait;

		////////////////////
		//モーション関数群//
		////////////////////

		//行動関数-待機
		void WaitMotion(const shared_ptr<GameObject>& obj);

		//行動関数-移動
		void MoveMotion(const shared_ptr<GameObject>& obj);

		//行動関数-その場で音波攻撃
		void SonarAttack();

		//波のシェアードポインタ
		shared_ptr<GameObject> m_wave;
	};
}