#pragma once

#include "stdafx.h"

namespace basedx11{

	//--------------------------------------------------------------------------------------
	//	class FixedBox : public GameObject;
	//	用途: ステージのガイド線
	//--------------------------------------------------------------------------------------
	class FixedBox : public GameObject{
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		FixedBox(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
			);
		virtual ~FixedBox();
		//初期化
		virtual void OnCreate() override;
		virtual void OnLastUpdate() override;
		//操作
	};

	//--------------------------------------------------------------------------------------
	//	struct InstanceConstantBuffer;
	//	用途: インスタンスシャドウマップ用コンスタントバッファ構造体
	//--------------------------------------------------------------------------------------
	struct InstanceShadowmapConstantBuffer
	{
		XMMATRIX mView;
		XMMATRIX mProj;
		InstanceShadowmapConstantBuffer() {
			memset(this, 0, sizeof(InstanceShadowmapConstantBuffer));
		};
	};
	//シェーダ宣言(マクロ使用)
	DECLARE_DX11_CONSTANT_BUFFER(CBInstanceShadowmap, InstanceShadowmapConstantBuffer)
	DECLARE_DX11_VERTEX_SHADER(VSInstanceShadowmap, VertexPositionNormalTextureMatrix)

	//--------------------------------------------------------------------------------------
	//	struct InstanceConstantBuffer;
	//	用途: インスタンス描画用コンスタントバッファ構造体
	//--------------------------------------------------------------------------------------
	struct InstanceConstantBuffer
	{
		Matrix4X4 View;
		Matrix4X4 Projection;
		Vector4 LightDir;
		Vector4 Param;	//汎用パラメータ
		Vector4 LPos;
		Vector4 EyePos;
		Matrix4X4 LView;
		Matrix4X4 LProjection;
		InstanceConstantBuffer() {
			memset(this, 0, sizeof(InstanceConstantBuffer));
		};
	};
	//シェーダ宣言(マクロ使用)
	DECLARE_DX11_CONSTANT_BUFFER(CBInstance, InstanceConstantBuffer)
	DECLARE_DX11_VERTEX_SHADER(VSInstance, VertexPositionNormalTextureMatrix)
	DECLARE_DX11_PIXEL_SHADER(PSInstance)


	//--------------------------------------------------------------------------------------
	//	class InstanceShadowmap : public Shadowmap;
	//	用途: インスタンスシャドウマップコンポーネント
	//--------------------------------------------------------------------------------------
	class InstanceShadowmap : public Shadowmap{
	public:
		explicit InstanceShadowmap(const shared_ptr<GameObject>& GameObjectPtr);
		virtual ~InstanceShadowmap();
		//操作
		virtual void OnDraw()override;
	};



	//--------------------------------------------------------------------------------------
	//	class DrawBoxManager : public GameObject;
	//	用途: ステージのガイド線の描画マネージャ
	//--------------------------------------------------------------------------------------
	class DrawBoxManager : public GameObject{
		//インスタンスの最大値
		const size_t m_MaxInstance;
		wstring m_MeshKey;
		wstring m_TextureKey;
		vector<Matrix4X4> m_InstanceVec;
		//行列用の頂点バッファ
		ComPtr<ID3D11Buffer> m_MatrixBuffer;
		//行列用の頂点バッファの作成
		void CreateMatrixBuffer();
	public:
		//構築と破棄
		DrawBoxManager(const shared_ptr<Stage>& StagePtr, size_t MaxInstance, const wstring& MeshKey, const wstring& TextureKey);
		virtual ~DrawBoxManager();
		//初期化
		virtual void OnCreate() override;
		//アクセサ
		const ComPtr<ID3D11Buffer>& GetMatrixBuffer() const{
			return m_MatrixBuffer;
		}
		const vector<Matrix4X4>& GetInstanceVec(){
			return m_InstanceVec;
		}
		//操作
		//スケール、回転、位置で追加
		void AddInstanceVectors(const Vector3& Scale, const Vector3& Rot, const Vector3& Pos);
		//行列で追加
		void AddInstanceMatrix(const Matrix4X4& Mat);
		//仮想関数
		virtual void OnLastUpdate() override;
		virtual void OnDraw() override;
	};


	//--------------------------------------------------------------------------------------
	//	class FixedBoxManager : public GameObject;
	//	用途: ステージのガイド線のマネージャ（描画する）
	//--------------------------------------------------------------------------------------
	class FixedBoxManager : public GameObject{
		//頂点とインデックスを行列と、ナンバーに合わせて変更する
		void ChangeVertex(vector<VertexPositionNormalTexture>& vertices, vector<uint16_t>& indices, const Matrix4X4& Mat, size_t Num);
		//リソース
		shared_ptr<MeshResource> m_BoxesRes;
	public:
		//構築と破棄
		FixedBoxManager(shared_ptr<Stage>& StagePtr);
		virtual ~FixedBoxManager();
		//初期化
		virtual void OnCreate() override;
	};

	//--------------------------------------------------------------------------------------
	//	class TitelSprite : public GameObject;
	//	用途: 配置スプライト(タイトルスプライト)
	//--------------------------------------------------------------------------------------
	class TitelSprite : public GameObject{
		Vector3 m_StartPos;
		vector< vector<VertexPositionColorTexture> > m_BarBurtexVec;

	public:
		//構築と破棄
		TitelSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~TitelSprite();
		//初期化
		virtual void OnCreate() override;

	};


	//--------------------------------------------------------------------------------------
	//	class EasySprite : public GameObject;
	//	用途: 配置スプライト(Easyスプライト)
	//--------------------------------------------------------------------------------------
	class EasySprite : public GameObject{
		Vector3 m_StartPos;
		vector< vector<VertexPositionColorTexture> > m_BarBurtexVec;

	public:
		//構築と破棄
		EasySprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~EasySprite();
		
		void Rifmotion();

		//初期化
		virtual void OnCreate() override;
		virtual void OnUpdate() override;

		void Start();

	};

	//--------------------------------------------------------------------------------------
	//	class MenuSprite : public GameObject;
	//	用途: 配置スプライト(メニュースプライト)
	//--------------------------------------------------------------------------------------
	class MenuSprite : public GameObject{
		Vector3 m_StartPos;
		vector< vector<VertexPositionColorTexture> > m_BarBurtexVec;

	public:
		//構築と破棄
		MenuSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~MenuSprite();
		//初期化
		virtual void OnCreate() override;

	};

	//--------------------------------------------------------------------------------------
	//	class ClearSprite : public GameObject;
	//	用途: 配置スプライト（クリアスプライト）
	//--------------------------------------------------------------------------------------
	class ClearSprite : public GameObject{
		Vector3 m_StartPos;
		vector< vector<VertexPositionColorTexture> > m_BarBurtexVec;

	public:
		//構築と破棄
		ClearSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~ClearSprite();
		//初期化
		virtual void OnCreate() override;

	};

	//--------------------------------------------------------------------------------------
	//	class OverSprite : public GameObject;
	//	用途: 配置スプライト（ゲームオーバースプライト）
	//--------------------------------------------------------------------------------------
	class OverSprite : public GameObject{
		Vector3 m_StartPos;
		vector< vector<VertexPositionColorTexture> > m_BarBurtexVec;

	public:
		//構築と破棄
		OverSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~OverSprite();
		//初期化
		virtual void OnCreate() override;

	};

	//--------------------------------------------------------------------------------------
	//	class ToMenuSprite : public GameObject;
	//	用途: 配置スプライト（メニュースプライト）
	//--------------------------------------------------------------------------------------
	class ToMenuSprite : public GameObject{
		Vector3 m_StartPos;
		vector< vector<VertexPositionColorTexture> > m_BarBurtexVec;

	public:
		//構築と破棄
		ToMenuSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~ToMenuSprite();
		//初期化
		virtual void OnCreate() override;

	};

	//--------------------------------------------------------------------------------------
	//	class StartSprite : public GameObject;
	//	用途: 配置スプライト(ゲームスタートスプライト)
	//--------------------------------------------------------------------------------------
	class StartSprite : public GameObject{
		Vector3 m_StartPos;
		vector< vector<VertexPositionColorTexture> > m_BarBurtexVec;
		Color4 m_color;

		float m_Max;
		float m_Min;

		float m_Speed;

	public:
		//構築と破棄
		StartSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~StartSprite();

		void SetSpeed(float);
		//初期化
		virtual void OnCreate() override;
		virtual void OnUpdate() override;

	};


	//--------------------------------------------------------------------------------------
	//	class NormalSprite : public GameObject;
	//	用途: 配置スプライト(Normalスプライト)
	//--------------------------------------------------------------------------------------
	class NormalSprite : public GameObject{
		Vector3 m_StartPos;
		vector< vector<VertexPositionColorTexture> > m_BarBurtexVec;

	public:
		//構築と破棄
		NormalSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~NormalSprite();
		void Rifmotion();
		//初期化
		virtual void OnCreate() override;

	};

	//--------------------------------------------------------------------------------------
	//	class HardSprite : public GameObject;
	//	用途: 配置スプライト(Hardスプライト)
	//--------------------------------------------------------------------------------------
	class HardSprite : public GameObject{
		Vector3 m_StartPos;
		vector< vector<VertexPositionColorTexture> > m_BarBurtexVec;

	public:
		//構築と破棄
		HardSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~HardSprite();
		void Rifmotion();

		//初期化
		virtual void OnCreate() override;

	};

	//--------------------------------------------------------------------------------------------
	//class AlretSprite : public GameObject
	//警告スプライトの作成
	//--------------------------------------------------------------------------------------------
	class AlretSprite : public GameObject{
		shared_ptr< StateMachine<AlretSprite> >  m_StateMachine;	//ステートマシーン
		Vector3 m_StartPos;
		vector< vector<VertexPositionColorTexture> > m_BarBurtexVec;
		int m_HP;
		Color4 m_color;

		float m_Max;
		float m_Min;

		float m_Speed;


	public:
		//構築と破棄
		AlretSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~AlretSprite();

		shared_ptr< StateMachine<AlretSprite> > GetStateMachine() const{
			return m_StateMachine;
		}

		Color4 GetColor(){
			return m_color;
		}

		void SetColor(Color4 color){
			m_color = color;
		}

		void SetMaxAlpha(float w){
			m_Max = w;
		}
		void SetMinAlpha(float w){
			m_Min = w;
		}
		float GetMaxAlpha(){
			return m_Max;
		}
		float GetMinAlpha(){
			return m_Min;
		}

		float GetSpeed(){
			return m_Speed;
		}

		void CState();
		void RState();

		//初期化
		virtual void OnCreate() override;
		virtual void OnUpdate() override;

		void SetHP(int hp);
		void SetSpeed(float speed);

	};

	//--------------------------------------------------------------------------------------
	//	class NomalState : public ObjState<AlretSprite>;
	//	用途: 通常状態
	//--------------------------------------------------------------------------------------
	class NomalState : public ObjState<AlretSprite>
	{
		NomalState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<NomalState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<AlretSprite>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<AlretSprite>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<AlretSprite>& Obj)override;
	};
	//--------------------------------------------------------------------------------------
	//	class FlashingState : public ObjState<AlretSprite>;
	//	用途:　警告状態（スプライトを表示させ点滅させる）
	//--------------------------------------------------------------------------------------
	class FlashingState : public ObjState<AlretSprite>
	{
		FlashingState(){}
	public:

		//ステートのインスタンス取得
		static shared_ptr<FlashingState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<AlretSprite>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<AlretSprite>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<AlretSprite>& Obj)override;
	};

	//--------------------------------------------------------------------------------------------
	//class ReTuroSprite : public GameObject
	//『ReTutoSprite』ゲームの最初に出る注意事項のスプライトの表示
	//--------------------------------------------------------------------------------------------
	class ReTuroSprite : public GameObject{
		Vector3 m_StartPos;
		vector< vector<VertexPositionColorTexture> > m_BarBurtexVec;
		Color4 m_color;

		float m_Max;
		float m_Min;

		float m_Speed;

	public:
		//構築と破棄
		ReTuroSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~ReTuroSprite();

		void SetSpeed(float);

		//点滅に関するカウント
		int Flash_Count;

		//初期化
		virtual void OnCreate() override;
		virtual void OnUpdate() override;

	};

	//--------------------------------------------------------------------------------------
	//class MultiEfect : public MultiParticle;
	//用途: 複数のエフェクトクラス
	//--------------------------------------------------------------------------------------
	class MultiEfect : public MultiParticle{
	public:
		//構築と破棄
		MultiEfect(shared_ptr<Stage>& StagePtr);
		virtual ~MultiEfect();
		//--------------------------------------------------------------------------------------
		//	void InsertSpark(
		//	const Vector3& Pos	//放出位置（エミッター位置）
		//	);
		//用途: スパークを放出する
		//戻り値: なし。
		//--------------------------------------------------------------------------------------
		void InsertSpark(const Vector3& Pos);
	};

	//--------------------------------------------------------------------------------------
	//class MultiSpark : public MultiParticle;
	//用途: 複数のスパーククラス
	//--------------------------------------------------------------------------------------
	class MultiSpark : public MultiParticle{
	public:
		//構築と破棄
		MultiSpark(shared_ptr<Stage>& StagePtr);
		virtual ~MultiSpark();
		//--------------------------------------------------------------------------------------
		//	void InsertSpark(
		//	const Vector3& Pos	//放出位置（エミッター位置）
		//	);
		//用途: スパークを放出する
		//戻り値: なし。
		//--------------------------------------------------------------------------------------
		void InsertSpark(const Vector3& Pos);
	};

	//--------------------------------------------------------------------------------------
	//class MultiSpark_jr : public MultiParticle;
	//用途: 複数の小規模なスパーククラス
	//--------------------------------------------------------------------------------------
	class MultiSpark_jr : public MultiParticle{
	public:
		//構築と破棄
		MultiSpark_jr(shared_ptr<Stage>& StagePtr);
		virtual ~MultiSpark_jr();
		//--------------------------------------------------------------------------------------
		//	void InsertSpark(
		//	const Vector3& Pos	//放出位置（エミッター位置）
		//	);
		//用途: 小規模なスパークを放出する
		//戻り値: なし。
		//--------------------------------------------------------------------------------------
		virtual void OnUpdate() override;
		void InsertSpark(const Vector3& Pos, UINT num);
	};

	//--------------------------------------------------------------------------------------
	//class MultiSpark_Heart : public MultiParticle;
	//用途: 複数の小規模なハートのスパーククラス
	//--------------------------------------------------------------------------------------
	class MultiSpark_Heart : public MultiParticle{
	public:
		//構築と破棄
		MultiSpark_Heart(shared_ptr<Stage>& StagePtr);
		virtual ~MultiSpark_Heart();

		virtual void OnUpdate() override;

		//アクセサ：受け取った座標にパーティクルを生成する。
		void InsertSpark(const Vector3& Pos, UINT num);

	};

	//--------------------------------------------------------------------------------------
	//class MultiSpark_Circle: public MultiParticle;
	//用途:パーワーアップアイテムのエフェクト
	//--------------------------------------------------------------------------------------
	class MultiSpark_Circle : public MultiParticle{
	public:
		//構築と破棄
		MultiSpark_Circle(shared_ptr<Stage>& StagePtr);
		virtual ~MultiSpark_Circle();
		//--------------------------------------------------------------------------------------
		//	void InsertSpark(
		//	const Vector3& Pos	//放出位置（エミッター位置）
		//	);
		//用途: 小規模なCircleスパークを放出する
		//戻り値: なし。
		//--------------------------------------------------------------------------------------
		void InsertSpark(const Vector3& Pos);
	};

	//--------------------------------------------------------------------------------------
	//class MultiSpark_STAN : public MultiParticle;
	//用途: 麻痺中に出るエフェクト
	//--------------------------------------------------------------------------------------
	class MultiSpark_STAN : public MultiParticle{
	public:
		//構築と破棄
		MultiSpark_STAN(shared_ptr<Stage>& StagePtr);
		virtual ~MultiSpark_STAN();

		virtual void OnUpdate() override;

		//アクセサ：受け取った座標にパーティクルを生成する。
		void InsertSpark(const Vector3& Pos, UINT num);

	};

	//--------------------------------------------------------------------------------------
	//class MultiCross : public MultiParticle;
	//用途: 干渉点に出るエフェクト
	//--------------------------------------------------------------------------------------
	class MultiCross : public MultiParticle{
	public:
		//構築と破棄
		MultiCross(shared_ptr<Stage>& StagePtr);
		virtual ~MultiCross();
		//--------------------------------------------------------------------------------------
		//	void InsertSpark(
		//	const Vector3& Pos	//放出位置（エミッター位置）
		//	);
		//用途: スパークを放出する
		//戻り値: なし。
		//--------------------------------------------------------------------------------------
		void InsertSpark(const Vector3& Pos);
		virtual void OnUpdate() override;
	};


	//--------------------------------------------------------------------------------------
	//	class GoToMenuSprite : public GameObject;
	//	用途: 配置スプライト
	//--------------------------------------------------------------------------------------
	class GoToMenuSprite : public GameObject{
		Vector3 m_StartPos;
		vector< vector<VertexPositionColorTexture> > m_BarBurtexVec;

	public:
		//構築と破棄
		GoToMenuSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~GoToMenuSprite();
		//初期化
		virtual void OnCreate() override;
		//更新
		virtual void OnUpdate()override;

	};

	//--------------------------------------------------------------------------------------
	//	class GoToTitleSprite : public GameObject;
	//	用途: 配置スプライト
	//--------------------------------------------------------------------------------------
	class GoToTitleSprite : public GameObject{
		Vector3 m_StartPos;
		vector< vector<VertexPositionColorTexture> > m_BarBurtexVec;

	public:
		//構築と破棄
		GoToTitleSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~GoToTitleSprite();
		//初期化
		virtual void OnCreate() override;

	};

	//--------------------------------------------------------------------------------------
	//	class ScoreSprite : public GameObject;
	//	用途: 配置スプライト(Scoreスプライト)
	//--------------------------------------------------------------------------------------
	class ScoreSprite : public GameObject{
		Vector3 m_StartPos;
		vector< vector<VertexPositionColorTexture> > m_BarBurtexVec;

	public:
		//構築と破棄
		ScoreSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~ScoreSprite();
		//初期化
		virtual void OnCreate() override;

	};

	//--------------------------------------------------------------------------------------
	//	class TIMESprite : public GameObject;
	//	用途: 配置スプライト(Timeスプライト)
	//--------------------------------------------------------------------------------------
	class TIMESprite : public GameObject{
		Vector3 m_StartPos;
		Vector3 m_StartScale;
		vector< vector<VertexPositionColorTexture> > m_BarBurtexVec;

	public:
		//構築と破棄
		TIMESprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos,const Vector3& StartScale);
		virtual ~TIMESprite();
		//初期化
		virtual void OnCreate() override;

	};

	//--------------------------------------------------------------------------------------
	//	class EnemyUISprite : public GameObject;
	//	用途: 配置スプライト(ゲーム画面に出す「Enemy」スプライト)
	//--------------------------------------------------------------------------------------
	class EnemyUISprite : public GameObject{
		Vector3 m_StartPos;
		vector< vector<VertexPositionColorTexture> > m_BarBurtexVec;

	public:
		//構築と破棄
		EnemyUISprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~EnemyUISprite();
		//初期化
		virtual void OnCreate() override;

	};

	//--------------------------------------------------------------------------------------
	//	class RANKSprite : public GameObject;
	//	用途: 配置スプライト(ゲームクリア画面にだす「Rank」スプライト)
	//--------------------------------------------------------------------------------------
	class RANKSprite : public GameObject{
		Vector3 m_StartPos;
		vector< vector<VertexPositionColorTexture> > m_BarBurtexVec;

	public:
		//構築と破棄
		RANKSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~RANKSprite();
		//初期化
		virtual void OnCreate() override;

	};

	//--------------------------------------------------------------------------------------
	//	class RANK_ASprite : public GameObject;
	//	用途: 配置スプライト(ランクAスプライト)
	//--------------------------------------------------------------------------------------
	class RANK_ASprite : public GameObject{
		Vector3 m_StartPos;
		vector< vector<VertexPositionColorTexture> > m_BarBurtexVec;

	public:
		//構築と破棄
		RANK_ASprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~RANK_ASprite();
		//初期化
		virtual void OnCreate() override;

	};

	//--------------------------------------------------------------------------------------
	//	class RANK_BSprite : public GameObject;
	//	用途: 配置スプライト(ランクBスプライト)
	//--------------------------------------------------------------------------------------
	class RANK_BSprite : public GameObject{
		Vector3 m_StartPos;
		vector< vector<VertexPositionColorTexture> > m_BarBurtexVec;

	public:
		//構築と破棄
		RANK_BSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~RANK_BSprite();
		//初期化
		virtual void OnCreate() override;

	};

	//--------------------------------------------------------------------------------------
	//	class RANK_CSprite : public GameObject;
	//	用途: 配置スプライト(ランクCスプライト)
	//--------------------------------------------------------------------------------------
	class RANK_CSprite : public GameObject{
		Vector3 m_StartPos;
		vector< vector<VertexPositionColorTexture> > m_BarBurtexVec;

	public:
		//構築と破棄
		RANK_CSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~RANK_CSprite();
		//初期化
		virtual void OnCreate() override;

	};

	//--------------------------------------------------------------------------------------
	//	class ToTutorialSprite : public GameObject;
	//	用途: 配置スプライト
	//--------------------------------------------------------------------------------------
	class ToTutorialSprite : public GameObject{
		Vector3 m_StartPos;
		vector< vector<VertexPositionColorTexture> > m_BarBurtexVec;

	public:
		//構築と破棄
		ToTutorialSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~ToTutorialSprite();
		//初期化
		virtual void OnCreate() override;
		virtual void OnUpdate() override;

		//画像の切り替え
		bool ChangeSprite();
	};

	//--------------------------------------------------------------------------------------
	//	class ToNextSprite : public GameObject;
	//	用途: 配置スプライト(チュートリアルスプライト『次へ』)
	//--------------------------------------------------------------------------------------
	class ToNextSprite : public GameObject{
		Vector3 m_StartPos;
		vector< vector<VertexPositionColorTexture> > m_BarBurtexVec;
		Color4 m_color;

		float m_Max;
		float m_Min;

		float m_Speed;

	public:
		//構築と破棄
		ToNextSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~ToNextSprite();

		//初期化
		virtual void OnCreate() override;
		virtual void OnUpdate() override;

	};

	//--------------------------------------------------------------------------------------
	//	class ToPrevSprite : public GameObject;
	//	用途: 配置スプライト(チュートリアルスプライト『戻る』)
	//--------------------------------------------------------------------------------------
	class ToPrevSprite : public GameObject{
		Vector3 m_StartPos;
		vector< vector<VertexPositionColorTexture> > m_BarBurtexVec;
		Color4 m_color;

		float m_Max;
		float m_Min;

		float m_Speed;

	public:
		//構築と破棄
		ToPrevSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~ToPrevSprite();

		//初期化
		virtual void OnCreate() override;
		virtual void OnUpdate() override;

	};

	//tuika
	//
	//--------------------------------------------------------------------------------------
	//	class HPBAR_MAX : public GameObject;
	//	用途: 配置スプライト(HPバーマックス状態)
	//--------------------------------------------------------------------------------------
		class HPBAR_MAX : public GameObject{
		Vector3 m_StartPos;
		vector< vector<VertexPositionColorTexture> > m_BarBurtexVec;

	public:
		//構築と破棄
		HPBAR_MAX(shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~HPBAR_MAX();
		//初期化
		virtual void OnCreate() override;

	};

	//--------------------------------------------------------------------------------------
	//	class HPBAR_MID : public GameObject;
	//	用途: 配置スプライト(HPバー1減少状態)
	//--------------------------------------------------------------------------------------
	class HPBAR_MID : public GameObject{
		Vector3 m_StartPos;
		vector< vector<VertexPositionColorTexture> > m_BarBurtexVec;

	public:
		//構築と破棄
		HPBAR_MID(shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~HPBAR_MID();
		//初期化
		virtual void OnCreate() override;

		};

	//--------------------------------------------------------------------------------------
	//	class HPBAR_LAST : public GameObject;
	//	用途: 配置スプライト(HPバー赤ゲージ状態)
	//--------------------------------------------------------------------------------------
	class HPBAR_LAST : public GameObject{
		Vector3 m_StartPos;
		vector< vector<VertexPositionColorTexture> > m_BarBurtexVec;

	public:
		//構築と破棄
		HPBAR_LAST(shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~HPBAR_LAST();
		//初期化
		virtual void OnCreate() override;

	};

	//--------------------------------------------------------------------------------------
	//	class ThxSprite : public GameObject;
	//	用途: 配置スプライト(Thxスプライト)
	//--------------------------------------------------------------------------------------
	class ThxSprite : public GameObject{
		Vector3 m_StartPos;
		vector< vector<VertexPositionColorTexture> > m_BarBurtexVec;

		
	public:
		//構築と破棄
		ThxSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~ThxSprite();

		//初期化
		virtual void OnCreate() override;
		virtual void OnUpdate() override;

		void Start();
		float m_Alpha;
		float set_Alpha;

	};

	//--------------------------------------------------------------------------------------
	//	class BlindSprite : public GameObject;
	//	用途: 配置スプライト(Thxスプライト)
	//--------------------------------------------------------------------------------------
	class BlindSprite : public GameObject{
		Vector3 m_StartPos;
		vector< vector<VertexPositionColorTexture> > m_BarBurtexVec;

	public:
		//構築と破棄
		BlindSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~BlindSprite();

		//初期化
		virtual void OnCreate() override;
	
	};

}
//endof  basedx11
