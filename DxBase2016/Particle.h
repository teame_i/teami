#pragma once

#include "stdafx.h"

namespace basedx11{

	//--------------------------------------------------------------------------------------
	//  class MultiOrb : public MultiParticle;
	//  用途: 複数のオーブ。人が満足しきった時に発生し、名声ゲージに飛んでいきます。
	//--------------------------------------------------------------------------------------
	class MultiOrb : public MultiParticle{
	public:
		//構築と破棄
		MultiOrb(shared_ptr<Stage>& StagePtr);
		virtual ~MultiOrb();

		virtual void OnUpdate() override;

		//アクセサ：受け取った座標にパーティクルを生成する。
		void InsertSpark(const Vector3& Pos, UINT num);

	};

	//--------------------------------------------------------------------------------------
	//  class PowerUpEffect : public MultiParticle;
	//  用途: プレイヤーがパワーアップ状態の間出てくるエフェクト
	//--------------------------------------------------------------------------------------
	class PowerUpEffect : public MultiParticle{
	public:
		//構築と破棄
		PowerUpEffect(shared_ptr<Stage>& StagePtr);
		virtual ~PowerUpEffect();

		virtual void OnUpdate() override;

		//アクセサ：受け取った座標にパーティクルを生成する。
		void InsertSpark(const Vector3& Pos, UINT num);

		float m_Radius;
		float m_Up;
		float Timer;
		bool AddScaleflag;

	};

	//--------------------------------------------------------------------------------------
	//  class HeelEffect : public MultiParticle;
	//  用途: Playerが回復したときに出るエフェクト
	//--------------------------------------------------------------------------------------
	class HeelEffect : public MultiParticle{
	public:
		//構築と破棄
		HeelEffect(shared_ptr<Stage>& StagePtr);
		virtual ~HeelEffect();

		virtual void OnUpdate() override;

		//アクセサ：受け取った座標にパーティクルを生成する。
		void InsertSpark(const Vector3& Pos, UINT num);

	};

	//--------------------------------------------------------------------------------------
	//  class Player_DamageEffect : public MultiParticle;
	//  用途: Playerがダメージを受けた時に出るエフェクト
	//--------------------------------------------------------------------------------------
	class Player_DamageEffect : public MultiParticle{
	public:
		//構築と破棄
		Player_DamageEffect(shared_ptr<Stage>& StagePtr);
		virtual ~Player_DamageEffect();

		virtual void OnUpdate() override;

		//アクセサ：受け取った座標にパーティクルを生成する。
		void InsertSpark(const Vector3& Pos, UINT num);

	};

	//--------------------------------------------------------------------------------------
	//  class BOSS_DamageEffect : public MultiParticle;
	//  用途: BOSSがダメージを受けた時に出るエフェクト
	//--------------------------------------------------------------------------------------
	class BOSS_DamageEffect : public MultiParticle{
	public:
		//構築と破棄
		BOSS_DamageEffect(shared_ptr<Stage>& StagePtr);
		virtual ~BOSS_DamageEffect();

		virtual void OnUpdate() override;

		//アクセサ：受け取った座標にパーティクルを生成する。
		void InsertSpark(const Vector3& Pos);

	};

	//--------------------------------------------------------------------------------------
	//class MultiSpark_Square : public MultiParticle;
	//用途: 複数の小規模なSquareスパーククラス
	//--------------------------------------------------------------------------------------
	class BOSS_Stan_Effect : public MultiParticle{
	public:
		//構築と破棄
		BOSS_Stan_Effect(shared_ptr<Stage>& StagePtr);
		virtual ~BOSS_Stan_Effect();
		//--------------------------------------------------------------------------------------
		//	void InsertSpark(
		//	const Vector3& Pos	//放出位置（エミッター位置）
		//	);
		//用途: 小規模なSquareスパークを放出する
		//戻り値: なし。
		//--------------------------------------------------------------------------------------
		virtual void OnUpdate() override;
		void InsertSpark(const Vector3& Pos);
	};

	//--------------------------------------------------------------------------------------
	//class MultiSpark_Square : public MultiParticle;
	//用途: 複数の小規模なSquareスパーククラス
	//--------------------------------------------------------------------------------------
	class MultiSpark_Square : public MultiParticle{
	public:
		//構築と破棄
		MultiSpark_Square(shared_ptr<Stage>& StagePtr);
		virtual ~MultiSpark_Square();
		//--------------------------------------------------------------------------------------
		//	void InsertSpark(
		//	const Vector3& Pos	//放出位置（エミッター位置）
		//	);
		//用途: 小規模なSquareスパークを放出する
		//戻り値: なし。
		//--------------------------------------------------------------------------------------
		virtual void OnUpdate() override;
		void InsertSpark(const Vector3& Pos, UINT num);
	};
}
//endof  basedx11
