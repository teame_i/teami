#include "stdafx.h"
#include "Project.h"

namespace basedx11{

	MyCamera::MyCamera() :
		Camera(),
		m_RadY(0.5f),
		m_RadXZ(0),
		m_Arm(5.0f),
		m_Leap(1.0f)
	{
	}
	MyCamera::~MyCamera(){}

	shared_ptr<GameObject> MyCamera::GetTargetObject() const{
		if (!m_Target.expired()){
			return m_Target.lock();
		}
		return nullptr;
	}

	void MyCamera::SetTargetObject(const shared_ptr<GameObject>& Obj){
		m_Target = Obj;
	}

	float MyCamera::GetToTargetLerp() const{
		return m_Leap;
	}
	void MyCamera::SetToTargetLerp(float f){
		m_Leap = f;
	}

	void MyCamera::OnUpdate(){
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		//前回のターンからの時間
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		Vector3 NewEye = GetEye();
		Vector3 NewAt = GetAt();
		//計算に使うための腕角度（ベクトル）
		Vector3 ArmVec = NewEye - NewAt;
		//正規化しておく
		ArmVec.Normalize();
		if (CntlVec[0].bConnected){
			//上下角度の変更
			if (CntlVec[0].fThumbRY >= 0.1f){
				m_RadY += 0.06f;
			}
			else if (CntlVec[0].fThumbRY <= -0.1f){
				m_RadY -= 0.06f;
			}
			if (m_RadY > XM_PI / 6.0f){
				m_RadY = XM_PI / 6.0f;
			}
			else if (m_RadY <= 0.1f){
				//カメラが真横になったらそれ以上下がらない
				m_RadY = 0.1f;
			}
			ArmVec.y = sin(m_RadY);
			//ここでY軸回転を作成
			if (CntlVec[0].fThumbRX != 0){
				m_RadXZ += -CntlVec[0].fThumbRX * ElapsedTime * 2.0f;
				//上の2.0は回転スピード
				if (abs(m_RadXZ) >= XM_2PI){
					//1週回ったら0回転にする
					m_RadXZ = 0;
				}
			}
			//クオータニオンでY回転（つまりXZベクトルの値）を計算
			Quaternion QtXZ;
			QtXZ.RotationAxis(Vector3(0, 1.0f, 0), m_RadXZ);
			QtXZ.Normalize();
			//移動先行の行列計算することで、XZの値を算出
			Matrix4X4 Mat;
			Mat.STRTransformation(
				Vector3(1.0f, 1.0f, 1.0f),
				Vector3(0.0f, 0.0f, -1.0f),
				QtXZ
				);

			Vector3 PosXZ = Mat.PosInMatrixSt();
			//XZの値がわかったので腕角度に代入
			ArmVec.x = PosXZ.x;
			ArmVec.z = PosXZ.z;
			//腕角度を正規化
			ArmVec.Normalize();

			auto TargetPtr = GetTargetObject();
			if (TargetPtr){
				//目指したい場所
				Matrix4X4 ToAtMat = TargetPtr->GetComponent<TransformMatrix>()->GetWorldMatrix();
				Vector3 ToAt = ToAtMat.PosInMatrixSt();
				NewAt = Lerp::CalculateLerp(GetAt(), ToAt, 0, 1.0f, m_Leap, Lerp::Linear);
				NewAt.y += 0.2f;
				NewEye.z += 1.0f;
			}
		//目指したい場所にアームの値と腕ベクトルでEyeを調整
			NewEye = NewAt + ArmVec * m_Arm;
		}
		SetEye(NewEye);
		SetAt(NewAt);
		Camera::OnUpdate();
	}

}