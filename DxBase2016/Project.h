#pragma once

namespace basedx11 {

	//オブジェクトのデータID
	enum DataID {
		NULLID = 0,
		BOX,
		ENEMY,
		HEAL,
		POWERUP,
		PLAYER,
		ROAD,
		ENEMYBOSS,
		SPACE
	};

}


#include "resource.h"
#include "Scene.h"
#include "GameStage.h"
#include "Character.h"
#include "Player.h"
#include "Wave.h"
#include "Enemy.h"
#include "Item.h"
#include "Camera.h"
#include "UnevenGround.h"
#include "Time.h"
#include "Particle.h"
#include "MotionManager.h"