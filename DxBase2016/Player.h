#pragma once

#include "stdafx.h"


namespace basedx11{

	class ItemTest;
	class Item_PowerUP;
	class HealArea;
	class Item_RandomBox;

	class Enemy;
	class EnemyBoss;
	//--------------------------------------------------------------------------------------
	//	class Player : public GameObject;
	//	用途: プレイヤー
	//--------------------------------------------------------------------------------------
	class Player : public GameObject{
		shared_ptr< StateMachine<Player> >  m_StateMachine;	//ステートマシーン
		//プレイヤーの座標
		Vector3 m_Position;
		//移動の向きを得る
		Vector3 GetAngle();
		//最高速度
		float m_MaxSpeed;
		//減速率
		float m_Decel;
		//質量
		float m_Mass;
		//波
		shared_ptr<GameObject> m_wave;
	public:
		//構築と破棄
		Player(const shared_ptr<Stage>& StagePtr, Vector3 Position);
		virtual ~Player(){}
		//初期化
		virtual void OnCreate() override;
		//アクセサ
		shared_ptr< StateMachine<Player> > GetStateMachine() const{
			return m_StateMachine;
		}
		//モーションを実装する関数群
		//移動して向きを移動方向にする
		void MoveRotationMotion();

		//Aボタンで波を発生させるかどうかを得る
		bool IsWaveMotion();
		//Aボタンで波を発生させる処理
		void WaveMotion();
		//Aボタン押し続けてるかどうかを得る
		bool IsWaveMove();
		//波のカウント
		int GetWaveCount();
		//波の削除
		void WaveRemove();
		//波の生存時間
		void WaveTime();
		//HP回復アイテム取得した時
		void AddHP();
		//ダメージを受けた時
		void Dameg();

		//波の最大発生数
		int MaxWave;

		//PlayerHP
		int PlayerHP;
		//待機時間
		float Waiting;
		//ステートネーム(現在のステートを記録する)
		wstring SteatName;

		//PowerLimitの制御関数
		void PowerUpTime();
		//PowerUpの制限時間
		float PowerLimit;
		//Counter再初期化
		void TimeReflesh();
		//Waveの再初期化
		void WaveRef();

		//時間加算用変数エラプスドタイム
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		//無敵時間制御
		float MUTEKI_Timer;
		//無敵ステートになったら呼び続ける。一定時間が立ったらステートをデフォルトへ戻す
		void MUTEKI_TIME();
		//無敵時間をリフレッシュ
		void MUTEKI_Reflesh();
		//点滅処理
		void Flash_Player();
		//点滅時間換算用
		float FLASH_TIME;
		//プレイヤーの色を元に戻す
		void Flash_Reflesh();
		void setDEFAULT_COLOR();
		Color4 DEFAULT_DIFFUSE;
		Color4 DEFAULT_EMISSIVE;

		//PowerUpエフェクトの作成
		void PowerUpEffect_Player();
		//回復時エフェクト
		void HeelEffect_Player();

		//衝突した相手による個別処理
		void ActionWithItem(const shared_ptr<ItemTest>& Item);
		void ActionWithItem(const shared_ptr<Item_PowerUP>& Item);
		void ActionWithItem(const shared_ptr<Item_RandomBox>& Item);
		
		//Enemyの衝突
		void ActionWithEnemy(const shared_ptr<Enemy>& Enemy);
		//EnemyBossの衝突
		void ActionWithEnemyBoss(const shared_ptr<EnemyBoss>& EnemyBoss);

		//更新
		virtual void OnUpdate() override;
		//衝突時
		virtual void OnCollision(const shared_ptr<GameObject>& other) override;
		//ターンの最終更新時
		virtual void OnLastUpdate() override;
	};

	//--------------------------------------------------------------------------------------
	//	class DefaultState : public ObjState<Player>;
	//	用途: 通常移動
	//--------------------------------------------------------------------------------------
	class DefaultState : public ObjState<Player>
	{
		DefaultState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<DefaultState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Player>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Player>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Player>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class PowerUpStateState : public ObjState<Player>;
	//	用途: PowerUp状態
	//--------------------------------------------------------------------------------------
	class PowerUpState : public ObjState<Player>
	{
		PowerUpState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<PowerUpState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Player>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Player>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Player>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class MUTEKI_State : public ObjState<Player>;
	//	用途: 無敵状態
	//--------------------------------------------------------------------------------------
	class MUTEKI_State : public ObjState<Player>
	{
		MUTEKI_State(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<MUTEKI_State> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Player>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Player>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Player>& Obj)override;
	};
}
//endof  basedx11
