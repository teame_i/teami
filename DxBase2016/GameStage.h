#pragma once

#include "stdafx.h"

namespace basedx11{
	//--------------------------------------------------------------------------------------
	//	class StareStage : public Stage;
	//	用途: 素材提供画面ステージクラス
	//--------------------------------------------------------------------------------------
	class StareStage : public Stage{
		//リソースの作成
		void CreateResourses();
		//ビューの作成
		void CreateViews();

		//素材提供先スプライトの作成
		void CreateThxSprite();
		//暗幕(黒)スプライトの作成
		void CreateBlindSprite();

	public:
		//構築と破棄
		StareStage() :Stage(){
		}
		virtual ~StareStage(){
		}

		//初期化
		virtual void OnCreate()override;
		//更新
		virtual void OnUpdate() override;

		void DeleteBlind_and_Thx();

		float Timer;

	};

	//--------------------------------------------------------------------------------------
	//	class TitelStage : public Stage;
	//	用途: タイトルステージクラス
	//--------------------------------------------------------------------------------------
	class TitelStage : public Stage{
		shared_ptr< StateMachine<TitelStage> >  m_StateMachine;	//ステートマシーン
		//リソースの作成
		void CreateResourses();
		//ビューの作成
		void CreateViews();
		//タイトルスプライトの作成
		void CreateTitel();
		//スタートスプライトの作成
		void CreateStart();

		//素材提供先スプライトの作成
		void CreateThxSprite();
		//暗幕(黒)スプライトの作成
		void CreateBlindSprite();

		
		//BGMの再生
		void AddMusic();

	public:
		//構築と破棄
		TitelStage() :Stage(),Timer(0){
		}
		virtual ~TitelStage(){
		}
		//アクセサ
		shared_ptr< StateMachine<TitelStage> > GetStateMachine() const{
			return m_StateMachine;
		}

		//ステージ番号
		int StageNumber;

		//初期化
		virtual void OnCreate()override;
		//更新
		virtual void OnUpdate() override;
		bool StartMotion();
		void SceneMotion();

		void DeleteBlind_and_Thx();

		float Timer;
		//素材提供画面制御
		void OneShot_thx();
		bool thxflag;

	};

	//--------------------------------------------------------------------------------------
	//	class TitelState : public ObjState<TitelStage>;
	//	用途: スタートステート
	//--------------------------------------------------------------------------------------
	class TitelState : public ObjState<TitelStage>
	{
		TitelState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<TitelState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<TitelStage>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<TitelStage>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<TitelStage>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class StartState : public ObjState<TitelStage>;
	//	用途: スタートステート
	//--------------------------------------------------------------------------------------
	class StartState : public ObjState<TitelStage>
	{
		StartState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<StartState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<TitelStage>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<TitelStage>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<TitelStage>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class MenuStage : public Stage;
	//	用途: メニューステージクラス
	//--------------------------------------------------------------------------------------
	class MenuStage : public Stage{
		shared_ptr< StateMachine<MenuStage> >  m_StateMachine;	//ステートマシーン
		//リソースの作成
		void CreateResourses();
		//ビューの作成
		void CreateViews();
		//メニュースプライトの作成
		void CreateEasy();
		void CreateNormal();
		void CreateHard();
		void CreateMenu();

	public:
		//構築と破棄
		MenuStage() :Stage(){}
		virtual ~MenuStage(){
		}
		//アクセサ
		shared_ptr< StateMachine<MenuStage> > GetStateMachine() const{
			return m_StateMachine;
		}

		//ステージ番号
		int StageNumber;

		//ステージ切り替えフラグ
		bool Change;

		//初期化
		virtual void OnCreate()override;
		//更新
		virtual void OnUpdate() override;

		bool ToPlayMotion();
		void ToMenuMotion();
		void ToChangeMotion();
		void GetNum();
		void AddMusic();
		void CrateStage();
		float ActionTime;
		Vector3 m_Scale;
		void Action();
		void RefreshAction();

		void CreateTitel();

	};

	//--------------------------------------------------------------------------------------
	//	class StartGameState : public ObjState<MenuStage>;
	//	用途: スタートステート
	//--------------------------------------------------------------------------------------
	class StartGameState : public ObjState<MenuStage>
	{
		StartGameState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<StartGameState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<MenuStage>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<MenuStage>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<MenuStage>& Obj)override;
	};
	//--------------------------------------------------------------------------------------
	//	class StartGameState : public ObjState<MenuStage>;
	//	用途: スタートステート
	//--------------------------------------------------------------------------------------
	class ToGameState : public ObjState<MenuStage>
	{
		ToGameState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<ToGameState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<MenuStage>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<MenuStage>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<MenuStage>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class TutorialStage : public Stage;
	//	用途: TutorialStageクラス
	//--------------------------------------------------------------------------------------
	class TutorialStage : public Stage{
		shared_ptr< StateMachine<TutorialStage> >  m_StateMachine;	//ステートマシーン
		//リソースの作成
		void CreateResourses();
		//ビューの作成
		void CreateViews();
		//Tutorialスプライトの作成
		void CreateToTutorial();
		//Tutorial内でNextスプライトの作成
		void CreateToNextSprite();
		//Tutorial内でPrevスプライトの作成
		void CreateToPrevSprite();

	public:
		//構築と破棄
		TutorialStage() :Stage(){}
		virtual ~TutorialStage(){
		}
		//アクセサ
		shared_ptr< StateMachine<TutorialStage> > GetStateMachine() const{
			return m_StateMachine;
		}

		//初期化
		virtual void OnCreate()override;
		//更新
		virtual void OnUpdate() override;
		bool ToPlayMotion();
		void ToTutorMotion();

		int count = 0;

	};

	//--------------------------------------------------------------------------------------
	//	class StartTutorialState : public ObjState<TutorialStage>;
	//	用途: スタートゲームステート
	//--------------------------------------------------------------------------------------
	class StartTutorialState : public ObjState<TutorialStage>
	{
		StartTutorialState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<StartTutorialState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<TutorialStage>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<TutorialStage>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<TutorialStage>& Obj)override;
	};
	//--------------------------------------------------------------------------------------
	//	class EndTutorialState : public ObjState<TutorialStage>;
	//	用途: ゲーム中？ステート
	//--------------------------------------------------------------------------------------
	class EndTutorialState : public ObjState<TutorialStage>
	{
		EndTutorialState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<EndTutorialState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<TutorialStage>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<TutorialStage>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<TutorialStage>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class GameClearStage : public Stage;
	//	用途: ゲームクリアステージクラス
	//--------------------------------------------------------------------------------------
	class GameClearStage : public Stage{
		shared_ptr< StateMachine<GameClearStage> >  m_StateMachine;	//ステートマシーン
		//リソースの作成
		void CreateResourses();
		//ビューの作成
		void CreateViews();
		//ゲームクリアプライトの作成
		void CreateToClear();


	public:
		//構築と破棄
		GameClearStage() :Stage(){}
		virtual ~GameClearStage(){
		}
		//アクセサ
		shared_ptr< StateMachine<GameClearStage> > GetStateMachine() const{
			return m_StateMachine;
		}

		//ステージ番号
		int Number;
		int ChangeNum;

		bool Change;

		//初期化
		virtual void OnCreate()override;
		//更新
		virtual void OnUpdate() override;
		bool ToTitelMotion();
		void ToChangeMotion();
		void ChangeMotion();
		void AddMusic();

		float Timer;//エラプスドタイムと一緒に使うタイマー変数
		float ElapsedTime = App::GetApp()->GetElapsedTime();////時間加算用変数エラプスドタイム

		void SpriteAction();
		float ActionTime;
		Vector3 m_Scale;
		void RefreshAction();
		void GAMERESULT();

		float GAMESCORE;//毎ターン、スコア(タイム)が換算される
		void GameScore();//スコアを取得、リザルトへ変換
		void RESULT();//リザルトのスプライトを更新
		//「メニューへ」の作成
		void CreateGoToMenu();
		//「タイトルへ」の作成
		void CreateGoToTitle();

		//ゲームクリアタイムの表示
		void CreateTime();//｢タイム｣スプライトの表示
		void ClearRank();//｢タイム｣スプライトの表示
		void SetClearTime();//クリアタイムを格納する
		float CLEARTIME;//クリアタイムが格納される
		void SetClearRANK();//クリアタイムからランクを割り当てる
		int CLEARRANK;
		void CreateClearTime();//クリアタイムを表示
		void ClearTime();//クリアタイムを更新する
		void ClearRANK();//クリアタイムからランクを割り当て、表示する
		void RandomCounterAnimation();//カウンターをわちゃわちゃさせる処理
		float RandomNumber;//カウンターがわちゃわちゃする為の変数(乱数)格納用

	};

	//--------------------------------------------------------------------------------------
	//	class GameClear_RESULT_State : public ObjState<GameClearStage>;
	//	用途: ゲームクリア_リザルト_ステート
	//--------------------------------------------------------------------------------------
	class GameClear_RESULT_State : public ObjState<GameClearStage>
	{
		GameClear_RESULT_State(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<GameClear_RESULT_State> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<GameClearStage>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<GameClearStage>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<GameClearStage>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class GameClear_SCENECHOICE_State : public ObjState<GameClearStage>;
	//	用途:ゲームクリア_SCENE選択_ステート
	//--------------------------------------------------------------------------------------
	class GameClear_SCENECHOICE_State : public ObjState<GameClearStage>
	{
		GameClear_SCENECHOICE_State(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<GameClear_SCENECHOICE_State> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<GameClearStage>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<GameClearStage>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<GameClearStage>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class ToTitelState : public ObjState<GameClearStage>;
	//	用途:	タイトル画面に移行するステート
	//--------------------------------------------------------------------------------------
	class ToTitelState : public ObjState<GameClearStage>
	{
		ToTitelState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<ToTitelState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<GameClearStage>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<GameClearStage>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<GameClearStage>& Obj)override;
	};


	//--------------------------------------------------------------------------------------
	//	class GameOverStage : public Stage;
	//	用途: ゲームオーバーステージクラス
	//--------------------------------------------------------------------------------------
	class GameOverStage : public Stage{
		shared_ptr< StateMachine<GameOverStage> >  m_StateMachine;	//ステートマシーン
		//リソースの作成
		void CreateResourses();
		//ビューの作成
		void CreateViews();
		//ゲームクリアプライトの作成
		void CreateToOver();
		//｢メニューへ｣スプライトの作成
		void CreateGoToMenu();
		//｢タイトルへ｣スプライトの作成
		void CreateGoToTitle();

	public:
		//構築と破棄
		GameOverStage() :Stage(){}
		virtual ~GameOverStage(){
		}
		//アクセサ
		shared_ptr< StateMachine<GameOverStage> > GetStateMachine() const{
			return m_StateMachine;
		}

		//ステージ番号
		int Number;
		int ChangeNum;

		bool Change;

		//アクションの実装
		void SpriteAction();
		float ActionTime;
		Vector3 m_Scale;
		void RefreshAction();

		//初期化
		virtual void OnCreate()override;
		//更新
		virtual void OnUpdate() override;
		bool ToTitelMotion();
		void ToChangeMotion();
		void ChangeMotion();
		void AddMusic();

	};

	//--------------------------------------------------------------------------------------
	//	class GameOverState : public ObjState<GameOverStage>;
	//	用途: ゲームオーバーステート
	//--------------------------------------------------------------------------------------
	class GameOverState : public ObjState<GameOverStage>
	{
		GameOverState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<GameOverState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<GameOverStage>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<GameOverStage>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<GameOverStage>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class ChangeTitelState : public ObjState<GameOverStage>;
	//	用途:	タイトル画面に移行するステート
	//--------------------------------------------------------------------------------------
	class ChangeTitelState : public ObjState<GameOverStage>
	{
		ChangeTitelState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<ChangeTitelState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<GameOverStage>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<GameOverStage>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<GameOverStage>& Obj)override;
	};



	class Item_RandomBox;

	//--------------------------------------------------------------------------------------
	//	class GameStage : public Stage;
	//	用途: ゲームステージクラス
	//--------------------------------------------------------------------------------------
	class GameStage : public Stage{
		//ピースの大きさ
		const Vector3 m_Piece;

		shared_ptr< StateMachine<GameStage> >  m_StateMachine;	//ステートマシーン

		float m_CountTime;//ゲーム開始からクリアまでのタイムを取得

		//リソースの作成
		void CreateResourses();
		//ビューの作成
		void CreateViews();
		//プレイヤーの作成
		void CreatePlayer();
		//アイテムの作成更新
		void UpdateItem();
		
		//[Enemy]の数
		int EnemyNum;
		
		//アイテムマネージャーのシェアードピーティーアールの作成
		void CreateItemManager();

		//エネミーマネージャーのシェアードピーティーアールの作成
		void CreateEnemyManager();

		//Enemyの生成と更新
		void UpdateEnemy();
		//タイマースプライトの作成
		void CreateTim();
		//敵残機UIの作成
		void CreateEnemyUI();
		
		//スパークの作成
		void CreateSpark();
		void CreateParticle();
		void CreateEfect();
		void CreateCross();

		void CreatePowerUpEffect();//PowerUpEffectの作成
		void CreateHeelEffect();//HeelEffectの作成
		void CreateJrSpark();//敵撃破時に撒き散らすEffectの作成
		void CreateHeartSpark();//回復アイテムが纏うEffectの作成
		void CreateCircleSpark();//強化アイテムが纏うEffectの作成

		void CreatePlayer_Dameage();//Player被弾時のEffectの作成
		void CreateBOSS_Enter();
		void CreateBOSS_Dameage();//BOSS被弾時のEffectの作成
		void CreateSTANSpark();//BOSS麻痺時のEffectの作成
		//アラートスプライトの作成
		void CreateAlret();

		//Retubeスプライトの作成
		void CreateReTuto();

		//でこぼこ床
		void CreateUnevenGround();

		//でこぼこ床(敵用)
		void CreateUnevenGround_Enemy();


		void SetWall();
		
	public:
		//構築と破棄
		GameStage() :Stage(), m_CountTime(0){}
		virtual ~GameStage(){}
		//初期化
		virtual void OnCreate()override;
		virtual void OnUpdate()override;
		virtual void OnLastUpdate()override;

		bool m_Changflag = false;
		float CameChang;

		UINT CheckItem();

		//アクセサ
		const Vector3& GetPiece() const{
			return m_Piece;
		}
		float GameTimer;//ゲーム開始と同時に時間を取るタイマー
		void ReFreshGameTimer();//GamePlayステートへ入る時、GameTimerを初期化
		void CountUpGameTimer();//呼び出されるとタイマーへ時間を加算する関数
		void TimeOver();

		float ENEMYCOUNTER;//各ステージで倒す敵の数を数える?変数(intが適切だが元がfloatなのでfloat型を使用) 
		void SetEnemyCounter();//初期ステートでステージ番号取ってENEMYCOUNTERに数を割り当てる
		void CounterDownEnemy();//エネミーカウンターの更新
		
		//tuika
		//
		void CreateHPBar();
		int Player_HP;
		void ControlHPBar();

		float HPActionTimer;
		void MAXtoMID();//HPがMAXからMIDに移行時に再生する
		void MIDtoLAST();//HPがMIDからLASTに移行時に再生する
		void HP_Animation_flag();//Animationの再生時間を管理する
		float FLAG_TIMER_A;
		float FLAG_TIMER_B;
		//
		//tuikaここまで

		//行動管理関数群へのシェアードptrを作成
		void CreateMotionManager();

		void AddScore();

		void SetGame(bool game);


		//アクセサ
		shared_ptr< StateMachine<GameStage> > GetStateMachine() const{
			return m_StateMachine;
		}


		//ナンバースプライトの作成
		void CreateTimer();
		//エネミーカウンターの追加
		void CreateEnemyCounter();

		//-------------------------------------------------------
		//Csvの行を取得
		//-------------------------------------------------------
		const size_t GetRow() const{
			return m_sRowSize;
		}

		//Csvの列を取得
		const size_t GetCol() const{
			return m_sColSize;
		}

		//引数から選択したセルのデータを取得する
		const int GetMapData(size_t row, size_t col) const{
			return m_MapDataVec[col][row];
		}

		//マップデータを設定する 2015/12/05作成
		void SetMapData(size_t row, size_t col, size_t enumMapDate){
			m_MapDataVec[col][row] = enumMapDate;
		}

		//マップデータサイズを取得する
		const Vector2 GetMapSize() const{
			return Vector2((float)m_sRowSize, (float)m_sColSize);
		}

		//プレイヤーを生成するCell座標を取得する
		const Vector2 GetPlayerSpawnCell() const{
			return m_vPlayerSpawnCell;
		}

		//マップデータから通れるか
		const bool GetNextCheckMapData(const size_t row, const size_t col) const {
			if (GetMapData(row, col) >= DataID::ROAD){
				return true;
			}
			return false;
		}

		//マップデータの最大最低からいかないようにする
		const bool GetCheckToDataMap(const int i_PlayerPosX, const int i_PlayerPosZ) const{
			bool OverCol = GetCol() > (size_t)i_PlayerPosX;
			bool OverRow = GetRow() > (size_t)i_PlayerPosZ;
			bool LowRow = i_PlayerPosX >= 0;
			bool LowCol = i_PlayerPosZ >= 0;

			if (OverRow && OverCol && LowRow && LowCol){
				return true;
			}

			return false;
		}

	private:
		//-------------------------------------------------------
		// ---------- member ----------.
		//-------------------------------------------------------
		//プレイヤーを生成するCell座標
		Vector2 m_vPlayerSpawnCell;

		//ステージ生成に必要なメンバ変数
		size_t m_sColSize;
		size_t m_sRowSize;
		//ワーク用のベクター配列のメンバ変数
		vector< vector <size_t> > m_MapDataVec;


		//CSVからボックス作成　
		void CreateRoad();



	};

	//タイマー判定用にゲームプレイ中ステート作成
	//
	//--------------------------------------------------------------------------------------
	//	class GamePlayステート : public ObjState<GameClearStage>;
	//	用途: ゲームプレイ中ステート
	//--------------------------------------------------------------------------------------
	class GamePlayState : public ObjState<GameStage>
	{
		GamePlayState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<GamePlayState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<GameStage>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<GameStage>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<GameStage>& Obj)override;
	};
	//
}
//endof  basedx11
