#pragma once

#include "stdafx.h"


namespace basedx11{

	//--------------------------------------------------------------------------------------
	//	class NumberSprite : public GameObject;
	//	用途: 配置オブジェクト
	//--------------------------------------------------------------------------------------
	class NumberSprite : public GameObject{
		Vector3 m_StartPos;
		Vector3 m_StartScale;
		//頂点の配列の配列（2次元配列）
		vector< vector<VertexPositionColorTexture> > m_NumberVertexVec;
		float m_TotalTime;

	public:
		//構築と破棄
		NumberSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos, const Vector3& StartScale);
		virtual ~NumberSprite();
		//初期化
		virtual void OnCreate() override;
		//変化
		virtual void OnUpdate() override;

		//アクセサ
		void SetNum(float num)
		{
			m_TotalTime = num;
		}
	};
}//end basedx11