#pragma once

#include "stdafx.h"

namespace basedx11{

	//--------------------------------------------------------------------------------------
	//	struct WaveData;
	//	用途: 波の情報（座標と半径）。「class UnevevGround」の中だけで使用します。
	//--------------------------------------------------------------------------------------
	struct WaveData{
		Vector3 Pos;
		float r;
		float Hight;
		WaveData(const Vector3& pos, float r, float hight) : Pos(pos), r(r), Hight(hight){}
		~WaveData(){}
	};


	//--------------------------------------------------------------------------------------
	//	class UnevenGround : public GameObject;
	//	用途: でこぼこのグランド
	//--------------------------------------------------------------------------------------
	class UnevenGround : public GameObject{
		//メッシュリソース
		shared_ptr<MeshResource> m_MeshResource;
		//メッシュリソースの作成
		void CreateMeshResource();
		//メッシュの更新
		void UpdateMeshResource();

		//でこぼこ床の範囲
		UINT m_Width;
		UINT m_Height;
		//分割数
		UINT m_Division;


		//受け取った座標と、受け取った波の情報で、座標にどれだけの力が加わったかを計算する
		float CalcWavePower(const Vector3& Pos, const vector<WaveData>& WaveVec);

	public:
		//構築と破棄
		UnevenGround(const shared_ptr<Stage>& StagePtr,
			UINT Width, UINT Height, UINT Division);
		virtual ~UnevenGround(){}
		
		//干渉点表示フラグ
		bool CrossUpdate;

		//干渉点ポジション
		vector<Vector3> m_CrossVec3Vec;

		//初期化
		virtual void OnCreate() override;
		virtual void OnUpdate() override;
		virtual void OnLastUpdate() override;


		//このでこぼこ床の範囲内ならtrue
		//戻り値がtrueならPos;itionとY軸で交差する三角形を返す
		bool GetTriangle(const Vector3& Position, vector<Vector3>& RetVec);
		//戻り値がtrueなら波のデータを2つと交点座標を返す
		bool CollitionWaveWave(const WaveData& d1, const WaveData& d2, vector<Vector3>& RetVec);

		//現在ゲーム上で動いている波を全て調べ、その波の情報を返す（座標と半径）
		void CheckAllActiveWave(vector<WaveData>& RetVec);

	};

}
//endof  basedx11
