#include "stdafx.h"
#include "Project.h"

class Item;

namespace basedx11{
	//--------------------------------------------------------------------------------------
	//	class Player : public GameObject;
	//	用途: プレイヤー
	//--------------------------------------------------------------------------------------
	//構築と破棄
	Player::Player(const shared_ptr<Stage>& StagePtr,
		Vector3 Position
		) :
		GameObject(StagePtr),
		m_Position(Position),
		m_MaxSpeed(40.0f),	//最高速度
		m_Decel(0.95f),	//減速値
		m_Mass(1.0f)	//質量
	{}

	//初期化
	void Player::OnCreate(){
		//初期位置などの設定
		auto Ptr = AddComponent<Transform>();
		Ptr->SetScale(0.25f, 0.25f, 0.25f);	//直径25センチの球体
		Ptr->SetRotation(0.0f, 0.0f, 0.0f);
		Ptr->SetPosition(m_Position);

		//Playerの体力
		PlayerHP = 3;
		//波の最大発生数
		MaxWave = 3;
		//待機時間
		Waiting = 0.0f;

		//Rigidbodyをつける
		auto PtrRedit = AddComponent<Rigidbody>();
		//反発力
		//PtrRedit->SetReflection(0.0f);
		//重力をつける
		auto PtrGravity = AddComponent<Gravity>();
		//最下地点
		PtrGravity->SetBaseY(0.24f);
		//衝突判定をつける
		auto PtrCol = AddComponent<CollisionSphere>();

		//文字列をつける
		auto PtrString = AddComponent<StringSprite>();
		PtrString->SetText(L"");
		PtrString->SetTextRect(Rect2D<float>(16.0f, 16.0f, 640.0f, 480.0f));

		//影をつける（シャドウマップを描画する）
		auto ShadowPtr = AddComponent<Shadowmap>();
		//影の形（メッシュ）を設定
		ShadowPtr->SetMeshResource(L"DEFAULT_SPHERE");
		//描画コンポーネントの設定
		auto PtrDraw = AddComponent<PNTStaticDraw>();
		//描画するメッシュを設定
		PtrDraw->SetMeshResource(L"DEFAULT_SPHERE");
		//描画するテクスチャを設定
		PtrDraw->SetTextureResource(L"PLAYER_TX");

		setDEFAULT_COLOR();

		//透明処理
		SetAlphaActive(true);
		//0番目のビューのカメラを得る
		//LookAtCameraである
		auto PtrCamera = dynamic_pointer_cast<MyCamera>(GetStage()->GetCamera(0));
		if (PtrCamera){
			//LookAtCameraに注目するオブジェクト（プレイヤー）の設定
			PtrCamera->SetTargetObject(GetThis<GameObject>());
		}
		//ステートマシンの構築
		m_StateMachine = make_shared< StateMachine<Player> >(GetThis<Player>());
		//最初のステートをDefaultStateに設定
		m_StateMachine->ChangeState(DefaultState::Instance());
		SteatName = L"DefaultState";
		SetDrawLayer(-2);


		//BGMの登録
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"Game");
		pMultiSoundEffect->AddAudioResource(L"Boss");
		pMultiSoundEffect->AddAudioResource(L"Soner");
		pMultiSoundEffect->AddAudioResource(L"Dameg");
		pMultiSoundEffect->AddAudioResource(L"Recovery"); 
		pMultiSoundEffect->AddAudioResource(L"POWERUP");

		//再生（繰り返し、0.5ボリューム）
		pMultiSoundEffect->Start(L"Game", XAUDIO2_LOOP_INFINITE, 0.5f);
	}

	//移動の向きを得る
	Vector3 Player::GetAngle(){
		Vector3 Angle(0, 0, 0);
		//コントローラの取得
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].bConnected){
			if (CntlVec[0].fThumbLX != 0 && CntlVec[0].fThumbLY != 0){
				float MoveLength = 0;	//動いた時のスピード
				auto PtrTransform = GetComponent<Transform>();
				auto PtrCamera = GetStage()->GetCamera(0);
				//進行方向の向きを計算
				Vector3 Front = PtrTransform->GetPosition() - PtrCamera->GetEye();
				Front.y = 0;
				Front.Normalize();
				//進行方向向きからの角度を算出
				float FrontAngle = atan2(Front.z, Front.x);
				//コントローラの向き計算
				float MoveX = CntlVec[0].fThumbLX;
				float MoveZ = CntlVec[0].fThumbLY;
				//コントローラの向きから角度を計算
				float CntlAngle = atan2(-MoveX, MoveZ);
				//トータルの角度を算出
				float TotalAngle = FrontAngle + CntlAngle;
				//角度からベクトルを作成
				Angle = Vector3(cos(TotalAngle), 0, sin(TotalAngle));
				//正規化する
				Angle.Normalize();
				//Y軸は変化させない
				Angle.y = 0;
			}
		}
		return Angle;
	}


	//更新
	void Player::OnUpdate(){
		//ステートマシンのUpdateを行う
		//この中でステートの更新が行われる(Execute()関数が呼ばれる)
		m_StateMachine->Update();


	}

	//ターンの最終更新時
	void Player::OnLastUpdate(){
		auto Enemyboss = GetStage()->GetSharedGameObject<EnemyBoss>(L"EnemyBoss",false);
		if (Enemyboss){
		
			if (Enemyboss->BossMusic){
				auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
				pMultiSoundEffect->Stop(L"Game");

			}
			//ボスの生存フラグに連動
			if (Enemyboss->m_EnemyBossFlg)
			{
				//BGMを止める
				auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
				pMultiSoundEffect->Stop(L"Game");
				float ElapsedTime = App::GetApp()->GetElapsedTime();
				Waiting += ElapsedTime;
				if (Waiting > 2.5f){
					//ゲームクリアシーンに移行
					PostEvent(0.0f, GetThis<Player>(), App::GetApp()->GetSceneBase(), L"ToClear");
				}
			}
		}


	}

	void Player::OnCollision(const shared_ptr<GameObject>& other)
	{
		//ヒットした相手が振り分け用のインターフェイスを保持してるか確認
		auto IAssign = dynamic_pointer_cast<ItemInterface>(other);
		auto IsEnemy = dynamic_pointer_cast<EnemyInterface>(other);
		if (IAssign){
			//インターフェイスを保持してた
			//処理を振り分ける
			IAssign->ItemAction(GetThis<Player>());
		}//IsRoomだったら
		if (IsEnemy){
			IsEnemy->EnemyAction(GetThis<Player>());

		}
	}

	int Player::GetWaveCount(){

		int count = 0;
		auto Group = GetStage()->GetSharedObjectGroup(L"Wave");
		auto WaveVec = Group->GetGroupVector();
		for (auto Ptr : WaveVec){
			//Ptrはweak_ptrなので有効性チェックが必要
			if (!Ptr.expired()){
				auto ShellPtr = dynamic_pointer_cast<Wave>(Ptr.lock());
				if (ShellPtr){
					if ((ShellPtr->IsUpdateActive()) && (ShellPtr->IsDrawActive())){
						count++;
					}
				}
			}
		}
		return count;
	}

	//モーションを実装する関数群
	//移動して向きを移動方向にする
	void Player::MoveRotationMotion(){
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		Vector3 Angle = GetAngle();
		//Transform
		auto PtrTransform = GetComponent<Transform>();
		//Rigidbodyを取り出す
		auto PtrRedit = GetComponent<Rigidbody>();
		//現在の速度を取り出す
		auto Velo = PtrRedit->GetVelocity();
		//目的地を最高速度を掛けて求める
		auto Target = Angle * m_MaxSpeed;
		//目的地に向かうために力のかける方向を計算する
		//Forceはフォースである
		auto Force = Target - Velo;
		//yは0にする
		Force.y = 0;
		//加速度を求める
		auto Accel = Force / m_Mass;
		//ターン時間を掛けたものを速度に加算する
		Velo += (Accel * ElapsedTime);
		//減速する
		Velo *= m_Decel;
		//速度を設定する
		PtrRedit->SetVelocity(Velo);
		//回転の計算
		float YRot = PtrTransform->GetRotation().y;
		Quaternion Qt;
		Qt.Identity();
		if (Angle.Length() > 0.0f){
			//ベクトルをY軸回転に変換
			float PlayerAngle = atan2(Angle.x, Angle.z);
			Qt.RotationRollPitchYaw(0, PlayerAngle, 0);
			Qt.Normalize();
		}
		else{
			Qt.RotationRollPitchYaw(0, YRot, 0);
			Qt.Normalize();
		}
		//Transform
		PtrTransform->SetQuaternion(Qt);
	}

	//Aボタンで波を発生させるかどうかを得る
	bool Player::IsWaveMotion(){
		//コントローラの取得
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].bConnected){
			//Aボタンが押された
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A){
				return true;
			}
		}
		return false;
	}

	//波の生存時間のカウント開始
	void Player::WaveTime(){
		if (!m_wave){
			return;
		}
		auto wave = dynamic_pointer_cast<Wave>(m_wave);
		wave->StartTimer();

	}

	//Aボタンで波を発生させる処理
	void Player::WaveMotion(){
		if (GetWaveCount() > MaxWave){
			return;
		}
		auto PtrTrans = GetComponent<Transform>();
		//グループ内に空きがあればそのオブジェクトを再利用
		//そうでなければ新規に作成
		auto Group = GetStage()->GetSharedObjectGroup(L"Wave");
		auto WaveVec = Group->GetGroupVector();
		//SEの再生
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->Start(L"Soner", 0, 0.5f);

		for (auto Ptr : WaveVec){
			//Ptrはweak_ptrなので有効性チェックが必要
			if (!Ptr.expired()){
				auto ShellPtr = dynamic_pointer_cast<Wave>(Ptr.lock());
				if (ShellPtr){
					//UpdateとDrawを行っていない物があるならそれを使う
					if ((!ShellPtr->IsUpdateActive()) && (!ShellPtr->IsDrawActive())){
						ShellPtr->Refresh(PtrTrans->GetPosition());
						m_wave = Ptr.lock();

						return;
					}
				}
			}
		}
		//ここまで来たら空きがなかったことになる
		//砲弾の追加
		auto Sh = GetStage()->AddGameObject<Wave>(PtrTrans->GetPosition());
		m_wave = Sh;
		//グループに追加
		Group->IntoGroup(Sh);
	
	}

	void Player::WaveRemove(){
		m_wave = nullptr;
	}

	//PowerUpの制限時間
	void Player::PowerUpTime()
	{
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		PowerLimit += ElapsedTime;
		if (PowerLimit >= 5.0f)
		{
			GetStateMachine()->ChangeState(DefaultState::Instance());
		}
	}

	//PowerUp状態の制限時間を初期化
	void Player::TimeReflesh()
	{
		PowerLimit = 0.0f;
	}

	//音波設置数を初期化
	void Player::WaveRef()
	{
		MaxWave = 3;
	}

	//回復アイテム取得時
	void Player::ActionWithItem(const shared_ptr<ItemTest>& Item)
	{
		Item->SetUpdateActive(false);
		Item->SetDrawActive(false);

		//ゲームステージのポインタ取得
		auto ItemManagerPtr = GetStage()->GetSharedGameObject<ItemManager>(L"ItemManager", false);
		auto& HitListPtr = ItemManagerPtr->GetList_ManageItemList();
		HitListPtr.push_back(Item);

		AddHP();

	}

	//PowerUpアイテム取得時
	void Player::ActionWithItem(const shared_ptr<Item_PowerUP>& Item)
	{
		Item->SetUpdateActive(false);
		Item->SetDrawActive(false);

		//ゲームステージのポインタ取得
		auto ItemManagerPtr = GetStage()->GetSharedGameObject<ItemManager>(L"ItemManager", false);
		auto& HitListPtr = ItemManagerPtr->GetList_ManageItemList();
		HitListPtr.push_back(Item);

		if (SteatName == L"DefaultState")
		{
			auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
			pMultiSoundEffect->Start(L"POWERUP", 0, 0.5f);
			MaxWave = 6;
			GetStateMachine()->ChangeState(PowerUpState::Instance());
		}
	};

	//Randombox取得時
	void Player::ActionWithItem(const shared_ptr<Item_RandomBox>& Item)
	{
		Item->SetUpdateActive(false);
		Item->SetDrawActive(false);

		//ゲームステージのポインタ取得
		auto ItemManagerPtr = GetStage()->GetSharedGameObject<ItemManager>(L"ItemManager", false);
		auto& HitListPtr = ItemManagerPtr->GetList_ManageItemList();
		HitListPtr.push_back(Item);

		float RANDAMITEM = Util::RandZeroToOne(true) * 3;
		if (RANDAMITEM >= 1.5&&PlayerHP<3)
		{
			PlayerHP += 1;
		}
		else if (RANDAMITEM < 1.5&&SteatName == L"DefaultState")
		{
			PlayerHP -= 1;
			MaxWave = 3;
			GetStateMachine()->ChangeState(PowerUpState::Instance());
		}
	};

	//Enemy接触時
	void Player::ActionWithEnemy(const shared_ptr<Enemy>& Enemy)
	{
		Dameg();

		//ゲームステージのポインタ取得&エネミーリストスポナーへ出荷
		auto EnemyManagerPtr = GetStage()->GetSharedGameObject<EnemyManager>(L"EnemyManager", false);
		auto& HitListPtr = EnemyManagerPtr->m_ManageEnemyList;
		HitListPtr.push_back(Enemy);

		Enemy->SetUpdateActive(false);
		Enemy->SetDrawActive(false);
	}

	//EnemyBoss接触時
	void Player::ActionWithEnemyBoss(const shared_ptr<EnemyBoss>& EnemyBoss)
	{
		Dameg();
	}

	//HP回復アイテムを取得した時によばれる
	void Player::AddHP(){

		HeelEffect_Player();
		
		if (PlayerHP < 3){
			//SEの再生
			auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
			pMultiSoundEffect->Start(L"Recovery", 0, 0.5f);
			PlayerHP += 1;
			auto ptrAlret = GetStage()->GetSharedGameObject<AlretSprite>(L"Alret");
			ptrAlret->SetHP(PlayerHP);

		};

	}
	//敵に衝突した時に呼ばれる
	void Player::Dameg(){
		if (!(SteatName == L"MUTEKI_State"))
		{
			//ここをプレイヤーがダメージ受けた時用のに変更
			auto PtrSpark = GetStage()->GetSharedGameObject<Player_DamageEffect>(L"Player_DamageEffect", false);
			PtrSpark->InsertSpark(GetComponent<Transform>()->GetPosition(),10);

			//SEの再生
			auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
			pMultiSoundEffect->Start(L"Dameg", 0, 0.5f);

			PlayerHP -= 1;
			auto ptrAlret = GetStage()->GetSharedGameObject<AlretSprite>(L"Alret");
			ptrAlret->SetHP(PlayerHP);

			//無敵ステートへ移行
			GetStateMachine()->ChangeState(MUTEKI_State::Instance());

			//Player死亡処理
			if (PlayerHP <= 0){
				auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
				pMultiSoundEffect->Stop(L"Game");
				auto Enemyboss = GetStage()->GetSharedGameObject<EnemyBoss>(L"EnemyBoss", false);
				Enemyboss->StopSound();
				//イベント送出
				PostEvent(0.0f, GetThis<Player>(), App::GetApp()->GetSceneBase(), L"ToOver");
			}
		}
	}
	
	//一定時間が立ったらステートをデフォルトへ戻す
	void Player::MUTEKI_TIME()
	{
		MUTEKI_Timer += ElapsedTime;
		if (MUTEKI_Timer > 3)
		{
			GetStateMachine()->ChangeState(DefaultState::Instance());
			return;
		}
		Flash_Player();
	}

	void Player::MUTEKI_Reflesh()
	{
		MUTEKI_Timer = 0;
	}

	void Player::Flash_Player()
	{
		FLASH_TIME++;
		if ((int)FLASH_TIME<10)
		{
			SetDrawActive(false);
		}
		else
		{
			SetDrawActive(true);
		}
		if ((int)FLASH_TIME > 15)
		{
			FLASH_TIME = 0;
		}
	}

	void Player::Flash_Reflesh()
	{
		auto PtrDraw = GetComponent<PNTStaticDraw>();
		PtrDraw->SetDiffuse(DEFAULT_DIFFUSE);
		PtrDraw->SetEmissive(DEFAULT_EMISSIVE);
		SetDrawActive(true);
	}

	void Player::setDEFAULT_COLOR()
	{
		auto PtrDraw = GetComponent<PNTStaticDraw>();
		DEFAULT_DIFFUSE = PtrDraw->GetDiffuse();
		DEFAULT_EMISSIVE = PtrDraw->GetEmissive();
	}

	//PowerUp状態の間表示されるエフェクト
	void Player::PowerUpEffect_Player()
	{
		auto PtrSpark = GetStage()->GetSharedGameObject<PowerUpEffect>(L"PowerUpEffect", false);
		PtrSpark->InsertSpark(GetComponent<Transform>()->GetPosition(), 15);
	}

	//Playerが回復したときに表示されるエフェクト
	void Player::HeelEffect_Player()
	{
		auto PtrSpark = GetStage()->GetSharedGameObject<HeelEffect>(L"HeelEffect", false);
		PtrSpark->InsertSpark(GetComponent<Transform>()->GetPosition(), 10);
	}

	//--------------------------------------------------------------------------------------
	//	class DefaultState : public ObjState<Player>;
	//	用途: 通常移動
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<DefaultState> DefaultState::Instance(){
		static shared_ptr<DefaultState> instance;
		if (!instance){
			instance = shared_ptr<DefaultState>(new DefaultState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void DefaultState::Enter(const shared_ptr<Player>& Obj){
		//何もしない
		//回数変更
		Obj->SteatName = L"DefaultState";
		Obj->WaveRef();
		Obj->Flash_Reflesh();
	}
	//ステート実行中に毎ターン呼ばれる関数
	void DefaultState::Execute(const shared_ptr<Player>& Obj){

		Obj->MoveRotationMotion();

		if (Obj->IsWaveMotion()){
			//波を発生させる
			Obj->WaveMotion();
		}
		else
		{
			//波の生存時間のカウント開始
			Obj->WaveTime();
			Obj->WaveRemove();
		}
	}
	//ステートにから抜けるときに呼ばれる関数
	void DefaultState::Exit(const shared_ptr<Player>& Obj){
		//何もしない
	}

	//--------------------------------------------------------------------------------------
	//	class PowerUpState : public ObjState<Player>;
	//	用途: 攻撃回数増加
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<PowerUpState> PowerUpState::Instance(){
		static shared_ptr<PowerUpState> instance;
		if (!instance){
			instance = shared_ptr<PowerUpState>(new PowerUpState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void PowerUpState::Enter(const shared_ptr<Player>& Obj){
		//何もしない
		Obj->SteatName = L"PowerUpState";
		Obj->TimeReflesh();
		Obj->PowerUpEffect_Player();

	}
	//ステート実行中に毎ターン呼ばれる関数
	void PowerUpState::Execute(const shared_ptr<Player>& Obj){

		Obj->MoveRotationMotion();

		Obj->PowerUpTime();

		if (Obj->IsWaveMotion()){
			//波を発生させる
			Obj->WaveMotion();
		}
		else
		{
			//波の生存時間のカウント開始
			Obj->WaveTime();
			Obj->WaveRemove();
		}

	}
	//ステートにから抜けるときに呼ばれる関数
	void PowerUpState::Exit(const shared_ptr<Player>& Obj){
		//何もしない
	}

	//--------------------------------------------------------------------------------------
	//	class MUTEKI_State : public ObjState<Player>;
	//	用途: 無敵状態
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<MUTEKI_State> MUTEKI_State::Instance(){
		static shared_ptr<MUTEKI_State> instance;
		if (!instance){
			instance = shared_ptr<MUTEKI_State>(new MUTEKI_State);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void MUTEKI_State::Enter(const shared_ptr<Player>& Obj){
		//何もしない
		//回数変更
		Obj->SteatName = L"MUTEKI_State";
		Obj->MUTEKI_Reflesh();
		Obj->WaveRef();
	}
	//ステート実行中に毎ターン呼ばれる関数
	void MUTEKI_State::Execute(const shared_ptr<Player>& Obj){

		Obj->MoveRotationMotion();

		if (Obj->IsWaveMotion()){
			//波を発生させる
			Obj->WaveMotion();
		}
		else
		{
			//波の生存時間のカウント開始
			Obj->WaveTime();
			Obj->WaveRemove();
		}

		//無敵時間処理と点滅処理
		Obj->MUTEKI_TIME();
	}
	//ステートにから抜けるときに呼ばれる関数
	void MUTEKI_State::Exit(const shared_ptr<Player>& Obj){
		//何もしない
		Obj->Flash_Reflesh();
	}
}
//endof  basedx11