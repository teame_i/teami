#pragma once

#include "stdafx.h"

namespace basedx11{

	//--------------------------------------------------------------------------------------
	//	class Wave : public GameObject;
	//	用途: 波
	//--------------------------------------------------------------------------------------

	class Wave : public GameObject{
		Vector3 m_Position;
		Vector3 m_NowScale;
		Vector3 m_Rotation;
		float m_Radius;
		bool m_IsTimer;
		bool m_Death;
	public:
		//構築と破棄
		Wave(const shared_ptr<Stage>& StagePtr,
			const Vector3& Position
			);
		virtual ~Wave();
		//波の生存時間をカウントする
		float Timer;
		//
		float WavePower;

		//再初期化
		void Refresh(const Vector3& Position);
		//初期化
		virtual void OnCreate() override;
		virtual void OnUpdate() override;
		//操作

		bool WaveUpde;

		void SetWavePower(float power){
			WavePower = power;
		}

		float GetWavePower(){
			return WavePower;
		}

		//波の生存時間を開始
		void StartTimer(){
			m_IsTimer = true;
		}

		//波の生存時間を開始
		void StartDeath(){
			m_Death = true;
		}

		//アクセサ：現在の半径を返す
		float GetRadius(){
			return m_Radius;
		}

	};

	//--------------------------------------------------------------------------------------
	//	class Wave_Enemy : public GameObject;
	//	用途: 敵の波
	//--------------------------------------------------------------------------------------

	class Wave_Enemy : public GameObject{
		Vector3 m_Position;
		Vector3 m_NowScale;
		Vector3 m_Rotation;
		float m_Radius;
		bool m_IsTimer;
	public:
		//構築と破棄
		Wave_Enemy(const shared_ptr<Stage>& StagePtr,
			const Vector3& Position
			);
		virtual ~Wave_Enemy();
		//波の生存時間をカウントする
		float Timer;
		//再初期化
		void Refresh(const Vector3& Position);
		//初期化
		virtual void OnCreate() override;
		virtual void OnUpdate() override;
		//操作

		//波の生存時間を開始
		void StartTimer(){
			m_IsTimer = true;
		}

		//アクセサ：現在の半径を返す
		float GetRadius(){
			return m_Radius;
		}

	};
}
//endof  basedx11
