#pragma once

#include"stdafx.h"

namespace basedx11{

	//プレイヤーの宣言
	class Player;

	//--------------------------------------------------------------------------------------
	//  class EnemyInterface
	// 用途　振り分けするインターフェイス
	//-----------------------------------------------------------------------
	class EnemyInterface{
	protected:
		EnemyInterface(){}
		virtual ~EnemyInterface(){}
	public:
		virtual void EnemyAction(const shared_ptr<Player>& ply) = 0;

	};

	//--------------------------------------------------------------------------------------
	//	class FixedEnemy : public GameObject;
	//	用途: Enemy[プレイヤーに突進するエネミー]
	//--------------------------------------------------------------------------------------
	class Enemy : public GameObject, public EnemyInterface
	{
		shared_ptr< StateMachine<Enemy> > m_StateMachine;

		Vector3 m_StartPos;
		Vector3 m_StartRot;
		Vector3 m_StartScal;

		//最高速度
		float m_MaxSpeed;
		//減速率
		float m_Decel;
		//質量
		float m_Mass;

		Vector3 E_velo;

		//EnemyのHP
		int EnemyHP;

		//無敵時間
		float Interval;

		//タイムテーブル用のベクター配列
		vector<int> TimeTable_Enemy;

	public:
		Enemy(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
			);

		shared_ptr< StateMachine<Enemy> > GetStateMachine() const{
			return m_StateMachine;
		}

		virtual ~Enemy();

		void OnCreate() override;

		void OnUpdate() override;

		//ステートネーム(現在のステートを記録する)
		wstring Enemy_SteatName;

		//移動の為のモーション(seekステアリング) 
		void MoveMotion();
		//当たり判定
		bool HitCheck();
		//干渉点に当たってるかどうか
		bool CrossHitCheck();
		//Waveを受けた時のモーション(改め、攻撃を受けた時の処理)
		void DamageMotion();
		//麻痺状態時に呼び出す処理
		void StanMotion();
		//麻痺時間
		float StanTime;
		//時間加算用変数エラプスドタイム
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		//麻痺時間のRefreshを行うよ
		void RefreshStan();

		//エネミー撃破時弾けるエフェクト
		void EnemyDameageEffect();

		//行動管理関数MotionManagerへ自身が持つベクター配列の要素を渡す
		void Call_Motion();
		float CALL_TIMER;
		int CALL_MOTION;

		//撤退用関数
		void Enemy_Escape();

		//衝突&操作
		virtual void EnemyAction(const shared_ptr<Player>& ply)override;

		Vector3 W_Pos;

		void SetW_Pos(Vector3 pos){
			W_Pos = pos;
		}
		Vector3 GetW_Pos(){
			return W_Pos;
		}
	};

	//-----------------------------------------------------------
	//class DefaultState : Public ObjState<Enemy>;
	//用途：Enemyの基本行動
	//-----------------------------------------------------------
	class Enemy_DefaultState : public ObjState < Enemy >
	{
		Enemy_DefaultState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<Enemy_DefaultState> Instance();
		//ステートに入った時に呼ばれる関数
		virtual void Enter(const shared_ptr<Enemy>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Enemy>& Obj)override;
		//ステートから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Enemy>& Obj)override;
	};

	//-----------------------------------------------------------
	//class StanState : public ObjState<Enemy>;
	//用途：Enemyの麻痺
	//-----------------------------------------------------------

	class Enemy_StanState : public ObjState < Enemy >
	{
		Enemy_StanState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<Enemy_StanState> Instance();
		//ステートに入った時に呼ばれる関数
		virtual void Enter(const shared_ptr<Enemy>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Enemy>& Obj)override;
		//ステートから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Enemy>& Obj)override;
	};

	/*
	エネミーカウンター(Manager)の追加
	*/
	//--------------------------------------------------------------------------------------
	//	class EnemyManager
	//	用途: 描画を切ったエネミーを再描画する
	//--------------------------------------------------------------------------------------
	class EnemyManager : public GameObject{

	public:
		EnemyManager(const shared_ptr<Stage>& StagePtr);
		//構築と破棄
		virtual ~EnemyManager(){}
		//タイムカウンター
		float m_RePopTime;
		//Itemの更新と描画が切れていないか監視
		void OnUpdate();

		//各アイテムリスト
		list<weak_ptr<GameObject>> m_ManageEnemyList;
		//当たったboxのリスト返すよ with ALLItem
		list<weak_ptr<GameObject>>& GetList_m_ManageEnemyList(){
			return m_ManageEnemyList;
		}
	};
	/*
	エネミーカウンター(Manager)の追加
	*/

	//--------------------------------------------------------------------------------------
	//	class EnemyBoss : public GameObject;
	//	用途: EnemyBoss[プレイヤーに突進するボスエネミー]
	//--------------------------------------------------------------------------------------
	class EnemyBoss : public GameObject, public EnemyInterface
	{
		shared_ptr< StateMachine<EnemyBoss> > mb_stateMAchine;

		Vector3 m_StartPos;
		Vector3 m_StartRot;
		Vector3 m_StartScal;

		//最高速度
		float m_MaxSpeed;
		//減速率
		float m_Decel;
		//質量
		float m_Mass;

		Vector3 E_velo;

		int EnemyBossHP;

		float BossInterval;

		float EnemyBossTime;

	public:
		void BossActive();
		EnemyBoss(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
			);

		shared_ptr< StateMachine<EnemyBoss> > GetStateMachine() const
		{
			return mb_stateMAchine;
		}

		virtual ~EnemyBoss();

		void OnCreate() override;

		void OnUpdate() override;

		//ボス死亡フラグ
		bool m_EnemyBossFlg;
		//ボスが描画してるかを見る
		bool m_EnemyDraw;

		//時間加算用変数エラプスドタイム
		float ElapsedTime = App::GetApp()->GetElapsedTime();

		//移動のためのモーション
		void Movemoton();

		//Waveを受けた時のモーション
		bool DamegeMoton();

		//ボスのHPを減らしたい
		void Damege();

		//ボスの無敵時間
		void BossTime();

		//エフェクトの作成
		void StartEfect();

		//BOSS被弾時Effectの生成・実行
		void BOSS_Damage_Effect();

		

		//ダメージカウント
		float DamegeCount;

		//時間がたつとtureを返す
		bool Defaultflg;

		void SetGravity();

		//ボスの音楽が流れてるか
		bool BossMusic;
		//ボス出現時に呼ばれる
		void ChangMusic();
		//ボスBGMを止める
		void StopSound();

		//ボス麻痺モーション
		void PalsyMotion();
		float PalsyTime;
		//干渉点の判定
		bool CrossCheck();
		//ボス待機
		void Wait();
		void RefStan();
		//干渉点に当たったかどうかを見る
		bool Crosshit;
		void hitrif();

		void MUTEKI_TIME();//無敵ステートから通常ステートへ移行する間に呼び続ける
		float muteki_time;
		void BOSS_Reflesh();//BOSSの無敵時間とColorを元に戻す

		void FLASH_BOSS();//BOSSが点滅します
		float FLASH_TIME_BOSS;//点滅時間

		void Spawn_Effect();
		//void Stan_Effect();
		void STAN_Effect();

		void setDEFAULT_BOSS_COLOR();//初期化時の色を取得・保管
		Color4 DEFAULT_DIFFUSE;
		Color4 DEFAULT_EMISSIVE;

		//衝突&操作
		virtual void EnemyAction(const shared_ptr<Player>& ply)override;

		Vector3 W_Pos;

		void SetW_Pos(Vector3 pos){
			W_Pos = pos;
		}

		Vector3 GetW_Pos(){
			return W_Pos;
		}

	};

	//-----------------------------------------------------------
	//class WaitingState : Public ObjState<EnemyBoss>;
	//用途：EnemyBossの待機状態。ザコが一定数倒されるとdefaultステートへ移行
	//-----------------------------------------------------------
	class WaitingState : public ObjState < EnemyBoss >
	{
		WaitingState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<WaitingState> Instance();
		//ステートに入った時に呼ばれる関数
		virtual void Enter(const shared_ptr<EnemyBoss>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<EnemyBoss>& Obj)override;
		//ステートから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<EnemyBoss>& Obj)override;
	};

	//-----------------------------------------------------------
	//class DefaultState : Public ObjState<EnemyBoss>;
	//用途：EnemyBossの基本行動。波に当たると麻痺ステートへ移行
	//-----------------------------------------------------------
	class EB_DefaultState : public ObjState < EnemyBoss >
	{
		EB_DefaultState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<EB_DefaultState> Instance();
		//ステートに入った時に呼ばれる関数
		virtual void Enter(const shared_ptr<EnemyBoss>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<EnemyBoss>& Obj)override;
		//ステートから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<EnemyBoss>& Obj)override;
	};

	//-----------------------------------------------------------
	//class DamegeState : Public ObjState<Enemy>;
	//用途：EnemyBossが麻痺した状態。干渉点に当たるとダメージを負い、MUTEKIステートへ移行
	//-----------------------------------------------------------
	class DamegeState_B : public ObjState < EnemyBoss >
	{
		DamegeState_B(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<DamegeState_B> Instance();
		//ステートに入った時に呼ばれる関数
		virtual void Enter(const shared_ptr<EnemyBoss>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<EnemyBoss>& Obj)override;
		//ステートから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<EnemyBoss>& Obj)override;
	};

	//-----------------------------------------------------------
	//class MUTEKI_BOSS_State : Public ObjState<Enemy>;
	//用途：EnemyBossがダメージを受けた後の無敵時間。一定時間後にdefaultステートへ移行
	//-----------------------------------------------------------
	class MUTEKI_BOSS_State : public ObjState < EnemyBoss >
	{
		MUTEKI_BOSS_State(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<MUTEKI_BOSS_State> Instance();
		//ステートに入った時に呼ばれる関数
		virtual void Enter(const shared_ptr<EnemyBoss>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<EnemyBoss>& Obj)override;
		//ステートから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<EnemyBoss>& Obj)override;
	};
}