#include "stdafx.h"
#include "Project.h"

class Player;
namespace basedx11{

	

	//--------------------------------------------------------------------------------------
	//	class ItemTest : public GameObject;
	//	用途: 出現後待機、取得するとライフを回復するアイテム
	//--------------------------------------------------------------------------------------
	//構築と破棄
	ItemTest::ItemTest(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
		) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)

	{
	}
	ItemTest::~ItemTest(){}

	//初期化
	void ItemTest::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		SetUpdateActive(true);
		SetDrawActive(true);

		//反発を実装する場合はRigidbodyをつける
		auto PtrRegid = AddComponent<Rigidbody>();
		//衝突判定
		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);
		SetDrawLayer(-2);
		SetAlphaActive(true);

		//重力をつける
		auto PtrGravity = AddComponent<Gravity>();
		//最下地点
		PtrGravity->SetBaseY(0.37f);

		//AddComponent<PNTCollisionDraw>();

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetTextureResource(L"Recovery_TX");
		PtrDraw->SetOwnShadowActive(true);

		//アクションの登録
		auto PtrAction = AddComponent<Action>();
		//1秒で90度回転
		PtrAction->AddRotateBy(1.0f, Vector3(0.0f, XM_PIDIV2, 0.0f));
		//ループする
		PtrAction->SetLooped(true);
		//アクション開始
		PtrAction->Run();

		//ItemGroupへ格納y
		auto Group = GetStage()->GetSharedObjectGroup(L"ItemGroup");
		Group->IntoGroup(GetThis<ItemTest>());

	}

	//再初期化
	void ItemTest::Refresh(const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position)
	{
		SetUpdateActive(true);
		SetDrawActive(true);
			m_Scale = Scale,
			m_Rotation = Rotation,
			m_Position = Position;

		//Transform取得
		auto Ptr = GetComponent<Transform>();
		Ptr->SetScale(m_Scale);
		Ptr->SetRotation(m_Rotation);
		Ptr->SetPosition(m_Position);
		//Ptr->SetPosition(m_StartPos);
		//描画コンポーネント
		auto PtrDraw = GetComponent<PNTStaticDraw>();
		PtrDraw->SetDiffuse(Color4(1.0f, 1.0f, 1.0f, 1.0f));
		//重力を取り出す
		auto PtrGravity = GetComponent<Gravity>();
	}

	//更新
	void ItemTest::OnLastUpdate()
	{
		float ElapsedTime_ReDraw = App::GetApp()->GetElapsedTime();
		bool CheckDrawActive = this->GetDrawActive();
		if (!CheckDrawActive)
		{
			ReDraw += ElapsedTime_ReDraw;
		}
		if (ReDraw > 5)
		{
			SetDrawActive(true);
			SetUpdateActive(true);
			ReDraw = 0;
		}
		Timer++;
		if (Timer > 60)
		{
			Effect_Heart();
			Timer = 0;
		}
	}

	//衝突&操作
	void ItemTest::ItemAction(const shared_ptr<Player>& ply){
		ply->ActionWithItem(GetThis<ItemTest>());
	}

	//Effect生成
	void ItemTest::Effect_Heart()
	{
		auto PtrTransform = GetComponent<Transform>();
		Vector3 pos = PtrTransform->GetPosition();
		auto PtrHeart = GetStage()->GetSharedGameObject<MultiSpark_Heart>(L"MultiSpark_Heart", false);
		PtrHeart->InsertSpark(pos, 10);
	}
	//--------------------------------------------------------------------------------------
	//	class Item_PowerUP : public GameObject;
	//	用途: 出現後待機、取得すると攻撃の手数が一つ増える
	//--------------------------------------------------------------------------------------
	//構築と破棄
	Item_PowerUP::Item_PowerUP(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
		) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)

	{
	}
	Item_PowerUP::~Item_PowerUP(){}

	//初期化
	void Item_PowerUP::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		SetUpdateActive(true);
		SetDrawActive(true);
		SetAlphaActive(true);

		//反発を実装する場合はRigidbodyをつける
		auto PtrRegid = AddComponent<Rigidbody>();
		//衝突判定
		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);
		SetDrawLayer(-2);


		//重力をつける
		auto PtrGravity = AddComponent<Gravity>();
		//最下地点
		PtrGravity->SetBaseY(0.37f);

		//AddComponent<PNTCollisionDraw>();

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetTextureResource(L"PowerUp_TX");
		PtrDraw->SetOwnShadowActive(true);

		//アクションの登録
		auto PtrAction = AddComponent<Action>();
		//1秒で90度回転
		PtrAction->AddRotateBy(1.0f, Vector3(0.0f, 180, 0.0f));
		//ループする
		PtrAction->SetLooped(true);
		//アクション開始
		PtrAction->Run();

		//Itemgroupへ格納
		auto Group = GetStage()->GetSharedObjectGroup(L"ItemGroup");
		Group->IntoGroup(GetThis<Item_PowerUP>());

	}

	//再初期化
	void Item_PowerUP::Refresh(const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position)
	{
		SetUpdateActive(true);
		SetDrawActive(true);
		m_Scale = Scale,
			m_Rotation = Rotation,
			m_Position = Position;

		//Transform取得
		auto Ptr = GetComponent<Transform>();
		Ptr->SetScale(m_Scale);
		Ptr->SetRotation(m_Rotation);
		Ptr->SetPosition(m_Position);
		//Ptr->SetPosition(m_StartPos);
		//描画コンポーネント
		auto PtrDraw = GetComponent<PNTStaticDraw>();
		PtrDraw->SetDiffuse(Color4(1.0f, 1.0f, 1.0f, 0.0f));
		//重力を取り出す
		auto PtrGravity = GetComponent<Gravity>();
	}



	//更新
	void Item_PowerUP::OnLastUpdate()
	{
		Effect_Circle();
	}

	//衝突&操作
	void Item_PowerUP::ItemAction(const shared_ptr<Player>& ply){
		ply->ActionWithItem(GetThis<Item_PowerUP>());
	}

	//Effect生成
	void Item_PowerUP::Effect_Circle()
	{
		auto PtrTransform = GetComponent<Transform>();
		Vector3 pos = PtrTransform->GetPosition();
		auto PtrHeart = GetStage()->GetSharedGameObject<MultiSpark_Circle>(L"MultiSpark_Circle", false);
		PtrHeart->InsertSpark(pos);
	}
	//--------------------------------------------------------------------------------------
	//	class Item_RandomBox : public GameObject;
	//	用途: 出現後待機、取得するとライフを回復・パワーアップのどちらかが行われる
	//--------------------------------------------------------------------------------------
	//構築と破棄
	Item_RandomBox::Item_RandomBox(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
		) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)

	{
	}
	Item_RandomBox::~Item_RandomBox(){}

	//初期化
	void Item_RandomBox::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		SetUpdateActive(true);
		SetDrawActive(true);

		//反発を実装する場合はRigidbodyをつける
		auto PtrRegid = AddComponent<Rigidbody>();
		//衝突判定
		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);
		
		//重力をつける
		auto PtrGravity = AddComponent<Gravity>();
		//最下地点
		PtrGravity->SetBaseY(0.25f);

		//AddComponent<PNTCollisionDraw>();

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetTextureResource(L"PowerUp_TX");
		PtrDraw->SetOwnShadowActive(true);
		SetAlphaActive(true);
		SetAlphaActive(true);
	}

	//再初期化
	void Item_RandomBox::Refresh(const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position)
	{
		SetUpdateActive(true);
		SetDrawActive(true);
			m_Scale = Scale,
			m_Rotation = Rotation,
			m_Position = Position;

		//Transform取得
		auto Ptr = GetComponent<Transform>();
		Ptr->SetScale(m_Scale);
		Ptr->SetRotation(m_Rotation);
		Ptr->SetPosition(m_Position);
		//Ptr->SetPosition(m_StartPos);
		//描画コンポーネント
		auto PtrDraw = GetComponent<PNTStaticDraw>();
		PtrDraw->SetDiffuse(Color4(1.0f, 1.0f, 1.0f, 1.0f));
		//重力を取り出す
		auto PtrGravity = GetComponent<Gravity>();
	}

	//衝突&操作
	void Item_RandomBox::ItemAction(const shared_ptr<Player>& ply){
		ply->ActionWithItem(GetThis<Item_RandomBox>());
	}

	//--------------------------------------------------------------------------------------
	//	class ItemManager;
	//	用途: 更新と描画が無効になっているアイテムの更新と描画を再有効かする
	//--------------------------------------------------------------------------------------
	ItemManager::ItemManager(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr)
	{}

	//アイテムマネージャー
	void ItemManager::OnUpdate()
	{
		float ElapsedTime_ItemWatcher = App::GetApp()->GetElapsedTime();//時間を取得

		if (m_ManageItemList.size() != 0)
		{

			//リストの中身があった場合
			m_RePopTime += ElapsedTime_ItemWatcher;//タイマーに時間を加算

			////一定時間になったらItemGroupの中のオブジェクトをチェックする
			if (m_RePopTime > 3.0f)
			{
				auto a = m_ManageItemList.front();
				//ロック解除
				auto ShBox = dynamic_pointer_cast<GameObject>(a.lock());
				if (!ShBox){ return; }
				//リストの先頭を取得
				if (!ShBox->GetUpdateActive() && !ShBox->GetDrawActive())
				{
					//更新と描画を有効化
					ShBox->SetUpdateActive(true);
					ShBox->SetDrawActive(true);
					//最初の要素を削除
					m_ManageItemList.pop_front();
					//タイマーを初期化
					m_RePopTime = 0;
				}

			}
		}
	}




}
//endof  basedx11