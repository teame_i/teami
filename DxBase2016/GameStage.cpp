
#include "stdafx.h"
#include "Project.h"


namespace basedx11{

	//--------------------------------------------------------------------------------------
	//	class StareStage : public Stage;
	//	用途: タイトルステージクラス
	//--------------------------------------------------------------------------------------
	//リソースの作成
	void StareStage::CreateResourses(){

		wstring strTexture = App::GetApp()->m_wstrRelativeDataPath + L"thx.png";//素材提供先Spriteの追加
		App::GetApp()->RegisterTexture(L"THX_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Blind_Black.png";//暗幕(黒)Spriteの追加
		App::GetApp()->RegisterTexture(L"Blind_TX", strTexture);


		//wstring NanikaWav = App::GetApp()->m_wstrRelativeDataPath + L"Morning.wav";
		//App::GetApp()->RegisterWav(L"MORNING", NanikaWav);
		//NanikaWav = App::GetApp()->m_wstrRelativeDataPath + L"Button.wav";
		//App::GetApp()->RegisterWav(L"BUTTON", NanikaWav);
	}
	//ビュー類の作成
	void StareStage::CreateViews(){
		//最初にデフォルトのレンダリングターゲット類を作成する
		//パラメータの2048.0fはシャドウマップのサイズ。大きいほど影が細かくなる（4096.0fなど）
		//影を細かくするとメモリを大量に消費するので注意！
		CreateDefaultRenderTargets(2048.0f);
		//影のビューサイズの設定。この値を小さくすると影が表示される範囲が小さくなる。
		//値が小さいほうが影は細かくなる
		//スタティック変数なので一度設定したらその値はステージを超えて保持される。
		Shadowmap::SetViewSize(32.0f);
		//マルチビューコンポーネントの取得
		auto PtrMultiView = GetComponent<MultiView>();
		//マルチビューにビューの追加
		auto PtrView = PtrMultiView->AddView();
		//ビューの矩形を設定（ゲームサイズ全体）
		Rect2D<float> rect(0, 0, (float)App::GetApp()->GetGameWidth(),
			(float)App::GetApp()->GetGameHeight());
		//ビューの背景色
		Color4 ViewBkColor(0.0f, 0.0f, 0.0f, 1.0f);
		//最初のビューにパラメータの設定
		PtrView->ResetParamaters<MyCamera, MultiLight>(rect, ViewBkColor, 1, 0.0f, 1.0f);
		//最初のビューのビューのライトの設定
		auto PtrLight = PtrView->GetMultiLight()->GetLight(0);
		PtrLight->SetPositionToDirectional(-0.25f, 1.0f, -0.25f);
		//ビューのカメラの設定
		auto PtrCamera = PtrView->GetCamera();
		PtrCamera->SetEye(Vector3(0.0f, 0.0f, -5.0f));
		PtrCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));
	}

	//暗幕スプライトの作成
	void StareStage::CreateBlindSprite(){
		vector<Vector3>Vec = {
			{ 750.0f, 380.0f, 0 },
		};
		for (auto v : Vec){
			auto ptr = AddGameObject<BlindSprite>(v);
			SetSharedGameObject(L"Blind", ptr);
		}
	}

	//素材提供先スプライトの作成
	void StareStage::CreateThxSprite(){
		vector<Vector3>Vec = {
			{ 650.0f, 800.0f, 0 },
		};
		for (auto v : Vec){
			auto ptr = AddGameObject<ThxSprite>(v);
			SetSharedGameObject(L"forThx", ptr);
		}
	}

	void StareStage::DeleteBlind_and_Thx()
	{
		auto thxPtr = GetSharedGameObject<ThxSprite>(L"forThx", false);
		auto BlindPtr = GetSharedGameObject<BlindSprite>(L"Blind", false);

		//時間加算用変数エラプスドタイム
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		//Timerへ加算
		Timer += ElapsedTime;

		if (Timer > 3)
		{

			thxPtr->SetDrawActive(false);
			BlindPtr->SetDrawActive(false);
			PostEvent(1.0f, GetThis<StareStage>(), App::GetApp()->GetSceneBase(), L"ToTitle");
		}
	}

	

	//初期化
	void StareStage::OnCreate(){
		try{

			CreateResourses();
			CreateViews();
			CreateBlindSprite();
			CreateThxSprite();

		}
		catch (...){
			throw;
		}
	}

	//操作
	void StareStage::OnUpdate(){

		DeleteBlind_and_Thx();

	}

	//--------------------------------------------------------------------------------------
	//	class TitelStage : public Stage;
	//	用途: タイトルステージクラス
	//--------------------------------------------------------------------------------------
	//リソースの作成
	void TitelStage::CreateResourses(){
		wstring strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Titel.png";
		App::GetApp()->RegisterTexture(L"TITEL_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Start.png";
		App::GetApp()->RegisterTexture(L"START_TX", strTexture);

		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"thx.png";//素材提供先Spriteの追加
		App::GetApp()->RegisterTexture(L"THX_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Blind_Black.png";//暗幕(黒)Spriteの追加
		App::GetApp()->RegisterTexture(L"Blind_TX", strTexture);


		wstring NanikaWav = App::GetApp()->m_wstrRelativeDataPath + L"Morning.wav";
		App::GetApp()->RegisterWav(L"MORNING", NanikaWav);
		NanikaWav = App::GetApp()->m_wstrRelativeDataPath + L"Button.wav";
		App::GetApp()->RegisterWav(L"BUTTON", NanikaWav);
	}
	//ビュー類の作成
	void TitelStage::CreateViews(){
		//最初にデフォルトのレンダリングターゲット類を作成する
		//パラメータの2048.0fはシャドウマップのサイズ。大きいほど影が細かくなる（4096.0fなど）
		//影を細かくするとメモリを大量に消費するので注意！
		CreateDefaultRenderTargets(2048.0f);
		//影のビューサイズの設定。この値を小さくすると影が表示される範囲が小さくなる。
		//値が小さいほうが影は細かくなる
		//スタティック変数なので一度設定したらその値はステージを超えて保持される。
		Shadowmap::SetViewSize(32.0f);
		//マルチビューコンポーネントの取得
		auto PtrMultiView = GetComponent<MultiView>();
		//マルチビューにビューの追加
		auto PtrView = PtrMultiView->AddView();
		//ビューの矩形を設定（ゲームサイズ全体）
		Rect2D<float> rect(0, 0, (float)App::GetApp()->GetGameWidth(),
			(float)App::GetApp()->GetGameHeight());
		//ビューの背景色
		Color4 ViewBkColor(0.0f, 0.0f, 0.0f, 1.0f);
		//最初のビューにパラメータの設定
		PtrView->ResetParamaters<MyCamera, MultiLight>(rect, ViewBkColor, 1, 0.0f, 1.0f);
		//最初のビューのビューのライトの設定
		auto PtrLight = PtrView->GetMultiLight()->GetLight(0);
		PtrLight->SetPositionToDirectional(-0.25f, 1.0f, -0.25f);
		//ビューのカメラの設定
		auto PtrCamera = PtrView->GetCamera();
		PtrCamera->SetEye(Vector3(0.0f, 0.0f, -5.0f));
		PtrCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));
	}


	//タイトルスプライトの作成
	void TitelStage::CreateTitel(){
		vector<Vector3>Vec = {
			{ 650.0f, 500.0f, 0 },
		};
		for (auto v : Vec){
			AddGameObject<TitelSprite>(v);
		}
	}

	void TitelStage::CreateStart(){
		vector<Vector3>Vec = {
			{ 650.0f, 100.0f, 0 },
		};
		for (auto v : Vec){
			auto ptr = AddGameObject<StartSprite>(v);
			SetSharedGameObject(L"Start", ptr);
		}
	}

	//初期化
	void TitelStage::OnCreate(){
		try{

			StageNumber = 1;
			CreateResourses();
			CreateViews();
			CreateTitel();
			CreateStart();
			
			//音楽の再生
			AddMusic();

			//ステートマシンの構築
			m_StateMachine = make_shared< StateMachine<TitelStage> >(GetThis<TitelStage>());
			//最初のステートをDefaultStateに設定
			m_StateMachine->SetCurrentState(TitelState::Instance());
			//DefaultStateの初期化実行を行う
			m_StateMachine->GetCurrentState()->Enter(GetThis<TitelStage>());

		}
		catch (...){
			throw;
		}
	}

	//操作
	void TitelStage::OnUpdate(){

		//ステートマシンのUpdateを行う
		//この中でステートの切り替えが行われる
		m_StateMachine->Update();

	}

	//ミュージック(BGM)の再生
	void TitelStage::AddMusic()
	{
		//ミュージックコンポーネントを登録.
		auto pMusic = AddComponent<PlayMusic>(L"MORNING");
		//再生（繰り返し、0.5ボリューム）
		pMusic->Start(XAUDIO2_LOOP_INFINITE, 1.0f);
	}

	bool TitelStage::StartMotion(){
		//コントローラの取得
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].bConnected){
			//Aボタンが押された
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A){
				return true;
			}
		}
		return false;
	}

	void TitelStage::SceneMotion(){
		auto Startptr = GetSharedGameObject<StartSprite>(L"Start");
		Startptr->SetSpeed(1.0f);
		auto pMusic = AddComponent<PlayMusic>(L"MORNING");

		pMusic->Stop();

		App::GetApp()->GetScene<Scene>()->SetStageNumber(StageNumber);

		//イベント送出
		PostEvent(1.0f, GetThis<TitelStage>(), App::GetApp()->GetSceneBase(), L"ToMenu");

	}

	//--------------------------------------------------------------------------------------
	//	class TitelState : public ObjState<TitelStage>;
	//	用途: スタートステート
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<TitelState> TitelState::Instance(){
		static shared_ptr<TitelState> instance;
		if (!instance){
			instance = shared_ptr<TitelState>(new TitelState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void TitelState::Enter(const shared_ptr<TitelStage>& Obj){
	}
	//ステート実行中に毎ターン呼ばれる関数
	void TitelState::Execute(const shared_ptr<TitelStage>& Obj){

	
		if (Obj->StartMotion()){

			Obj->GetStateMachine()->ChangeState(StartState::Instance());

		}
		
	}
	//ステートにから抜けるときに呼ばれる関数
	void TitelState::Exit(const shared_ptr<TitelStage>& Obj){
		//何もしない
	}

	//--------------------------------------------------------------------------------------
	//	class StartState : public ObjState<TitelStage>;
	//	用途: スタートステート
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<StartState> StartState::Instance(){
		static shared_ptr<StartState> instance;
		if (!instance){
			instance = shared_ptr<StartState>(new StartState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void StartState::Enter(const shared_ptr<TitelStage>& Obj){
		Obj->SceneMotion();
		
	}
	//ステート実行中に毎ターン呼ばれる関数
	void StartState::Execute(const shared_ptr<TitelStage>& Obj){
		//Obj->DeleteBlind_and_Thx();
	}
	//ステートにから抜けるときに呼ばれる関数
	void StartState::Exit(const shared_ptr<TitelStage>& Obj){
		//何もしない
	}

	//--------------------------------------------------------------------------------------
	//	class MenuStage : public Stage;
	//	用途: メニューステージクラス
	//--------------------------------------------------------------------------------------
	//リソースの作成
	void MenuStage::CreateResourses(){
		wstring strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Stage1.png";
		App::GetApp()->RegisterTexture(L"STAGE1_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Stage2.png";//ライフ回復アイテムのTexture
		App::GetApp()->RegisterTexture(L"STAGE2_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Stage3.png";//パワーアップアイテムのTexture
		App::GetApp()->RegisterTexture(L"STAGE3_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"LeftArrow.png";
		App::GetApp()->RegisterTexture(L"LEFT_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"RightArrow.png";
		App::GetApp()->RegisterTexture(L"RIGHT_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Menu.png";
		App::GetApp()->RegisterTexture(L"Menu_TX", strTexture);

		

		wstring NanikaWav = App::GetApp()->m_wstrRelativeDataPath + L"Menu.wav";
		App::GetApp()->RegisterWav(L"MENU", NanikaWav);
		NanikaWav = App::GetApp()->m_wstrRelativeDataPath + L"Button.wav";
		App::GetApp()->RegisterWav(L"BUTTON", NanikaWav);
		NanikaWav = App::GetApp()->m_wstrRelativeDataPath + L"cursor.wav";
		App::GetApp()->RegisterWav(L"Cursor", NanikaWav);

		
	}

	//ビューの作成
	void MenuStage::CreateViews(){
		//最初にデフォルトのレンダリングターゲット類を作成する
		//パラメータの2048.0fはシャドウマップのサイズ。大きいほど影が細かくなる（4096.0fなど）
		//影を細かくするとメモリを大量に消費するので注意！
		CreateDefaultRenderTargets(2048.0f);
		//影のビューサイズの設定。この値を小さくすると影が表示される範囲が小さくなる。
		//値が小さいほうが影は細かくなる
		//スタティック変数なので一度設定したらその値はステージを超えて保持される。
		Shadowmap::SetViewSize(32.0f);
		//マルチビューコンポーネントの取得
		auto PtrMultiView = GetComponent<MultiView>();
		//マルチビューにビューの追加
		auto PtrView = PtrMultiView->AddView();
		//ビューの矩形を設定（ゲームサイズ全体）
		Rect2D<float> rect(0, 0, (float)App::GetApp()->GetGameWidth(),
			(float)App::GetApp()->GetGameHeight());
		//ビューの背景色
		Color4 ViewBkColor(0.0f, 0.0f, 0.0f, 1.0f);
		//最初のビューにパラメータの設定
		PtrView->ResetParamaters<MyCamera, MultiLight>(rect, ViewBkColor, 1, 0.0f, 1.0f);
		//最初のビューのビューのライトの設定
		auto PtrLight = PtrView->GetMultiLight()->GetLight(0);
		PtrLight->SetPositionToDirectional(-0.25f, 1.0f, -0.25f);
		//ビューのカメラの設定
		auto PtrCamera = PtrView->GetCamera();
		PtrCamera->SetEye(Vector3(12.0f, 30.0f, 12.0f));
		PtrCamera->SetAt(Vector3(12.0f, 0.0f, 12.0f));
		PtrCamera->SetUp(Vector3(0.0f, 0.0f, 1.0f));  //上から見るのでUpを変える


	}
	//メニュースプライトの作成
	void MenuStage::CreateMenu(){
		vector<Vector3>Vec = {
			{ 650.0f, 700.0f, 0 },
		};
		for (auto v : Vec){
			auto ptr = AddGameObject<MenuSprite>(v);
			SetSharedGameObject(L"Menu", ptr);

		}
	}

	//簡単ステージスプライトの作成
	void MenuStage::CreateEasy(){
		vector<Vector3>Vec = {
			{ 650.0f, 500.0f, 0 },
		};
		for (auto v : Vec){
			auto ptr = AddGameObject<EasySprite>(v);
			SetSharedGameObject(L"Easy", ptr);

		}
	}

	//普通ステージスプライトの作成
	void MenuStage::CreateNormal(){
		vector<Vector3>Vec = {
			{ 650.0f, 300.0f, 0 },
		};
		for (auto v : Vec){
			auto ptr = AddGameObject<NormalSprite>(v);
			SetSharedGameObject(L"Normal", ptr);

		}
	}
	//難しいステージスプライトの作成
	void MenuStage::CreateHard(){
		vector<Vector3>Vec = {
			{ 650.0f, 100.0f, 0 },
		};
		for (auto v : Vec){
			auto ptr = AddGameObject<HardSprite>(v);
			SetSharedGameObject(L"Hard", ptr);

		}
	}

	//初期化
	void MenuStage::OnCreate(){
		try{
			//ステートマシンの構築
			m_StateMachine = make_shared< StateMachine<MenuStage> >(GetThis<MenuStage>());
			//最初のステートをDefaultStateに設定
			m_StateMachine->SetCurrentState(StartGameState::Instance());
			//DefaultStateの初期化実行を行う
			m_StateMachine->GetCurrentState()->Enter(GetThis<MenuStage>());

			CreateResourses();
			CreateEasy();
			CreateViews();
			AddMusic();
			CreateNormal();
			CreateHard();
			CreateMenu();

			auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
			pMultiSoundEffect->AddAudioResource(L"BUTTON");//Playerより持ってきましたSoner.SE
			pMultiSoundEffect->AddAudioResource(L"Cursor");

		}
		catch (...){
			throw;
		}
	}

	//操作
	void MenuStage::OnUpdate(){

		//ステートマシンのUpdateを行う
		//この中でステートの切り替えが行われる
		m_StateMachine->Update();

	}

	bool MenuStage::ToPlayMotion(){
		//コントローラ情報の取得.
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].bConnected){
			//Aボタンが押されたら.
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A) {
				App::GetApp()->GetScene<Scene>()->SetStageNumber(StageNumber);
				return true;
			}
		}
		return false;
	}

	//ミュージック(BGM)の再生
	void MenuStage::AddMusic()
	{
		//ミュージックコンポーネントを登録.
		auto pMusic = AddComponent<PlayMusic>(L"MENU");
		//再生（繰り返し、0.5ボリューム）
		pMusic->Start(XAUDIO2_LOOP_INFINITE, 0.5f);
	}

	void MenuStage::ToMenuMotion(){

		auto pMusic = AddComponent<PlayMusic>(L"MENU");
		pMusic->Stop();

		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->Start(L"BUTTON", 0, 1.0f);

		//イベント送出
			if (StageNumber == 1)//選択されたステージが1番だった時、チュートリアルを挟む
			{
			PostEvent(1.0f, GetThis<MenuStage>(), App::GetApp()->GetSceneBase(), L"ToTotriul");
		}
			else
			{
				PostEvent(1.0f, GetThis<MenuStage>(), App::GetApp()->GetSceneBase(), L"ToGame");
			}
	}

	void MenuStage::GetNum(){
	
		//シーンからゲームセレクトで選んだゲームステージの番号取得
		int i_StageNumber = App::GetApp()->GetScene<Scene>()->GetStageNumber();

		StageNumber = i_StageNumber;

	}

	//ステージセレクトを行う
	void MenuStage::ToChangeMotion(){
		//コントローラ情報の取得.
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].bConnected){
			if (CntlVec[0].fThumbLY == 0.0f){
				Change = false;
			
			}
			if (CntlVec[0].fThumbLY >= 0.5f && !Change ){

				auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
				pMultiSoundEffect->Start(L"Cursor", 0, 1.0f);
				Change = true;
				StageNumber -= 1;
				App::GetApp()->GetScene<Scene>()->SetStageNumber(StageNumber);			
			}
			if (CntlVec[0].fThumbLY <= -0.5f && !Change ){
				
				auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
				pMultiSoundEffect->Start(L"Cursor", 0, 1.0f);
				Change = true;
				StageNumber += 1;
				App::GetApp()->GetScene<Scene>()->SetStageNumber(StageNumber);

			}
		}

		if (StageNumber <= 0){
			StageNumber = 1;
			App::GetApp()->GetScene<Scene>()->SetStageNumber(StageNumber);
	
		}

		if (StageNumber > 3){
		
			StageNumber = 3;
			App::GetApp()->GetScene<Scene>()->SetStageNumber(StageNumber);

		}
	
	}


	void MenuStage::Action()
	{
		//時間でスケーリング変更するための準備
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		ActionTime += ElapsedTime;
		Vector3 Scale = m_Scale;//スプライトのスケール格納&変更用変数(丁度いい名前が思い浮かびません)

		//初期スケールを設定
		Scale.x = 1.0f;
		Scale.y = 1.0f;

		//スケール変更用の計算式？を定義
		float ScaleChanger = sin(ActionTime);

		//X方向へのスケーリング
		Scale.x += ScaleChanger;
		//Y方向へのスケーリング
		Scale.y += ScaleChanger;

		//各スケーリングが反転しないように調整
		if (Scale.x <= 1.0)
		{
			Scale.x = 1;
			Scale.y = 1;
			ActionTime = 0;
		}

		//プレイヤーが1番を選択していた時、タイトルスプライトを点滅させる
		if (StageNumber == 1)
		{
			auto SpritePtr = GetSharedGameObject<EasySprite>(L"Easy", false);
			auto PtrTransform = SpritePtr->AddComponent<Transform>();
			PtrTransform->SetScale(Scale);
		}
		else
			//プレイヤーが2番を選択していた時、タイトルスプライトを点滅させる	
			if (StageNumber == 2)
			{
				auto SpritePtr = GetSharedGameObject<NormalSprite>(L"Normal", false);
				auto PtrTransform = SpritePtr->AddComponent<Transform>();
				PtrTransform->SetScale(Scale);
			}
			else
			if(StageNumber == 3){
				auto SpritePtr = GetSharedGameObject<HardSprite>(L"Hard", false);
				auto PtrTransform = SpritePtr->AddComponent<Transform>();
				PtrTransform->SetScale(Scale);

			}
	}


	void MenuStage::RefreshAction()
	{
		auto NormalPtr = GetSharedGameObject<NormalSprite>(L"Normal", false);
		auto EasyPtr = GetSharedGameObject<EasySprite>(L"Easy", false);
		auto HardPtr = GetSharedGameObject<HardSprite>(L"Hard", false);


		if (StageNumber == 1)
		{
			NormalPtr->Rifmotion();
		}

		if (StageNumber == 2)
		{
			EasyPtr->Rifmotion();
			HardPtr->Rifmotion();

		}
		if (StageNumber == 3)
		{
			NormalPtr->Rifmotion();
		}
	}




	//--------------------------------------------------------------------------------------
	//	class StartGameState : public ObjState<MenuStage>;
	//	用途: スタートステート
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<StartGameState> StartGameState::Instance(){
		static shared_ptr<StartGameState> instance;
		if (!instance){
			instance = shared_ptr<StartGameState>(new StartGameState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void StartGameState::Enter(const shared_ptr<MenuStage>& Obj){
		Obj->GetNum();

	}
	//ステート実行中に毎ターン呼ばれる関数
	void StartGameState::Execute(const shared_ptr<MenuStage>& Obj){
		
		Obj->ToChangeMotion();
		Obj->Action();
		Obj->RefreshAction();
		if (Obj->ToPlayMotion()){

			Obj->GetStateMachine()->ChangeState(ToGameState::Instance());
		}
	}
	//ステートにから抜けるときに呼ばれる関数
	void StartGameState::Exit(const shared_ptr<MenuStage>& Obj){
		//何もしない
	}

	//--------------------------------------------------------------------------------------
	//	class ToGameState : public ObjState<MenuStage>;
	//	用途: スタートステート
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<ToGameState> ToGameState::Instance(){
		static shared_ptr<ToGameState> instance;
		if (!instance){
			instance = shared_ptr<ToGameState>(new ToGameState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void ToGameState::Enter(const shared_ptr<MenuStage>& Obj){
		Obj->ToMenuMotion();

	}
	//ステート実行中に毎ターン呼ばれる関数
	void ToGameState::Execute(const shared_ptr<MenuStage>& Obj){

	}
	//ステートにから抜けるときに呼ばれる関数
	void ToGameState::Exit(const shared_ptr<MenuStage>& Obj){
		//何もしない
	}

	//--------------------------------------------------------------------------------------
	//	class TutorialStage : public Stage;
	//	用途:　チュートリアルステージクラス
	//--------------------------------------------------------------------------------------
	//リソースの作成
	void TutorialStage::CreateResourses(){
		wstring strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Tutorial.png";
		App::GetApp()->RegisterTexture(L"TUTORIAL_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Next.png";
		App::GetApp()->RegisterTexture(L"NEXT_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Prev.png";
		App::GetApp()->RegisterTexture(L"PREV_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"controller.png";
		App::GetApp()->RegisterTexture(L"PLAYRULE_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"ItemRule.png";
		App::GetApp()->RegisterTexture(L"ITEMRULE_TX", strTexture);

		wstring NanikaWav = App::GetApp()->m_wstrRelativeDataPath + L"Button.wav";
		App::GetApp()->RegisterWav(L"BUTTON", NanikaWav);
		NanikaWav = App::GetApp()->m_wstrRelativeDataPath + L"cursor.wav";
		App::GetApp()->RegisterWav(L"Cursor", NanikaWav);
		
	}

	//ビューの作成
	void TutorialStage::CreateViews(){
		//最初にデフォルトのレンダリングターゲット類を作成する
		//パラメータの2048.0fはシャドウマップのサイズ。大きいほど影が細かくなる（4096.0fなど）
		//影を細かくするとメモリを大量に消費するので注意！
		CreateDefaultRenderTargets(2048.0f);
		//影のビューサイズの設定。この値を小さくすると影が表示される範囲が小さくなる。
		//値が小さいほうが影は細かくなる
		//スタティック変数なので一度設定したらその値はステージを超えて保持される。
		Shadowmap::SetViewSize(32.0f);
		//マルチビューコンポーネントの取得
		auto PtrMultiView = GetComponent<MultiView>();
		//マルチビューにビューの追加
		auto PtrView = PtrMultiView->AddView();
		//ビューの矩形を設定（ゲームサイズ全体）
		Rect2D<float> rect(0, 0, (float)App::GetApp()->GetGameWidth(), (float)App::GetApp()->GetGameHeight());
		//ビューの背景色
		Color4 ViewBkColor(0.0f, 0.0f, 0.0f, 1.0f);
		//最初のビューにパラメータの設定
		PtrView->ResetParamaters<LookAtCamera, MultiLight>(rect, ViewBkColor, 1, 0.0f, 1.0f);
		//0番目のビューのカメラを得る
		auto PtrCamera = GetCamera(0);
		PtrCamera->SetEye(Vector3(0.0f, 0.0f, -5.0f));
		PtrCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));
	}

	//メニュースプライトの作成(メニューの画像を張り付けるよ！)
	void TutorialStage::CreateToTutorial(){
		vector<Vector3>Vec = {
			{ 650.0f, 500.0f, 0 },
		};
		for (auto v : Vec){
			AddGameObject<ToTutorialSprite>(v);
		}
	}

	//『Next』スプライトの作成
	void TutorialStage::CreateToNextSprite()
	{
		vector<Vector3>Vec =
		{
			{ 650.0f, 500.0f, 0 }
		};
		for (auto v : Vec){
			AddGameObject<ToNextSprite>(v);
		}
	}

	//『Prev』スプライトの作成
	void TutorialStage::CreateToPrevSprite()
	{
		vector<Vector3>Vec =
		{
			{ 650.0f, 500.0f, 0 }
		};
		for (auto v : Vec){
			AddGameObject<ToPrevSprite>(v);
		}
	}

	//初期化
	void TutorialStage::OnCreate(){
		try{
			//ステートマシンの構築
			m_StateMachine = make_shared< StateMachine<TutorialStage> >(GetThis<TutorialStage>());
			//最初のステートをDefaultStateに設定
			m_StateMachine->SetCurrentState(StartTutorialState::Instance());
			//DefaultStateの初期化実行を行う
			m_StateMachine->GetCurrentState()->Enter(GetThis<TutorialStage>());

			CreateResourses();
			CreateViews();
			CreateToTutorial();
			CreateToNextSprite();
			CreateToPrevSprite();

			auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
			pMultiSoundEffect->AddAudioResource(L"BUTTON");//Playerより持ってきましたSoner.SE
		}
		catch (...){
			throw;
		}
	}

	//操作
	void TutorialStage::OnUpdate(){

		//ステートマシンのUpdateを行う
		//この中でステートの切り替えが行われる
		m_StateMachine->Update();

		if (count <= 0){
		
			count = 0;
		}

	}

	bool TutorialStage::ToPlayMotion(){
		//コントローラ情報の取得.
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].bConnected){
			//Aボタンが押されたら.
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A) {
				count++;
				if (count > 1){
					App::GetApp()->GetScene<Scene>()->SetStageNumber(1);//Stageナンバーを取得
					return true;
				}
			}
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_B) {
				count--;
			}
		}
		return false;
	}

	void TutorialStage::ToTutorMotion()
	{
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->Start(L"BUTTON", 0, 1.0f);

		PostEvent(1.0f, GetThis<TutorialStage>(), App::GetApp()->GetSceneBase(), L"ToGame");
	}

	//--------------------------------------------------------------------------------------
	//	class StartTutorialState : public ObjState<MenuStage>;
	//	用途: スタートステート
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<StartTutorialState> StartTutorialState::Instance(){
		static shared_ptr<StartTutorialState> instance;
		if (!instance){
			instance = shared_ptr<StartTutorialState>(new StartTutorialState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void StartTutorialState::Enter(const shared_ptr<TutorialStage>& Obj){

		//		Obj->GetNum();
	}
	//ステート実行中に毎ターン呼ばれる関数
	void StartTutorialState::Execute(const shared_ptr<TutorialStage>& Obj){

		//Obj->ToPlayMotion();

		//Obj->ToChangeMotion();
		if (Obj->ToPlayMotion()){

			Obj->GetStateMachine()->ChangeState(EndTutorialState::Instance());
		}

	}
	//ステートにから抜けるときに呼ばれる関数
	void StartTutorialState::Exit(const shared_ptr<TutorialStage>& Obj){
		//何もしない
	}

	//--------------------------------------------------------------------------------------
	//	class EndTutorialState : public ObjState<MenuStage>;
	//	用途: スタートステート
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<EndTutorialState> EndTutorialState::Instance(){
		static shared_ptr<EndTutorialState> instance;
		if (!instance){
			instance = shared_ptr<EndTutorialState>(new EndTutorialState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void EndTutorialState::Enter(const shared_ptr<TutorialStage>& Obj){
		Obj->ToTutorMotion();
	}
	//ステート実行中に毎ターン呼ばれる関数
	void EndTutorialState::Execute(const shared_ptr<TutorialStage>& Obj){

	}
	//ステートにから抜けるときに呼ばれる関数
	void EndTutorialState::Exit(const shared_ptr<TutorialStage>& Obj){
		//何もしない
	}

	//

	//--------------------------------------------------------------------------------------
	//	class GameClearStage : public Stage;
	//	用途: ゲームクリアステージクラス
	//--------------------------------------------------------------------------------------
	//リソースの作成
	void GameClearStage::CreateResourses(){
		wstring	strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Clear.png";
		App::GetApp()->RegisterTexture(L"CREAR_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"ToMenu.png";//「メニューへ」Spriteの追加
		App::GetApp()->RegisterTexture(L"GOtoMENU_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"ToTitle.png";//「タイトルへ」Spriteの追加
		App::GetApp()->RegisterTexture(L"GOtoTITLE_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"SCORE.png";//「SCORE」Spriteの追加
		App::GetApp()->RegisterTexture(L"SCORE_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"TIME.png";//「TIME」Spriteの追加
		App::GetApp()->RegisterTexture(L"TIME_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"RANK.png";//「RANK」Spriteの追加
		App::GetApp()->RegisterTexture(L"RANK_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"RANK_A.png";//「RANK_A」Spriteの追加
		App::GetApp()->RegisterTexture(L"RANK_A_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"RANK_B.png";//「RANK_B」Spriteの追加
		App::GetApp()->RegisterTexture(L"RANK_B_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"RANK_C.png";//「RANK_C」Spriteの追加
		App::GetApp()->RegisterTexture(L"RANK_C_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"number.png";//0~9の数字Texture
		App::GetApp()->RegisterTexture(L"NUMBERs_TX", strTexture);
		

		wstring NanikaWav = App::GetApp()->m_wstrRelativeDataPath + L"GameClear.wav";
		App::GetApp()->RegisterWav(L"Clear", NanikaWav);
		NanikaWav = App::GetApp()->m_wstrRelativeDataPath + L"Button.wav";
		App::GetApp()->RegisterWav(L"BUTTON", NanikaWav);
		NanikaWav = App::GetApp()->m_wstrRelativeDataPath + L"cursor.wav";
		App::GetApp()->RegisterWav(L"Cursor", NanikaWav);

		
			}

	//ビューの作成
	void GameClearStage::CreateViews(){
		//最初にデフォルトのレンダリングターゲット類を作成する
		//パラメータの2048.0fはシャドウマップのサイズ。大きいほど影が細かくなる（4096.0fなど）
		//影を細かくするとメモリを大量に消費するので注意！
		CreateDefaultRenderTargets(2048.0f);
		//影のビューサイズの設定。この値を小さくすると影が表示される範囲が小さくなる。
		//値が小さいほうが影は細かくなる
		//スタティック変数なので一度設定したらその値はステージを超えて保持される。
		Shadowmap::SetViewSize(32.0f);
		//マルチビューコンポーネントの取得
		auto PtrMultiView = GetComponent<MultiView>();
		//マルチビューにビューの追加
		auto PtrView = PtrMultiView->AddView();
		//ビューの矩形を設定（ゲームサイズ全体）
		Rect2D<float> rect(0, 0, (float)App::GetApp()->GetGameWidth(), (float)App::GetApp()->GetGameHeight());
		//ビューの背景色
		Color4 ViewBkColor(0.0f, 0.0f, 0.0f, 1.0f);
		//最初のビューにパラメータの設定
		PtrView->ResetParamaters<LookAtCamera, MultiLight>(rect, ViewBkColor, 1, 0.0f, 1.0f);
		//0番目のビューのカメラを得る
		auto PtrCamera = GetCamera(0);
		PtrCamera->SetEye(Vector3(0.0f, 0.0f, -5.0f));
		PtrCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));
	}

	//ゲームクリアスプライトの作成
	void GameClearStage::CreateToClear(){
		vector<Vector3>Vec = {
			{ 650.0f, 680.0f, 0 },
		};
		for (auto v : Vec){
			AddGameObject<ClearSprite>(v);
		}
	}

	//「メニューへ」スプライトの作成
	void GameClearStage::CreateGoToMenu()
	{
		vector<Vector3>Vec =
		{
			{ 1000.0f, 150.0f, 0 },
		};
		for (auto v : Vec){
			auto GoToMenu = AddGameObject<GoToMenuSprite>(v);
			SetSharedGameObject(L"GoToMenu", GoToMenu);
		}
	}

	//「タイトルへ」スプライトの作成
	void GameClearStage::CreateGoToTitle()
	{
		vector<Vector3>Vec =
		{
			{ 300.0f, 150.0f, 0 },
		};
		for (auto v : Vec){
			auto GoToTitle = AddGameObject<GoToTitleSprite>(v);
			SetSharedGameObject(L"GoToTitle", GoToTitle);
		}
	}

	void GameClearStage::SpriteAction()
	{
		//時間でスケーリング変更するための準備
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		ActionTime += ElapsedTime;
		Vector3 Scale = m_Scale;//スプライトのスケール格納&変更用変数(丁度いい名前が思い浮かびません)

		//初期スケールを設定
		Scale.x = 1.0f;
		Scale.y = 1.0f;

		//スケール変更用の計算式？を定義
		float ScaleChanger = sin(ActionTime);

		//X方向へのスケーリング
		Scale.x += ScaleChanger;
		//Y方向へのスケーリング
		Scale.y += ScaleChanger;

		//各スケーリングが反転しないように調整
		if (Scale.x <= 1.0)
		{
			Scale.x = 1;
			Scale.y = 1;
			ActionTime = 0;
		}

		//プレイヤーが1番を選択していた時、タイトルスプライトを点滅させる
		if (Number == 1)
		{
			auto SpritePtr = GetSharedGameObject<GoToTitleSprite>(L"GoToTitle", false);
			auto PtrTransform = SpritePtr->AddComponent<Transform>();
			PtrTransform->SetScale(Scale);
		}
		else
			//プレイヤーが2番を選択していた時、タイトルスプライトを点滅させる	
			if (Number == 2)
			{
				auto SpritePtr = GetSharedGameObject<GoToMenuSprite>(L"GoToMenu", false);
				auto PtrTransform = SpritePtr->AddComponent<Transform>();
				PtrTransform->SetScale(Scale);

			}
	}
	//

	//
	void GameClearStage::RefreshAction()
	{
		if (Number == 1)
		{
			auto SpritePtr = GetSharedGameObject<GoToMenuSprite>(L"GoToMenu", false);
			auto PtrTransform = SpritePtr->AddComponent<Transform>();
			PtrTransform->SetScale(1, 1, 1);
		}

		if (Number == 2)
		{
			auto SpritePtr = GetSharedGameObject<GoToTitleSprite>(L"GoToTitle", false);
			auto PtrTransform = SpritePtr->AddComponent<Transform>();
			PtrTransform->SetScale(1, 1, 1);
		}
	}

	void GameClearStage::ChangeMotion(){
		//コントローラ情報の取得.
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].bConnected){
			if (CntlVec[0].fThumbLX == 0.0f){
				Change = false;

			}
			if (CntlVec[0].fThumbLX >= 0.5f && !Change){

				Change = true;
				Number += 1;
				//SEの再生
			auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
				pMultiSoundEffect->Start(L"Cursor", 0, 1.0f);

			}
			if (CntlVec[0].fThumbLX <= -0.5f && !Change){

				Change = true;
				Number -= 1;
				//SEの再生
				auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
				pMultiSoundEffect->Start(L"Cursor", 0, 1.0f);
			}
		}

		if (Number <= 0){
			Number = 1;
		}
		if (Number > 2){
			Number = 2;
		}

	}
	void GameClearStage::RESULT(){

		//GameScoreの1の位のシェアードゲームオブジェクト?を更新
		auto ptr_Num = GetSharedGameObject<NumberSprite>(L"ResultNum_OnePlace");
		ptr_Num->SetNum(CLEARTIME);
		//GameScoreの10の位のシェアードゲームオブジェクト?を更新
		ptr_Num = GetSharedGameObject<NumberSprite>(L"ResultNum_TenPlace");
		ptr_Num->SetNum(CLEARTIME / 10);
		//GameScoreの100の位のシェアードゲームオブジェクト?を更新
		ptr_Num = GetSharedGameObject<NumberSprite>(L"ResultNum_HandredPlace");
		ptr_Num->SetNum(CLEARTIME / 100);
	}

	//初期化
	void GameClearStage::OnCreate(){
		try{
			//ステートマシンの構築
			m_StateMachine = make_shared< StateMachine<GameClearStage> >(GetThis<GameClearStage>());
			//最初のステートをDefaultStateに設定
			m_StateMachine->SetCurrentState(GameClear_RESULT_State::Instance());
			//DefaultStateの初期化実行を行う
			m_StateMachine->GetCurrentState()->Enter(GetThis<GameClearStage>());
			//リソースの作成
			CreateResourses();

			CreateToClear();
			CreateClearTime();
			CreateTime();
			ClearRank();

			CreateViews();
			AddMusic();
			auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
			pMultiSoundEffect->AddAudioResource(L"BUTTON");
			pMultiSoundEffect->AddAudioResource(L"Cursor");		
		}
		catch (...){
			throw;
		}
	}

	//操作
	void GameClearStage::OnUpdate(){

		//ステートマシンのUpdateを行う
		//この中でステートの切り替えが行われる
		m_StateMachine->Update();

	}

	bool GameClearStage::ToTitelMotion(){
		//コントローラ情報の取得.
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].bConnected){
			//Aボタンが押されたら.
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A) {
				ChangeNum = Number;
				return true;
			}
		}
		return false;
	}

	//ミュージック(BGM)の再生
	void GameClearStage::AddMusic()
	{
		//ミュージックコンポーネントを登録.
		auto pMusic = AddComponent<PlayMusic>(L"Clear");
		//再生（繰り返し、0.5ボリューム）
		pMusic->Start(0, 0.5f);
	}

	void GameClearStage::ToChangeMotion(){
		//ミュージックコンポーネントを登録.
		auto pMusic = AddComponent<PlayMusic>(L"Clear");
		//停止
		pMusic->Stop();

		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->Start(L"BUTTON", 0, 1.0f);

		//イベント送出
		if (ChangeNum == 1){
			PostEvent(1.0f, GetThis<GameClearStage>(), App::GetApp()->GetSceneBase(), L"ToTitle");
		}
		if (ChangeNum == 2){
			PostEvent(1.0f, GetThis<GameClearStage>(), App::GetApp()->GetSceneBase(), L"ToMenu");
		}
		
	}

	//「TIME」スプライトの作成
	void GameClearStage::CreateTime()
	{
			AddGameObject<TIMESprite>(Vector3(425.0f, 500.0f, 0), Vector3(1.5f, 1.0f, 1.0f));
	}
	//「RANK」スプライトの作成
	void GameClearStage::ClearRank()
	{
		vector<Vector3>Vec = {
			{ 425.0f, 350.0f, 0 },
		};
		for (auto v : Vec){
			AddGameObject<RANKSprite>(v);
		}
	}

	//クリアタイムを格納する
	void GameClearStage::SetClearTime()
	{
		Timer += ElapsedTime;
		if (Timer < 3)
		{
			RandomCounterAnimation();
		}
		else
		{
			//SCENEから格納された時間を持ってくる
			auto setScore = App::GetApp()->GetScene<Scene>()->GetSCORE();
			CLEARTIME = setScore;
		}
		if (4 < Timer)
		{
			ClearRANK();	
		}
		if (5 < Timer)
		{
			GetStateMachine()->ChangeState(GameClear_SCENECHOICE_State::Instance());
		}
	}

	//クリアタイムからランクを割り当てる
	void GameClearStage::SetClearRANK()
	{
		if (CLEARTIME < 100)
		{
			CLEARRANK = 0;
		}
	}

	//クリアタイムを表示
	void GameClearStage::CreateClearTime()
	{
		//TimerSpriteの1の位のシェアードゲームオブジェクト?を作成
		auto ptr = AddGameObject<NumberSprite>(Vector3(790, 300, 0), Vector3(2, 2, 2));//yを350から250へ
		SetSharedGameObject(L"ResultNum_OnePlace", ptr);
		//TimerSpriteの10の位のシェアードゲームオブジェクト?を作成
		ptr = AddGameObject<NumberSprite>(Vector3(700, 300, 0), Vector3(2, 2, 2));//yを350から250へ
		SetSharedGameObject(L"ResultNum_TenPlace", ptr);
		//TimerSpriteの100の位のシェアードゲームオブジェクト?を作成
		ptr = AddGameObject<NumberSprite>(Vector3(610, 300, 0), Vector3(2, 2, 2));//yを350から250へ
		SetSharedGameObject(L"ResultNum_HandredPlace", ptr);

	}

	//クリアタイムを更新する	
	void GameClearStage::ClearTime(){

		//GameScoreの1の位のシェアードゲームオブジェクト?を更新
		auto ptr_Num = GetSharedGameObject<NumberSprite>(L"ResultNum_OnePlace");
		ptr_Num->SetNum(CLEARTIME);
		//GameScoreの10の位のシェアードゲームオブジェクト?を更新
		ptr_Num = GetSharedGameObject<NumberSprite>(L"ResultNum_TenPlace");
		ptr_Num->SetNum(CLEARTIME / 10);
		//GameScoreの100の位のシェアードゲームオブジェクト?を更新
		ptr_Num = GetSharedGameObject<NumberSprite>(L"ResultNum_HandredPlace");
		ptr_Num->SetNum(CLEARTIME / 100);
	}

	void GameClearStage::RandomCounterAnimation()
	{
		RandomNumber += 111;
		CLEARTIME = RandomNumber;
		if (CLEARTIME > 999){ RandomNumber = 0; };
	}

	void GameClearStage::ClearRANK()
	{
		if (CLEARTIME < 100)
		{
			vector<Vector3>Vec =
			{
				{ 720.0f, 350.0f, 0 },
			};
			for (auto v : Vec)
			{
				AddGameObject<RANK_ASprite>(v);
			}
		}
		else if (100 < CLEARTIME < 300)
		{
			vector<Vector3>Vec =
			{
				{ 720.0f, 350.0f, 0 },
			};
			for (auto v : Vec)
			{
				AddGameObject<RANK_BSprite>(v);
			}
		}
		else if (300 < CLEARTIME)
		{
			vector<Vector3>Vec =
			{
				{ 720.0f, 350.0f, 0 },
			};
			for (auto v : Vec)
			{
				AddGameObject<RANK_CSprite>(v);
			}
		}
	}

	//--------------------------------------------------------------------------------------
	//	class GameClear_RESULT_State : public ObjState<GameClearStage>;
	//	用途: ゲームクリアRESULTステート
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<GameClear_RESULT_State> GameClear_RESULT_State::Instance(){
		static shared_ptr<GameClear_RESULT_State> instance;
		if (!instance){
			instance = shared_ptr<GameClear_RESULT_State>(new GameClear_RESULT_State);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void GameClear_RESULT_State::Enter(const shared_ptr<GameClearStage>& Obj){
	
	}
	//ステート実行中に毎ターン呼ばれる関数
	void GameClear_RESULT_State::Execute(const shared_ptr<GameClearStage>& Obj){

		Obj->SetClearTime();
		Obj->ClearTime();	

	}
	//ステートにから抜けるときに呼ばれる関数
	void GameClear_RESULT_State::Exit(const shared_ptr<GameClearStage>& Obj){
		//何もしない
	}

	//--------------------------------------------------------------------------------------
	//	class GameClear_SCENECHOICE_State : public ObjState<GameClearStage>;
	//	用途: ゲームクリア準備ステート
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<GameClear_SCENECHOICE_State> GameClear_SCENECHOICE_State::Instance(){
		static shared_ptr<GameClear_SCENECHOICE_State> instance;
		if (!instance){
			instance = shared_ptr<GameClear_SCENECHOICE_State>(new GameClear_SCENECHOICE_State);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void GameClear_SCENECHOICE_State::Enter(const shared_ptr<GameClearStage>& Obj)
	{
		Obj->CreateGoToMenu();
		Obj->CreateGoToTitle();

	}
	//ステート実行中に毎ターン呼ばれる関数
	void GameClear_SCENECHOICE_State::Execute(const shared_ptr<GameClearStage>& Obj)
	{
		Obj->ChangeMotion();
		Obj->SpriteAction();
		Obj->RefreshAction();
		if (Obj->ToTitelMotion())
		{
			Obj->GetStateMachine()->ChangeState(ToTitelState::Instance());
		
		}

	}
	//ステートにから抜けるときに呼ばれる関数
	void GameClear_SCENECHOICE_State::Exit(const shared_ptr<GameClearStage>& Obj)
	{
		//何もしない
	}

	//--------------------------------------------------------------------------------------
	//	class ToTitelState : public ObjState<GameClearStage>;
	//	用途: スタート画面に移行するステート
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<ToTitelState> ToTitelState::Instance(){
		static shared_ptr<ToTitelState> instance;
		if (!instance){
			instance = shared_ptr<ToTitelState>(new ToTitelState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void ToTitelState::Enter(const shared_ptr<GameClearStage>& Obj){
		Obj->ToChangeMotion();

	}
	//ステート実行中に毎ターン呼ばれる関数
	void ToTitelState::Execute(const shared_ptr<GameClearStage>& Obj){

	}
	//ステートにから抜けるときに呼ばれる関数
	void ToTitelState::Exit(const shared_ptr<GameClearStage>& Obj){
		//何もしない
	}

	//--------------------------------------------------------------------------------------
	//	class GameOverStage : public Stage;
	//	用途: ゲームオーバーステージクラス
	//--------------------------------------------------------------------------------------
	//リソースの作成
	void GameOverStage::CreateResourses(){
		wstring strTexture = App::GetApp()->m_wstrRelativeDataPath + L"OVER.png";
		App::GetApp()->RegisterTexture(L"OVER_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"ToMenu.png";//「メニューへ」Spriteの追加
		App::GetApp()->RegisterTexture(L"GOtoMENU_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"ToTitle.png";//「タイトルへ」Spriteの追加
		App::GetApp()->RegisterTexture(L"GOtoTITLE_TX", strTexture);

		

		wstring NanikaWav = App::GetApp()->m_wstrRelativeDataPath + L"Gameover.wav";
		App::GetApp()->RegisterWav(L"OVER", NanikaWav);
		NanikaWav = App::GetApp()->m_wstrRelativeDataPath + L"Button.wav";
		App::GetApp()->RegisterWav(L"BUTTON", NanikaWav);
		NanikaWav = App::GetApp()->m_wstrRelativeDataPath + L"cursor.wav";
		App::GetApp()->RegisterWav(L"Cursor", NanikaWav);
	}

	//ビューの作成
	void GameOverStage::CreateViews(){
		//最初にデフォルトのレンダリングターゲット類を作成する
		//パラメータの2048.0fはシャドウマップのサイズ。大きいほど影が細かくなる（4096.0fなど）
		//影を細かくするとメモリを大量に消費するので注意！
		CreateDefaultRenderTargets(2048.0f);
		//影のビューサイズの設定。この値を小さくすると影が表示される範囲が小さくなる。
		//値が小さいほうが影は細かくなる
		//スタティック変数なので一度設定したらその値はステージを超えて保持される。
		Shadowmap::SetViewSize(32.0f);
		//マルチビューコンポーネントの取得
		auto PtrMultiView = GetComponent<MultiView>();
		//マルチビューにビューの追加
		auto PtrView = PtrMultiView->AddView();
		//ビューの矩形を設定（ゲームサイズ全体）
		Rect2D<float> rect(0, 0, (float)App::GetApp()->GetGameWidth(), (float)App::GetApp()->GetGameHeight());
		//ビューの背景色
		Color4 ViewBkColor(0.0f, 0.0f, 0.0f, 1.0f);
		//最初のビューにパラメータの設定
		PtrView->ResetParamaters<LookAtCamera, MultiLight>(rect, ViewBkColor, 1, 0.0f, 1.0f);
		//0番目のビューのカメラを得る
		auto PtrCamera = GetCamera(0);
		PtrCamera->SetEye(Vector3(0.0f, 0.0f, -5.0f));
		PtrCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));
	}

	//ゲームクリアスプライトの作成
	void GameOverStage::CreateToOver(){
		vector<Vector3>Vec = {
			{ 650.0f, 500.0f, 0 },
		};
		for (auto v : Vec){
			AddGameObject<OverSprite>(v);
		}
	}

	//「メニューへ」スプライトの作成
	void GameOverStage::CreateGoToMenu()
	{
		vector<Vector3>Vec =
		{
			{ 1000.0f, 200.0f, 0 },
		};
		for (auto v : Vec){
			auto GoToMenu = AddGameObject<GoToMenuSprite>(v);
			SetSharedGameObject(L"GoToMenu", GoToMenu);
		}
	}

	//「タイトルへ」スプライトの作成
	void GameOverStage::CreateGoToTitle()
	{
		vector<Vector3>Vec =
		{
			{ 300.0f, 200.0f, 0 },
		};
		for (auto v : Vec){
			auto GoToTitle = AddGameObject<GoToTitleSprite>(v);
			SetSharedGameObject(L"GoToTitle", GoToTitle);
		}

	}

	//
	void GameOverStage::SpriteAction()
	{
		//時間でスケーリング変更するための準備
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		ActionTime += ElapsedTime;
		Vector3 Scale = m_Scale;//スプライトのスケール格納&変更用変数(丁度いい名前が思い浮かびません)

		//初期スケールを設定
		Scale.x = 1.0f;
		Scale.y = 1.0f;

		//スケール変更用の計算式？を定義
		float ScaleChanger = sin(ActionTime);

		//X方向へのスケーリング
		Scale.x += ScaleChanger;
		//Y方向へのスケーリング
		Scale.y += ScaleChanger;

		//各スケーリングが反転しないように調整
		if (Scale.x <= 1.0)
		{
			Scale.x = 1;
			Scale.y = 1;
			ActionTime = 0;
		}

		//プレイヤーが1番を選択していた時、タイトルスプライトを点滅させる
		if (Number == 1)
		{
			auto SpritePtr = GetSharedGameObject<GoToTitleSprite>(L"GoToTitle", false);
			auto PtrTransform = SpritePtr->AddComponent<Transform>();
			PtrTransform->SetScale(Scale);
		}
		else
			//プレイヤーが2番を選択していた時、タイトルスプライトを点滅させる	
			if (Number == 2)
			{
				auto SpritePtr = GetSharedGameObject<GoToMenuSprite>(L"GoToMenu", false);
				auto PtrTransform = SpritePtr->AddComponent<Transform>();
				PtrTransform->SetScale(Scale);

			}
	}
	//

	//
	void GameOverStage::RefreshAction()
	{
		if (Number == 1)
		{
			auto SpritePtr = GetSharedGameObject<GoToMenuSprite>(L"GoToMenu", false);
			auto PtrTransform = SpritePtr->AddComponent<Transform>();
			PtrTransform->SetScale(1, 1, 1);
		}

		if (Number == 2)
		{
			auto SpritePtr = GetSharedGameObject<GoToTitleSprite>(L"GoToTitle", false);
			auto PtrTransform = SpritePtr->AddComponent<Transform>();
			PtrTransform->SetScale(1, 1, 1);
		}
	}

	//初期化
	void GameOverStage::OnCreate(){
		try{
			//ステートマシンの構築
			m_StateMachine = make_shared< StateMachine<GameOverStage> >(GetThis<GameOverStage>());
			//最初のステートをDefaultStateに設定
			m_StateMachine->SetCurrentState(GameOverState::Instance());
			//DefaultStateの初期化実行を行う
			m_StateMachine->GetCurrentState()->Enter(GetThis<GameOverStage>());

			CreateResourses();
			CreateToOver();
			CreateGoToMenu();//「メニューへ」Spriteの作成
			CreateGoToTitle();//「タイトルへ」Spriteの作成
			CreateViews();
			AddMusic();

			auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
			pMultiSoundEffect->AddAudioResource(L"BUTTON");//Playerより持ってきましたSoner.SE
			pMultiSoundEffect->AddAudioResource(L"Cursor");
			

		}
		catch (...){
			throw;
		}
	}

	//操作
	void GameOverStage::OnUpdate(){

		//ステートマシンのUpdateを行う
		//この中でステートの切り替えが行われる
		m_StateMachine->Update();

	}

	//ミュージック(BGM)の再生
	void GameOverStage::AddMusic()
	{
		//ミュージックコンポーネントを登録.
		auto pMusic = AddComponent<PlayMusic>(L"OVER");
		//再生（繰り返し、0.5ボリューム）
		pMusic->Start(0, 0.9f);
	}

	void GameOverStage::ChangeMotion(){
		//コントローラ情報の取得.
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].bConnected){
			if (CntlVec[0].fThumbLX == 0.0f){
				Change = false;

			}
			if (CntlVec[0].fThumbLX >= 0.5f && !Change){

				Change = true;
				Number += 1;
				//SEの再生
				auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
				pMultiSoundEffect->Start(L"Cursor", 0, 1.0f);
			}
			if (CntlVec[0].fThumbLX <= -0.5f && !Change){

				Change = true;
				Number -= 1;
				//SEの再生
				auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
				pMultiSoundEffect->Start(L"Cursor", 0, 1.0f);
			}
		}

		if (Number <= 0){
			Number = 1;
		}
		if (Number > 2){
			Number = 2;
		}


	}

	bool GameOverStage::ToTitelMotion(){
		//コントローラ情報の取得.
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].bConnected){
			//Aボタンが押されたら.
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A) {
				ChangeNum = Number;
				return true;
			}
		}
		return false;
	}

	void GameOverStage::ToChangeMotion(){
		//ミュージックコンポーネントを登録.
		auto pMusic = AddComponent<PlayMusic>(L"OVER");
		//停止
		pMusic->Stop();

		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->Start(L"BUTTON", 0, 1.0f);


		//イベント送出
		if (ChangeNum == 1){
			PostEvent(1.0f, GetThis<GameOverStage>(), App::GetApp()->GetSceneBase(), L"ToTitle");
		}
		if (ChangeNum == 2){
			PostEvent(1.0f, GetThis<GameOverStage>(), App::GetApp()->GetSceneBase(), L"ToMenu");
		}
	}

	//--------------------------------------------------------------------------------------
	//	class GameOverState : public ObjState<GameOverStage>;
	//	用途: ゲームオーバーステート
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<GameOverState> GameOverState::Instance(){
		static shared_ptr<GameOverState> instance;
		if (!instance){
			instance = shared_ptr<GameOverState>(new GameOverState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void GameOverState::Enter(const shared_ptr<GameOverStage>& Obj){
	}
	//ステート実行中に毎ターン呼ばれる関数
	void GameOverState::Execute(const shared_ptr<GameOverStage>& Obj){

		Obj->ChangeMotion();
		Obj->SpriteAction();
		Obj->RefreshAction();
		if (Obj->ToTitelMotion()){

			Obj->GetStateMachine()->ChangeState(ChangeTitelState::Instance());

		}

	}
	//ステートにから抜けるときに呼ばれる関数
	void GameOverState::Exit(const shared_ptr<GameOverStage>& Obj){
		//何もしない
	}

	//--------------------------------------------------------------------------------------
	//	class ChangeTitelState : public ObjState<GameOverStage>;
	//	用途: スタート画面に移行するステート
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<ChangeTitelState> ChangeTitelState::Instance(){
		static shared_ptr<ChangeTitelState> instance;
		if (!instance){
			instance = shared_ptr<ChangeTitelState>(new ChangeTitelState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void ChangeTitelState::Enter(const shared_ptr<GameOverStage>& Obj){
		Obj->ToChangeMotion();

	}
	//ステート実行中に毎ターン呼ばれる関数
	void ChangeTitelState::Execute(const shared_ptr<GameOverStage>& Obj){

	}
	//ステートにから抜けるときに呼ばれる関数
	void ChangeTitelState::Exit(const shared_ptr<GameOverStage>& Obj){
		//何もしない
	}


	//--------------------------------------------------------------------------------------
	//	class GameStage : public Stage;
	//	用途: ゲームステージクラス
	//--------------------------------------------------------------------------------------
	//リソースの作成

	void GameStage::CreateResourses(){
		wstring	strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Player.png";
		App::GetApp()->RegisterTexture(L"PLAYER_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Plate.png";
		App::GetApp()->RegisterTexture(L"PLATE_TX", strTexture); 
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Wave.png";
		App::GetApp()->RegisterTexture(L"WAVE_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"life.png";
		App::GetApp()->RegisterTexture(L"HEART_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Enemy.png";
		App::GetApp()->RegisterTexture(L"ENEMY_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"SoundWave.png";
		App::GetApp()->RegisterTexture(L"SoundWave_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"effect.png";
		App::GetApp()->RegisterTexture(L"EFECT_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"spark.png";
		App::GetApp()->RegisterTexture(L"SPARK_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"number.png";//0~9の数字Texture
		App::GetApp()->RegisterTexture(L"NUMBERs_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"EnemyBoss.png";
		App::GetApp()->RegisterTexture(L"ENEMYBOSS_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Alert.png";
		App::GetApp()->RegisterTexture(L"ALERT_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"PowerUp.png";
		App::GetApp()->RegisterTexture(L"PowerUp_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Recovery.png";
		App::GetApp()->RegisterTexture(L"Recovery_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Danger.png";
		App::GetApp()->RegisterTexture(L"DANGER_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Lightning.png";
		App::GetApp()->RegisterTexture(L"LIGHTNING_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"EnemyUneven.png";//エネミー用の凸凹床
		App::GetApp()->RegisterTexture(L"ENEMYUNVEN_TX", strTexture);
		//strTexture = App::GetApp()->m_wstrRelativeDataPath + L"HeelEffect.png";//回復時に表示されるパーティクル
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"HeelEffect_a.png";
		App::GetApp()->RegisterTexture(L"HeelEffect_TX", strTexture);
		//strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Damage_A.png";//敵撃破時に弾けるパーティクルEffect
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Damage_Ca.png";//敵撃破時に弾けるパーティクルEffect(太)
		App::GetApp()->RegisterTexture(L"Dameage_Enemy_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"TIME.png";//「TIME」Spriteの追加
		App::GetApp()->RegisterTexture(L"TIME_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"heart_a.png";//アイテムがまとう「Heart」Spriteの追加
		App::GetApp()->RegisterTexture(L"heart_a_TX", strTexture); 
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"enemyui.png";//アイテムがまとう「Heart」Spriteの追加
		App::GetApp()->RegisterTexture(L"ENEMYUI_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"bar.png";//PlayerのHPのBarSpriteの追加
		App::GetApp()->RegisterTexture(L"BAR_MAX_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"bar2.png";//PlayerのHPのBarSpriteの追加
		App::GetApp()->RegisterTexture(L"BAR_MID_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"bar3.png";//PlayerのHPのBarSpriteの追加
		App::GetApp()->RegisterTexture(L"BAR_LAST_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"ReTuto.png";
		App::GetApp()->RegisterTexture(L"RETUTO_TX", strTexture);	//ゲーム画面に表示される説明用sprite
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Effect_Square_B.png";//肉抜きされたSquare(青)Spriteの追加
		App::GetApp()->RegisterTexture(L"Square_B_TX", strTexture);

		
		

		//サウンドリソース
		wstring NanikaWav = App::GetApp()->m_wstrRelativeDataPath + L"Game.wav";
		App::GetApp()->RegisterWav(L"Game", NanikaWav);
		NanikaWav = App::GetApp()->m_wstrRelativeDataPath + L"Boss.wav";
		App::GetApp()->RegisterWav(L"Boss", NanikaWav);
		NanikaWav = App::GetApp()->m_wstrRelativeDataPath + L"Dameg.wav";
		App::GetApp()->RegisterWav(L"Dameg", NanikaWav);
		NanikaWav = App::GetApp()->m_wstrRelativeDataPath + L"Soner.wav";
		App::GetApp()->RegisterWav(L"Soner", NanikaWav);
		NanikaWav = App::GetApp()->m_wstrRelativeDataPath + L"Recovery.wav";
		App::GetApp()->RegisterWav(L"Recovery", NanikaWav);
		NanikaWav = App::GetApp()->m_wstrRelativeDataPath + L"button65.wav";
		App::GetApp()->RegisterWav(L"BUTTON", NanikaWav);
		NanikaWav = App::GetApp()->m_wstrRelativeDataPath + L"PowerUp.wav";
		App::GetApp()->RegisterWav(L"POWERUP", NanikaWav);
		NanikaWav = App::GetApp()->m_wstrRelativeDataPath + L"Lightning.wav";
		App::GetApp()->RegisterWav(L"Lightning", NanikaWav);

	}

	//ビュー類の作成
	void GameStage::CreateViews(){
		//最初にデフォルトのレンダリングターゲット類を作成する
		//パラメータの2048.0fはシャドウマップのサイズ。大きいほど影が細かくなる（4096.0fなど）
		//影を細かくするとメモリを大量に消費するので注意！
		CreateDefaultRenderTargets(2048.0f);
		//影のビューサイズの設定。この値を小さくすると影が表示される範囲が小さくなる。
		//値が小さいほうが影は細かくなる
		//スタティック変数なので一度設定したらその値はステージを超えて保持される。
		Shadowmap::SetViewSize(32.0f);
		//マルチビューコンポーネントの取得
		auto PtrMultiView = GetComponent<MultiView>();
		//マルチビューにビューの追加
		auto PtrView = PtrMultiView->AddView();
		//ビューの矩形を設定（ゲームサイズ全体）
		Rect2D<float> rect(0, 0, (float)App::GetApp()->GetGameWidth(), (float)App::GetApp()->GetGameHeight());
		//ビューの背景色
		Color4 ViewBkColor(0.0f, 0.0f, 0.0f, 1.0f);
		//最初のビューにパラメータの設定
		PtrView->ResetParamaters<MyCamera, MultiLight>(rect, ViewBkColor, 1, 0.0f, 1.0f);
		//最初のビューのビューのライトの設定
		auto PtrLight = PtrView->GetMultiLight()->GetLight(0);
		PtrLight->SetPositionToDirectional(-0.25f, 1.0f, -0.25f);
		//ビューのカメラの設定
		auto PtrCamera = PtrView->GetCamera();
		PtrCamera->SetEye(Vector3(0.0f, 2.0f, -4.0f));
		PtrCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));
	}



	//CSVファイルを使ってゲームステージの作成
	void GameStage::CreateRoad(){
		////Csvファイルからゲームステージの選択
		wstring MediaPath = App::GetApp()->m_wstrRelativeDataPath + L"GameStage\\";
		//シーンからゲームセレクトで選んだゲームステージの番号取得
		int i_StageNumber = App::GetApp()->GetScene<Scene>()->GetStageNumber();
		//ステージ番号からステージの名前を取得
		wstring i_StageName = Util::IntToWStr(i_StageNumber);
		wstring Filename = MediaPath + L"Stage_";

		Filename += i_StageName + L".csv";

		//ローカル上にCSVファイルクラスの作成
		CsvFile GameStageCsv(Filename);
		if (!GameStageCsv.ReadCsv()){
			//ファイルが存在しなかった
			//初期化失敗
			throw BaseException(
				L"CSVファイルがありません。",
				Filename,
				L"選択された場所にはアクセスできません"
				);
		}


		const int iDataSizeRow = 0;		//データが0行目から始まるようの定数
		vector< wstring > StageMapVec;	//ワーク用のベクター配列
		//iDataSizeRowのデータを抜き取りベクター配列に格納
		GameStageCsv.GetRowVec(iDataSizeRow, StageMapVec);
		////行、列の取得
		m_sRowSize = static_cast<size_t>(_wtoi(StageMapVec[0].c_str())); //Row  = 行
		m_sColSize = static_cast<size_t>(_wtoi(StageMapVec[1].c_str())); //Col　= 列

		//マップデータ配列作成
		m_MapDataVec = vector< vector<size_t> >(m_sRowSize, vector<size_t>(m_sColSize));	//列の数と行の数分作成

		//配列の初期化
		for (size_t i = 0; i < m_sRowSize; i++){
			for (size_t j = 0; j < m_sColSize; j++)
			{
				m_MapDataVec[i][j] = 0;								//size_t型のvector配列を０で初期化			
			}
		}
		//1行目からステージが定義されている
		const int iDataStartRow = 1;
		//assert(m_sRowSize > 0 && "行が０以下なのでゲームが開始できません");
		if (m_sRowSize > 0){
			for (size_t i = 0; i < m_sRowSize; i++){
				GameStageCsv.GetRowVec(i + iDataStartRow, StageMapVec);		//スタート + i　だから最初は 2が入る
				for (size_t j = 0; j < m_sColSize; j++){					//列分ループ処理
					const int iNum = _wtoi(StageMapVec[j].c_str());			//列から取得したwstring型をintに変換→格納	
					//マップデータ配列に格納
					m_MapDataVec[i][j] = iNum;
					//配置されるオブジェクトの基準スケール
					float ObjectScale = 1.0f;
					float boxscale = 2.0f;
					float CharScale = 0.5f;
					//基準Scale
					Vector3 Scale(boxscale, ObjectScale, ObjectScale);
					Vector3 BoxScale(boxscale, ObjectScale, ObjectScale);
					Vector3 CharSc(CharScale, CharScale, CharScale);

					//基準Position
					Vector3 Pos(static_cast<float>(j), 0.5, static_cast<float>(i));
					Vector3 BossPos(static_cast<float>(j), 10, static_cast<float>(i));
					Vector3 WallPos(static_cast<float>(j), 0.3, static_cast<float>(i));
					Vector3 BoxPos(static_cast<float>(j), -0.4, static_cast<float>(i));
					switch (iNum){
					case DataID::BOX:				//	Csv	:	1　壁
						AddGameObject<FixedBox>(Scale, Vector3(0, 0, 0), Pos);
						break;
					case DataID::ENEMY:				//	Csv	:	2　敵
						AddGameObject<Enemy>(CharSc, Vector3(0, 0, 0), Pos);
						break;
					case DataID::HEAL:				//	Csv	:	3　HP回復
						AddGameObject<ItemTest>(CharSc, Vector3(0, 0, 0), Pos);
						break;
					case DataID::POWERUP:			//	Csv	:	4　強化アイテム
						AddGameObject<Item_PowerUP>(CharSc, Vector3(0, 0, 0), Pos);
						break;
					case DataID::PLAYER:			//	Csv	:	5　プレイヤー
						//シェア配列にプレイヤーを追加
						SetSharedGameObject(L"Player", AddGameObject<Player>(Pos));
						break;
					case DataID::ROAD:				//	Csv	:	6　床
						AddGameObject<FixedBox>(BoxScale, Vector3(0, XM_PIDIV2, 0), Pos);
						break;
					case DataID::ENEMYBOSS:	//Csv	:	7	エネミーボス
						//シェア配列にプレイヤーを追加
						SetSharedGameObject(L"EnemyBoss", AddGameObject<EnemyBoss>(CharSc, Vector3(0, 0, 0), BossPos));
						break;
					case DataID::SPACE:
						break;
					default:
						assert(!"不正な値が入ってます。Csvマップの可能性が高いです");
						break;
					}

				}

			}
			////描画マネージャの作成
			auto WallPtr = AddGameObject<DrawBoxManager>(1000, L"DEFAULT_SQUARE", L"DANGER_TX");
			//シェアオブジェクトにマネージャーを追加
			SetSharedGameObject(L"DrawBoxManager", WallPtr);

		}
	}

	//アイテムマネージャーのシェアードptrを作成
	void GameStage::CreateItemManager()
		{
			auto Ptr = AddGameObject<ItemManager>();
			SetSharedGameObject(L"ItemManager", Ptr);
		}
	//エネミーマネージャーのシェアードptrを作成
	void GameStage::CreateEnemyManager()
		{
			auto Ptr = AddGameObject<EnemyManager>();
			SetSharedGameObject(L"EnemyManager", Ptr);
		}


	void GameStage::OnUpdate(){

		auto PtrPlayer = GetSharedGameObject<Player>(L"Player");
		auto PtrBoss = GetSharedGameObject<EnemyBoss>(L"EnemyBoss");
		auto PtrCamera = dynamic_pointer_cast<MyCamera>(GetCamera(0));
		//前回のターンからの経過時間を取得(1秒ごとカウントされる？)
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		if (PtrCamera){

			PtrCamera->SetTargetObject(PtrPlayer);
		//BOSS死亡フラグを見てtrueならカメラをBOSSに向ける
			if (PtrBoss->m_EnemyBossFlg){
				PtrCamera->SetToTargetLerp(0.2f);
				PtrCamera->SetTargetObject(PtrBoss);
			}
			//BOSS生成フラグがtrueならカメラを一定時間BOSSに向ける
			if (PtrBoss->m_EnemyDraw && !m_Changflag){
				CameChang += ElapsedTime;
				if (CameChang > 1.0f){
					PtrCamera->SetToTargetLerp(0.2f);
					PtrCamera->SetTargetObject(PtrBoss);
					SetGame(false);
				}
				if (CameChang > 3.0f){
					PtrCamera->SetToTargetLerp(1.0f);
					m_Changflag = true;
					SetGame(true);
					CameChang = 0.0f;
				}
			}
		}
	}


	void GameStage::OnLastUpdate(){

		//ステートの更新
		m_StateMachine->Update();

		auto ptr_Player = GetSharedGameObject<Player>(L"Player");
		auto ptr_Alret = GetSharedGameObject<AlretSprite>(L"Alret");
		//プレイヤーのHPが１ならアラートスプライトの表示を行う
		if (ptr_Player->PlayerHP == 1){
			if (ptr_Alret){

				ptr_Alret->CState();
			}
		}
		else
		{
			ptr_Alret->RState();
		}

		//ゲームタイマーを更新用の変数へ格納
		float TimeCounter = GameTimer;
		//エネミーカウンターを更新用の変数へ格納
		float EnemyCounter = ENEMYCOUNTER;
		

		//TimerSpriteの1の位のシェアードゲームオブジェクト?を更新
		auto ptr_Num = GetSharedGameObject<NumberSprite>(L"TimeNum_OnePlace");
		ptr_Num->SetNum(TimeCounter);
		//TimerSpriteの10の位のシェアードゲームオブジェクト?を更新
		ptr_Num = GetSharedGameObject<NumberSprite>(L"TimeNum_TenPlace");
		ptr_Num->SetNum(TimeCounter / 10);
		//TimerSpriteの100の位のシェアードゲームオブジェクト?を更新
		ptr_Num = GetSharedGameObject<NumberSprite>(L"TimeNum_HandredPlace");
		ptr_Num->SetNum(TimeCounter / 100);
		
		//エネミーカウンターの更新
		//エネミーカウンターの1の位のシェアードゲームオブジェクト?を更新
		ptr_Num = GetSharedGameObject<NumberSprite>(L"EnemyNum_OnePlace");
		ptr_Num->SetNum(EnemyCounter);
		//エネミーカウンターの10の位のシェアードゲームオブジェクト?を更新
		ptr_Num = GetSharedGameObject<NumberSprite>(L"EnemyNum_TenPlace");
		ptr_Num->SetNum(EnemyCounter / 10);

		}

	//ゲームオブジェクトのUpDateをカメラ移動中変更する
	void GameStage::SetGame(bool game){
	
		auto PtrPlayer = GetSharedGameObject<Player>(L"Player");
		auto PtrEnemy = GetSharedObjectGroup(L"EnemyGroup");
		auto PtrEvec = PtrEnemy->GetGroupVector();
		auto PtrBoss = GetSharedGameObject<EnemyBoss>(L"EnemyBoss");
	
	
		PtrPlayer->SetUpdateActive(game);
		PtrBoss->SetUpdateActive(game);
		for (auto Ptr : PtrEvec){
		
			auto enemy = dynamic_pointer_cast<Enemy>(Ptr.lock());
			enemy->SetUpdateActive(game);

		}
	}
	//「TIME」スプライトの作成
	void GameStage::CreateTim()
	{
			AddGameObject<TIMESprite>(Vector3(490.0f, 750.0f, 0), Vector3(0.8f, 0.5f, 1.0f));
	}
	//敵残機UIの作成
	void GameStage::CreateEnemyUI(){
	
		vector<Vector3>Vec = {
			{ 1100.0f, 750.0f, 0 },
		};
		for (auto v : Vec){
			AddGameObject<EnemyUISprite>(v);
		}
	
	}

	//アラートUIの作成
	void GameStage::CreateAlret(){
		vector<Vector3>Vec = {
			{ 640.0f, 3.0f, 0 },
		};
		for (auto v : Vec)
		{
			auto ptr = AddGameObject<AlretSprite>(v);
			SetSharedGameObject(L"Alret", ptr);
		}
	}

	//UIの作成
	void GameStage::CreateReTuto()
	{
		vector<Vector3>Vec = {
			{ 640.0f, 3.0f, 0 },
		};
		for (auto v : Vec)
		{
			auto ptr = AddGameObject<ReTuroSprite>(v);
			SetSharedGameObject(L"ReTuto", ptr);
		}
	}

	//でこぼこ床
	void GameStage::CreateUnevenGround(){
		//波が立つ水面メッシュ
		auto Ptr = AddGameObject<UnevenGround>(40, 40, 100);
		SetSharedGameObject(L"Uneven", Ptr);
	}
	//スパークの作成
	void GameStage::CreateSpark(){
		auto MultiSparkPtr = AddGameObject<MultiSpark>();
		//シェア配列にスパークを追加
		SetSharedGameObject(L"MultiSpark", MultiSparkPtr);
		//エフェクトはZバッファを使用する
		GetParticleManager()->SetZBufferUse(true);
	}

	void GameStage::CreateEfect(){
		auto MultiSparkPtr = AddGameObject<MultiEfect>();
		//シェア配列にスパークを追加
		SetSharedGameObject(L"MultiEfect", MultiSparkPtr);
		//エフェクトはZバッファを使用する
		GetParticleManager()->SetZBufferUse(true);

	
	}

	//エフェクトの作成
	void GameStage::CreateParticle(){
		auto MultiSparkPtr = AddGameObject<MultiOrb>();
		//シェア配列にスパークを追加
		SetSharedGameObject(L"MultiOrb", MultiSparkPtr);
		//エフェクトはZバッファを使用する
		GetParticleManager()->SetZBufferUse(true);
	}
	//エフェクトの作成
	void GameStage::CreateCross(){
		auto MultiSparkPtr = AddGameObject<MultiCross>();

		//シェア配列にスパークを追加
		SetSharedGameObject(L"MultiCross", MultiSparkPtr);
		//エフェクトはZバッファを使用する
		GetParticleManager()->SetZBufferUse(true);

	}

	//PowerUpEffectを作成
	void GameStage::CreatePowerUpEffect()
	{
		auto PowerUpEffectPtr = AddGameObject<PowerUpEffect>();

		//シェア配列にスパークを追加
		SetSharedGameObject(L"PowerUpEffect", PowerUpEffectPtr);
		//エフェクトはZバッファを使用する
		GetParticleManager()->SetZBufferUse(true);
	}

	//HeelEffectを作成
	void GameStage::CreateHeelEffect()
	{
		auto HeelEffectPtr = AddGameObject<HeelEffect>();

		//シェア配列にスパークを追加
		SetSharedGameObject(L"HeelEffect", HeelEffectPtr);
		//エフェクトはZバッファを使用する
		GetParticleManager()->SetZBufferUse(true);
	}

	//エネミー撃破時まき散らすEffectの作成
	void GameStage::CreateJrSpark()
	{
		auto MultiSparkPtr = AddGameObject<MultiSpark_jr>();
		//シェア配列にスパークを追加
		SetSharedGameObject(L"MultiSpark_jr", MultiSparkPtr);
		//エフェクトはZバッファを使用する
		GetParticleManager()->SetZBufferUse(true);
	}

	//アイテムがまとうHeartのパーティクルの作成
	void GameStage::CreateHeartSpark()
	{
		auto MultiSparkPtr = AddGameObject<MultiSpark_Heart>();
		//シェア配列にスパークを追加
		SetSharedGameObject(L"MultiSpark_Heart", MultiSparkPtr);
		//エフェクトはZバッファを使用する
		GetParticleManager()->SetZBufferUse(true);
	}

	//アイテムがまとうCircleのパーティクルの作成
	void GameStage::CreateCircleSpark()
	{
		auto MultiSparkPtr = AddGameObject<MultiSpark_Circle>();
		//シェア配列にスパークを追加
		SetSharedGameObject(L"MultiSpark_Circle", MultiSparkPtr);
		//エフェクトはZバッファを使用する
		GetParticleManager()->SetZBufferUse(true);

	}

	//Playerがダメージを喰らったときのパーティクルの作成
		void GameStage::CreatePlayer_Dameage()
	{
		auto MultiSparkPtr = AddGameObject<Player_DamageEffect>();
		//シェア配列にスパークを追加
		SetSharedGameObject(L"Player_DamageEffect", MultiSparkPtr);
		//エフェクトはZバッファを使用する
		GetParticleManager()->SetZBufferUse(true);

	}

	//BOSSがダメージを喰らったときのパーティクルの作成
		void GameStage::CreateBOSS_Enter()
	{
		auto MultiSparkPtr = AddGameObject<MultiSpark_Square>();
		//シェア配列にスパークを追加
		SetSharedGameObject(L"MultiSpark_Square", MultiSparkPtr);
		//エフェクトはZバッファを使用する
		GetParticleManager()->SetZBufferUse(true);

	}

	//BOSSがダメージを喰らったときのパーティクルの作成
	void GameStage::CreateBOSS_Dameage()
	{
		auto MultiSparkPtr = AddGameObject<BOSS_DamageEffect>();
		//シェア配列にスパークを追加
		SetSharedGameObject(L"BOSS_DamageEffect", MultiSparkPtr);
		//エフェクトはZバッファを使用する
		GetParticleManager()->SetZBufferUse(true);

	}

	//アイテムがまとうHeartのパーティクルの作成
	void GameStage::CreateSTANSpark()
	{
		auto MultiSparkPtr = AddGameObject<MultiSpark_STAN>();
		//シェア配列にスパークを追加
		SetSharedGameObject(L"MultiSpark_STAN", MultiSparkPtr);
		//エフェクトはZバッファを使用する
		GetParticleManager()->SetZBufferUse(true);
	}
	

	//ゲームタイマーを初期化
	//ゲームタイマーの初期値を入れるように変更(2016/05/17)
	void GameStage::ReFreshGameTimer()
	{

		GameTimer = 0.0f;
	}
	//カウントダウンするゲームタイマーの処理
	void GameStage::CountUpGameTimer()
	{
		//前回のターンからの経過時間を取得(1秒ごとカウントされる？)
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		//ゲームタイマーから減算
		GameTimer += ElapsedTime;
	}
	//タイムオーバーの処理
	//
	void GameStage::TimeOver()
	{
		//Timerが0以下になったらステートをゲームオーバーに切り替えてシーンchange
		if (GameTimer >= 999)
		{
			PostEvent(1.0f, GetThis<GameStage>(), App::GetApp()->GetSceneBase(), L"ToOver");
		}
	}
	//
	//ゲームタイマーの実装
	//
	void GameStage::CreateTimer()
	{
		//TimerSpriteの1の位のシェアードゲームオブジェクト?を作成
		auto ptr = AddGameObject<NumberSprite>(Vector3(660, 53, 0), Vector3(1, 1, 1));
		SetSharedGameObject(L"TimeNum_OnePlace", ptr);
		//TimerSpriteの10の位のシェアードゲームオブジェクト?を作成
		ptr = AddGameObject<NumberSprite>(Vector3(620, 53, 0), Vector3(1, 1, 1));
		SetSharedGameObject(L"TimeNum_TenPlace", ptr);
		//TimerSpriteの100の位のシェアードゲームオブジェクト?を作成
		ptr = AddGameObject<NumberSprite>(Vector3(580, 53, 0), Vector3(1, 1, 1));
		SetSharedGameObject(L"TimeNum_HandredPlace", ptr);

	}


	//
	//エネミーカウンターの初期化(初期値設定)
	void GameStage::SetEnemyCounter()
	{
		//シーンからゲームセレクトで選んだゲームステージの番号取得
		int i_StageNumber = App::GetApp()->GetScene<Scene>()->GetStageNumber();

		int StageNumber = i_StageNumber;

		switch (StageNumber)
		{
		case 1:
		{
			//ステージ1の時
			//ENEMYCOUNTER = 5;
			ENEMYCOUNTER = 5;//TEST
			break;
		}
		case 2:
		{
			//ステージ2の時
			ENEMYCOUNTER =	10;
			break;
		}
		case 3:
		{
			//ステージ3の時
			ENEMYCOUNTER = 20;
			break;
		}
		case 4:
		{
			//ステージ4の時
			ENEMYCOUNTER = 40;
			break;
		}

		}
	}

	//エネミーの数更新
	void GameStage::CounterDownEnemy()
	{
		ENEMYCOUNTER--;
		if (ENEMYCOUNTER < 0){
			ENEMYCOUNTER = 0;
		}
	}

	//エネミーカウンターの実装
	void GameStage::CreateEnemyCounter()
	{
		
		//エネミーカウンターの1の位のシェアードゲームオブジェクト?を作成
		auto ptr = AddGameObject<NumberSprite>(Vector3(1220, 53, 0), Vector3(1, 1, 1));
		SetSharedGameObject(L"EnemyNum_OnePlace", ptr);
		//エネミーカウンターの10の位のシェアードゲームオブジェクト?を作成
		ptr = AddGameObject<NumberSprite>(Vector3(1180, 53, 0), Vector3(1, 1, 1));
		SetSharedGameObject(L"EnemyNum_TenPlace", ptr);
		
	}

	//Timeを加算(Sceneへ格納)
	void GameStage::AddScore()
	{
		//追加
		float TIMER = GameTimer;//ゲームタイマーを引数用変数へ格納
		App::GetApp()->GetScene<Scene>()->SetSCORE(TIMER);//Sceneへシュート!!!
		//追加ここまで
	};

	//MotionManagerのシェアードptrを作成
	void GameStage::CreateMotionManager()
	{
		auto Ptr = AddGameObject<MotionManager>();
		SetSharedGameObject(L"MotionManager", Ptr);
	}

	//
	//HPバースプライトの作成とシェアードゲームオブジェクト化？
	void GameStage::CreateHPBar()
	{
		vector<Vector3>Vec =
		{
			{ 110.0f, 750.0f, 0 },
		};
		for (auto v : Vec){
			auto HPMAXBAR = AddGameObject<HPBAR_MAX>(v);
			SetSharedGameObject(L"HPBAR_MAX", HPMAXBAR);
		}
		Vec =
		{
			{ 110.0f, 750.0f, 0 },
		};
		for (auto v : Vec){
			auto HPMIDBAR = AddGameObject<HPBAR_MID>(v);
			SetSharedGameObject(L"HPBAR_MID", HPMIDBAR);
		}
		Vec =
		{
			{ 110.0f, 750.0f, 0 },
		};
		for (auto v : Vec){
			auto HPLASTBAR = AddGameObject<HPBAR_LAST>(v);
			SetSharedGameObject(L"HPBAR_LAST", HPLASTBAR);
		}
	}

	void GameStage::ControlHPBar()
	{
		auto ptr_Player = GetSharedGameObject<Player>(L"Player");
		Player_HP=ptr_Player->PlayerHP;
		switch (Player_HP)
		{
			case 3:
			{
				auto HPBar = GetSharedGameObject<HPBAR_MAX>(L"HPBAR_MAX");
				HPBar->SetDrawActive(true);
				auto HPBar_B = GetSharedGameObject<HPBAR_MID>(L"HPBAR_MID");
				HPBar_B->SetDrawActive(false);
				auto HPBar_C = GetSharedGameObject<HPBAR_LAST>(L"HPBAR_LAST");
				HPBar_C->SetDrawActive(false);
				HP_Animation_flag();
				break;
			}
			case 2:
			{
				auto HPBar = GetSharedGameObject<HPBAR_MAX>(L"HPBAR_MAX");
				HPBar->SetDrawActive(false);
				auto HPBar_B = GetSharedGameObject<HPBAR_MID>(L"HPBAR_MID");
				HPBar_B->SetDrawActive(true);
				auto HPBar_C = GetSharedGameObject<HPBAR_LAST>(L"HPBAR_LAST");
				HPBar_C->SetDrawActive(false);
				MAXtoMID();
				break;
			}
			case 1:
			{
				auto HPBar = GetSharedGameObject<HPBAR_MAX>(L"HPBAR_MAX");
				HPBar->SetDrawActive(false);
				auto HPBar_B = GetSharedGameObject<HPBAR_MID>(L"HPBAR_MID");
				HPBar_B->SetDrawActive(false);
				auto HPBar_C = GetSharedGameObject<HPBAR_LAST>(L"HPBAR_LAST");
				HPBar_C->SetDrawActive(true);
				MIDtoLAST();
				break;
			}
		}
		
	}

	//HPがMAXからMIDへ移行時に流すアニメーション
	void GameStage::MAXtoMID()
	{
		if (FLAG_TIMER_A < 60)
		{
			HP_Animation_flag();
			HPActionTimer++;
			auto HPBar = GetSharedGameObject<HPBAR_MAX>(L"HPBAR_MAX");
			auto HPBar_B = GetSharedGameObject<HPBAR_MID>(L"HPBAR_MID");

			if (HPActionTimer < 10)
			{
				HPBar->SetDrawActive(false);
				HPBar_B->SetDrawActive(true);
			}
			else
			{
				HPBar->SetDrawActive(true);
				HPBar_B->SetDrawActive(false);
			}
			if (HPActionTimer > 15)
			{
				HPActionTimer = 0;
			}
		}
	}

	//HPがMAXからMIDへ移行時に流すアニメーション
	void GameStage::MIDtoLAST()
	{
		if (FLAG_TIMER_B < 60)
		{
			HP_Animation_flag();
			HPActionTimer++;
			auto HPBar_B = GetSharedGameObject<HPBAR_MID>(L"HPBAR_MID");
			auto HPBar_C = GetSharedGameObject<HPBAR_LAST>(L"HPBAR_LAST");
			if (HPActionTimer < 10)
			{
				HPBar_B->SetDrawActive(false);
				HPBar_C->SetDrawActive(true);
			}
			else
			{
				HPBar_B->SetDrawActive(true);
				HPBar_C->SetDrawActive(false);
			}
			if (HPActionTimer > 15)
			{
				HPActionTimer = 0;
			}
		}
	}

	void GameStage::HP_Animation_flag()
	{
		auto ptr_Player = GetSharedGameObject<Player>(L"Player");
		Player_HP = ptr_Player->PlayerHP;
		if (Player_HP == 3)
		{
			FLAG_TIMER_A = 0;
			FLAG_TIMER_B = 0;
			HPActionTimer = 0;
		}
		if (Player_HP ==2)
		{
			FLAG_TIMER_A++;
			FLAG_TIMER_B = 0;
		}
		if (Player_HP == 1)
		{
			FLAG_TIMER_B++;
			FLAG_TIMER_A=0;
		}
		
	}
	//
	//tuikaここまで

	//初期化
	void GameStage::OnCreate(){
		try{
			//リソースの作成
			CreateResourses();
			//ビュー類を作成する
			CreateViews();
			//床シェアオブジェクトグループを作成
			CreateSharedObjectGroup(L"FIXED_BOX");
			//波のシェアオブジェクトグループの作成
			CreateSharedObjectGroup(L"Wave");

			//敵の波のシェアオブジェクトグループ作成
			CreateSharedObjectGroup(L"Wave_Enemy");

			//Itemシェアドオブジェクトグループの作成
			CreateSharedObjectGroup(L"ItemGroup");
			//凸凹床のシェアオブジェクトグループの作成
			CreateSharedObjectGroup(L"UnevenGroundGroup");
			
			//[Enemy]の作成
			CreateSharedObjectGroup(L"EnemyGroup");
			
			//CSVの作成
			CreateRoad();
			//アイテムマネージャーのsharedPtrを作成
			CreateItemManager();

			//敵残機UIの作成
			CreateEnemyUI();
			//アラートスプライトの作成
			CreateAlret();
			//『ReTutoSprite』の作成
			CreateReTuto();
			//凸凹床を作成
			CreateUnevenGround();
			CreateTim();

			//スパーク(火花)を作成
			CreateSpark();
			CreateParticle();
			CreateEfect();
			CreateCross();

			//PowerUp状態エフェクト(敵撃墜エフェクトになりそう)
			CreatePowerUpEffect();
			//HeelEffect(回復時に表示されるパーティクル)の作成
			CreateHeelEffect();
			//敵撃破時に弾けるエフェクトの作成
			CreateJrSpark();
			//回復アイテムが纏うEffectの作成
			CreateHeartSpark();
			//強化アイテムが纏うEffectの作成
			CreateCircleSpark();

			//
			CreatePlayer_Dameage();
			//BOSS登場時のEffectの作成
			CreateBOSS_Enter();
			////BOSS被弾時のEffectの作成
			CreateBOSS_Dameage();
			//BOSS麻痺時のEffectの作成
			CreateSTANSpark();

			//エネミーManager(エネミーカウンター)の実装
			CreateEnemyManager();
			//エネミーカウンターの作成
			CreateEnemyCounter();
			//Timeを加算する
			AddScore();

			//tuika
			//HPバーの作成
			CreateHPBar();
			//tuikaここまで

			//行動管理関数群へのシェアードptrを作成
			CreateMotionManager();


			//ステートマシンの構築
			m_StateMachine = make_shared< StateMachine<GameStage> >(GetThis<GameStage>());
			//最初のステートをGamePlayState(ゲーム遊んでますよ状態)に設定
			m_StateMachine->ChangeState(GamePlayState::Instance());
			//Timerの作成
			CreateTimer();


		}
		catch (...){
			throw;
		}
	}

	//--------------------------------------------------------------------------------------
	//	class GamePlayState : public ObjState<GameStage>;
	//	用途: ゲーム中ステート
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<GamePlayState> GamePlayState::Instance()
	{
		static shared_ptr<GamePlayState> instance;
		if (!instance){
			instance = shared_ptr<GamePlayState>(new GamePlayState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void GamePlayState::Enter(const shared_ptr<GameStage>& Obj)
	{
		Obj->ReFreshGameTimer();
		Obj->SetEnemyCounter();
	}
	//ステート実行中に毎ターン呼ばれる関数
	void GamePlayState::Execute(const shared_ptr<GameStage>& Obj)
	{
		Obj->CountUpGameTimer();
		Obj->TimeOver();
		Obj->AddScore();//追加
		//tuika
		Obj->ControlHPBar();
		//tuikaここまで
	}
	//ステートにから抜けるときに呼ばれる関数
	void GamePlayState::Exit(const shared_ptr<GameStage>& Obj)
	{
		//何もしない
	}


}
//endof  basedx11
