#pragma once
#include "stdafx.h"

namespace basedx11{

	//--------------------------------------------------------------------------------------
	//	class Scene : public SceneBase;
	//	用途: シーンクラス
	//--------------------------------------------------------------------------------------
	class Scene : public SceneBase{
	
		//ステージの番号を格納
		int m_Stagenumber;
		//スコア(タイム)を格納
		float m_SCORE;
		//素材提供画面を出すか？
		bool m_ThxFlag;

	public:
		//構築と破棄
		Scene():
			m_Stagenumber(0), m_SCORE(0), m_ThxFlag(0)
		{
			ShowCursor(false);
		}
		~Scene(){}
		//アクセサ
		//操作
		virtual void OnCreate()override;
		virtual void OnEvent(const shared_ptr<Event>& event);

		//ステージ番号を取得
		const int GetStageNumber()const {
			return m_Stagenumber;
		}

		//ステージ番号を設定
		void SetStageNumber(int num) {
			m_Stagenumber = num;
		}

		//スコアを取得
		const float GetSCORE()const
		{
			return m_SCORE;
		}

		//スコアを設定
		void SetSCORE(float num)
		{
			m_SCORE = num;
		}

		//素材提供表示フラグを取得
		const bool GetThxFlag()const
		{
			return m_ThxFlag;
		}

		//素材提供表示フラグを設定
		void SetThxFlag(bool num)
		{
			m_ThxFlag = num;
		}

	};
}
//end basedx11
