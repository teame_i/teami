#pragma once

#include "stdafx.h"

namespace basedx11{
	class MyCamera : public Camera{

		weak_ptr<GameObject> m_Target;

		float m_RadY;
		float m_RadXZ;
		float m_Leap;

		float	m_Arm;

	public:
		//�\�z�Ɣj��
		explicit MyCamera();
		virtual ~MyCamera();

		float Max = 5.0f;
		float Min = 1.0f;

		float YamaTMax = 6.0f;
		float YamaTMin = 2.0f;

		shared_ptr<GameObject> GetTargetObject() const;
		void SetTargetObject(const shared_ptr<GameObject>& Obj);
		float GetToTargetLerp() const;
		void SetToTargetLerp(float f);

		//������
		virtual void OnUpdate() override;

	};


}