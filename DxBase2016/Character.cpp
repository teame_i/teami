#include "stdafx.h"
#include "Project.h"

namespace basedx11{
	//--------------------------------------------------------------------------------------
	//	class FixedBox : public GameObject;
	//	用途: ステージのガイド線
	//--------------------------------------------------------------------------------------
	//構築と破棄
	FixedBox::FixedBox(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
		) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	FixedBox::~FixedBox(){}

	//初期化
	void FixedBox::OnCreate(){

		////床のグループを得る
		//auto Group = GetStage()->GetSharedObjectGroup(L"FIXED_BOX");
		////グループに自分自身を追加
		//Group->IntoGroup(GetThis<FixedBox>());


		auto PtrTransform = AddComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		//反発を実装する場合はRigidbodyをつける
		auto PtrRegid = AddComponent<Rigidbody>();
		//衝突判定
		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);
		//AddComponent<PNTCollisionDraw>();

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_SQUARE");

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_SQUARE");
		PtrDraw->SetTextureResource(L"DANGER_TX");
		PtrDraw->SetOwnShadowActive(true);
		SetDrawActive(false);
		SetDrawLayer(-2);
		SetAlphaActive(true);


	}


	void FixedBox::OnLastUpdate(){
		//マネージャに自分自身の行列を追加する
		auto ManagerPtr = GetStage()->GetSharedGameObject<DrawBoxManager>(L"DrawBoxManager");
		auto PtrTransform = GetComponent<Transform>();
		ManagerPtr->AddInstanceMatrix(PtrTransform->GetWorldMatrix());
	}


	//シェーダー実体（マクロ使用）
	IMPLEMENT_DX11_CONSTANT_BUFFER(CBInstanceShadowmap)
		IMPLEMENT_DX11_VERTEX_SHADER(VSInstanceShadowmap, App::GetApp()->m_wstrRelativeShadersPath + L"VSInstanceShadowmap.cso")
		IMPLEMENT_DX11_CONSTANT_BUFFER(CBInstance)
		IMPLEMENT_DX11_VERTEX_SHADER(VSInstance, App::GetApp()->m_wstrRelativeShadersPath + L"VSInstance.cso")
		IMPLEMENT_DX11_PIXEL_SHADER(PSInstance, App::GetApp()->m_wstrRelativeShadersPath + L"PSInstance.cso")



	//--------------------------------------------------------------------------------------
	//	class InstanceShadowmap : public Shadowmap;
	//	用途: インスタンスシャドウマップコンポーネント
	//--------------------------------------------------------------------------------------
	InstanceShadowmap::InstanceShadowmap(const shared_ptr<GameObject>& GameObjectPtr) :
		Shadowmap(GameObjectPtr){}
	InstanceShadowmap::~InstanceShadowmap(){}

	//操作
	void InstanceShadowmap::OnDraw(){
		//m_GameObjectがnullならDrawできない
		if (IsGameObjectActive()){
			auto PtrGameObject = GetGameObject();
			auto PtrStage = PtrGameObject->GetStage();
			if (!PtrStage){
				return;
			}
			auto PtrT = PtrGameObject->GetComponent<Transform>();
			//ステージから0番目のライトを取り出す
			auto PtrLight = PtrStage->GetTargetLight(0);
			//ステージからカメラを取り出す
			auto PtrCamera = PtrStage->GetTargetCamera();

			if (PtrT && PtrLight && PtrCamera){

				auto Dev = App::GetApp()->GetDeviceResources();
				auto pID3D11DeviceContext = Dev->GetD3DDeviceContext();
				//ステータスのポインタ
				auto RenderStatePtr = PtrStage->GetRenderState();

				//ライトの取得
				Matrix4X4 LightView, LightProj;

				Vector3 LightDir = -1.0 * PtrLight->GetDirectional();
				Vector3 LightAt = PtrCamera->GetAt();
				Vector3 LightEye = LightAt + (LightDir * GetLightHeight());

				//ライトのビューと射影を計算
				LightView.LookAtLH(LightEye, LightAt, Vector3(0, 1.0f, 0));
				LightProj.OrthographicLH(GetViewWidth(), GetViewHeight(), GetLightNear(), GetLightFar());

				InstanceShadowmapConstantBuffer Cb;
				Cb.mView = Matrix4X4EX::Transpose(LightView);
				Cb.mProj = Matrix4X4EX::Transpose(LightProj);

				//これより描画処理
				//コンスタントバッファの更新
				pID3D11DeviceContext->UpdateSubresource(CBInstanceShadowmap::GetPtr()->GetBuffer(), 0, nullptr, &Cb, 0, 0);

				//インプットレイアウトのセット
				pID3D11DeviceContext->IASetInputLayout(VSInstanceShadowmap::GetPtr()->GetInputLayout());
				//マネージャを取得
				auto GameObj = dynamic_pointer_cast<DrawBoxManager>(GetGameObject());
				if (GameObj->GetInstanceVec().size() <= 0){
					return;
				}
				//形状の頂点バッファと行列バッファを設定
				UINT stride[2] = { sizeof(VertexPositionNormalTexture), sizeof(Matrix4X4) };
				UINT offset[2] = { 0, 0 };
				ID3D11Buffer* pBuf[2] = { GetMeshResource()->GetVertexBuffer().Get(), GameObj->GetMatrixBuffer().Get() };
				//頂点バッファをセット
				pID3D11DeviceContext->IASetVertexBuffers(0, 2, pBuf, stride, offset);
				//頂点シェーダーのセット
				pID3D11DeviceContext->VSSetShader(VSInstanceShadowmap::GetPtr()->GetShader(), nullptr, 0);
				//インデックスバッファのセット
				pID3D11DeviceContext->IASetIndexBuffer(GetMeshResource()->GetIndexBuffer().Get(), DXGI_FORMAT_R16_UINT, 0);
				//描画方法（3角形）
				pID3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
				//ピクセルシェーダはセットしない！
				pID3D11DeviceContext->PSSetShader(nullptr, nullptr, 0);
				//ジオメトリシェーダの設定（使用しない）
				pID3D11DeviceContext->GSSetShader(nullptr, nullptr, 0);
				//コンスタントバッファの設定
				ID3D11Buffer* pConstantBuffer = CBInstanceShadowmap::GetPtr()->GetBuffer();
				pID3D11DeviceContext->VSSetConstantBuffers(0, 1, &pConstantBuffer);
				ID3D11Buffer* pNullConstantBuffer = nullptr;
				//コンスタントバッファをピクセルシェーダにセット
				pID3D11DeviceContext->PSSetConstantBuffers(0, 1, &pNullConstantBuffer);


				//描画
				//インスタンス描画(インスタンス配列の数だけ描画)
				pID3D11DeviceContext->DrawIndexedInstanced(GetMeshResource()->GetNumIndicis(), GameObj->GetInstanceVec().size(), 0, 0, 0);
				//後始末
				Dev->InitializeStates(RenderStatePtr);

			}

		}
	}




	//--------------------------------------------------------------------------------------
	//	class DrawBoxManager : public GameObject;
	//	用途: 固定のボックスの描画マネージャ
	//--------------------------------------------------------------------------------------
	//行列用の頂点バッファの作成
	void DrawBoxManager::CreateMatrixBuffer(){
		//行列バッファの作成
		//Max値で作成する
		vector<Matrix4X4> matrices(m_MaxInstance);
		for (auto& m : matrices){
			m = Matrix4X4();
		}
		VertexUtil::CreateDynamicVertexBuffer(m_MatrixBuffer, matrices);
	}

	//構築と破棄
	DrawBoxManager::DrawBoxManager(const shared_ptr<Stage>& StagePtr,
		size_t MaxInstance, const wstring& MeshKey, const wstring& TextureKey) :
		GameObject(StagePtr),
		m_MaxInstance(MaxInstance),
		m_MeshKey(MeshKey),
		m_TextureKey(TextureKey)
	{}
	DrawBoxManager::~DrawBoxManager(){}

	void DrawBoxManager::OnCreate(){
		AddComponent<Transform>();
		//行列バッファの作成
		CreateMatrixBuffer();
		//影をつける（シャドウマップを描画する）
		auto ShadowPtr = AddComponent<InstanceShadowmap>();
		//影の形（メッシュ）を設定
		ShadowPtr->SetMeshResource(m_MeshKey);
		//AddComponent<PNTCollisionDraw>();
		SetAlphaActive(true);


	}
	//操作
	//スケール、回転、位置で追加
	void DrawBoxManager::AddInstanceVectors(const Vector3& Scale, const Vector3& Rot, const Vector3& Pos){
		if (m_InstanceVec.size() >= m_MaxInstance){
			wstring strsz = Util::UintToWStr(m_InstanceVec.size());
			wstring strmax = Util::UintToWStr(m_MaxInstance);
			strsz += L">=";
			strsz += strmax;
			throw BaseException(
				L"これ以上オブジェクトを追加できません。",
				strsz,
				L"DrawBoxManager::AddInstanceVectors()"
				);
		}
		Matrix4X4 mat;
		mat.DefTransformation(Scale, Rot, Pos);
		m_InstanceVec.push_back(mat);
	}
	//行列で追加
	void DrawBoxManager::AddInstanceMatrix(const Matrix4X4& Mat){
		if (m_InstanceVec.size() >= m_MaxInstance){
			wstring strsz = Util::UintToWStr(m_InstanceVec.size());
			wstring strmax = Util::UintToWStr(m_MaxInstance);
			strsz += L">=";
			strsz += strmax;
			throw BaseException(
				L"これ以上オブジェクトを追加できません。",
				strsz,
				L"DrawBoxManager::AddInstanceMatrix()"
				);
		}
		m_InstanceVec.push_back(Mat);
	}
	//仮想関数
	void DrawBoxManager::OnLastUpdate(){
		//デバイスの取得
		auto Dev = App::GetApp()->GetDeviceResources();
		auto pDx11Device = Dev->GetD3DDevice();
		auto pID3D11DeviceContext = Dev->GetD3DDeviceContext();

		D3D11_MAP mapType = D3D11_MAP_WRITE_DISCARD;
		D3D11_MAPPED_SUBRESOURCE mappedBuffer;
		//行列のマップ
		if (FAILED(pID3D11DeviceContext->Map(m_MatrixBuffer.Get(), 0, mapType, 0, &mappedBuffer))){
			// Map失敗
			throw BaseException(
				L"行列のMapに失敗しました。",
				L"if(FAILED(pID3D11DeviceContext->Map()))",
				L"DrawBoxManager::Update3()"
				);
		}
		//行列の変更（インスタンス配列に登録されている数だけ設定）
		auto* matrices = (Matrix4X4*)mappedBuffer.pData;
		for (size_t i = 0; i < m_InstanceVec.size(); i++){
			matrices[i] = Matrix4X4EX::Transpose(m_InstanceVec[i]);
		}
		//アンマップ
		pID3D11DeviceContext->Unmap(m_MatrixBuffer.Get(), 0);

	}


	void DrawBoxManager::OnDraw(){
		//デバイスの取得
		auto Dev = App::GetApp()->GetDeviceResources();
		auto pDx11Device = Dev->GetD3DDevice();
		auto pID3D11DeviceContext = Dev->GetD3DDeviceContext();
		//ステータスのポインタ
		auto RenderStatePtr = GetStage()->GetRenderState();

		//シャドウマップのレンダラーターゲット
		auto ShadoumapPtr = GetStage()->GetShadowMapRenderTarget();

		auto PtrT = GetComponent<Transform>();
		//ステージからカメラを取り出す
		auto PtrCamera = GetStage()->GetTargetCamera();
		//カメラの取得
		Matrix4X4 View, Proj, WorldViewProj;
		View = PtrCamera->GetViewMatrix();
		Proj = PtrCamera->GetProjMatrix();

		//描画するメッシュリソースを取得
		auto MeshRes = App::GetApp()->GetResource<MeshResource>(m_MeshKey);

		//コンスタントバッファの設定
		InstanceConstantBuffer cb1;
		ZeroMemory(&cb1, sizeof(cb1));
		cb1.View = Matrix4X4EX::Transpose(View);
		cb1.Projection = Matrix4X4EX::Transpose(Proj);
		//ライトの設定
		//ステージから0番目のライトを取り出す
		auto PtrLight = GetStage()->GetTargetLight(0);
		cb1.LightDir = PtrLight->GetDirectional();
		cb1.LightDir.w = 1.0f;
		Matrix4X4 LightView, LightProj, LightViewProj;
		Vector3 LightDir = -1.0 * PtrLight->GetDirectional();
		Vector3 LightAt = PtrCamera->GetAt();
		Vector3 LightEye = LightDir;
		LightEye *= Shadowmap::GetLightHeight();
		LightEye = LightAt + LightEye;

		//ライトのビューと射影を計算
		LightView.LookAtLH(LightEye, LightAt, Vector3(0, 1.0f, 0));
		LightProj.OrthographicLH(Shadowmap::GetViewWidth(), Shadowmap::GetViewHeight(),
			Shadowmap::GetLightNear(), Shadowmap::GetLightFar());
		LightViewProj = LightView * LightProj;
		Matrix4X4 LWMatrix = PtrT->GetWorldMatrix() * LightViewProj;
		//コンスタントバッファに設定
		cb1.LPos = LightEye;
		cb1.LPos.w = 0;
		cb1.EyePos = PtrCamera->GetEye();
		cb1.EyePos.w = 0;
		cb1.LView = Matrix4X4EX::Transpose(LightView);
		cb1.LProjection = Matrix4X4EX::Transpose(LightProj);

		//コンスタントバッファの更新
		pID3D11DeviceContext->UpdateSubresource(CBInstance::GetPtr()->GetBuffer(), 0, nullptr, &cb1, 0, 0);
		//ストライドとオフセット
		//形状の頂点バッファと行列バッファを設定
		UINT stride[2] = { sizeof(VertexPositionNormalTexture), sizeof(Matrix4X4) };
		UINT offset[2] = { 0, 0 };
		ID3D11Buffer* pBuf[2] = { MeshRes->GetVertexBuffer().Get(), m_MatrixBuffer.Get() };
		pID3D11DeviceContext->IASetVertexBuffers(0, 2, pBuf, stride, offset);
		//インデックスバッファのセット
		pID3D11DeviceContext->IASetIndexBuffer(MeshRes->GetIndexBuffer().Get(), DXGI_FORMAT_R16_UINT, 0);
		//描画方法（3角形）
		pID3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		//ステータスのポインタ
		//テクスチャを取得
		ID3D11ShaderResourceView* pNull[1] = { 0 };
		ID3D11SamplerState* pNullSR[1] = { 0 };
		//テクスチャを設定
		auto PtrTextureResource = App::GetApp()->GetResource<TextureResource>(m_TextureKey);
		pID3D11DeviceContext->PSSetShaderResources(0, 1, PtrTextureResource->GetShaderResourceView().GetAddressOf());
		//リニアサンプラーを設定
		ID3D11SamplerState* samplerState = RenderStatePtr->GetLinearClamp();
		pID3D11DeviceContext->PSSetSamplers(0, 1, &samplerState);


		//シャドウマップのリソースビューを取得
		ID3D11ShaderResourceView* pShadowSRV = ShadoumapPtr->GetShaderResourceView();
		pID3D11DeviceContext->PSSetShaderResources(1, 1, &pShadowSRV);
		//シャドウ用にリニアサンプラーを取得
		ID3D11SamplerState* pShadowSamplerState = RenderStatePtr->GetLinearClamp();
		//シャドウ用リニアサンプラーを設定
		pID3D11DeviceContext->PSSetSamplers(1, 1, &pShadowSamplerState);


		//デプスステンシルは使用する
		pID3D11DeviceContext->OMSetDepthStencilState(RenderStatePtr->GetDepthDefault(), 0);
		//シェーダの設定
		pID3D11DeviceContext->VSSetShader(VSInstance::GetPtr()->GetShader(), nullptr, 0);
		pID3D11DeviceContext->PSSetShader(PSInstance::GetPtr()->GetShader(), nullptr, 0);
		//インプットレイアウトの設定
		pID3D11DeviceContext->IASetInputLayout(VSInstance::GetPtr()->GetInputLayout());
		//コンスタントバッファの設定
		ID3D11Buffer* pConstantBuffer = CBInstance::GetPtr()->GetBuffer();
		pID3D11DeviceContext->VSSetConstantBuffers(0, 1, &pConstantBuffer);
		pID3D11DeviceContext->PSSetConstantBuffers(0, 1, &pConstantBuffer);



		if (GetAlphaActive()) {
			pID3D11DeviceContext->OMSetBlendState(RenderStatePtr->GetAlphaBlendEx(), nullptr, 0xffffffff);
			//ラスタライザステート
			pID3D11DeviceContext->RSSetState(RenderStatePtr->GetCullFront());
			//描画
			pID3D11DeviceContext->DrawIndexedInstanced(MeshRes->GetNumIndicis(), m_InstanceVec.size(), 0, 0, 0);
			//ラスタライザステート
			pID3D11DeviceContext->RSSetState(RenderStatePtr->GetCullBack());
			//描画
			pID3D11DeviceContext->DrawIndexedInstanced(MeshRes->GetNumIndicis(), m_InstanceVec.size(), 0, 0, 0);
		}
		else {
			pID3D11DeviceContext->OMSetBlendState(RenderStatePtr->GetOpaque(), nullptr, 0xffffffff);
			pID3D11DeviceContext->RSSetState(RenderStatePtr->GetCullNone());
			//描画
			pID3D11DeviceContext->DrawIndexedInstanced(MeshRes->GetNumIndicis(), m_InstanceVec.size(), 0, 0, 0);
		}
		//後始末
		Dev->InitializeStates(RenderStatePtr);
		//インスタンス配列が増減する可能性があるので配列をクリア
		m_InstanceVec.clear();

	}


	//--------------------------------------------------------------------------------------
	//	class ToMenuSprite : public GameObject;
	//	用途: 配置スプライト(メニュースプライト)
	//--------------------------------------------------------------------------------------
	//構築と破棄
	ToMenuSprite::ToMenuSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr), m_StartPos(StartPos){
	}
	ToMenuSprite::~ToMenuSprite(){}

	//初期化
	void ToMenuSprite::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(9.0f, 3.0f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"MENU_TX");

		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_LeftBottomZeroPlusUpY);

		PtrSprite->SetRasterizerState(RasterizerState::CullNone);

		SetAlphaActive(true);
	}


	//--------------------------------------------------------------------------------------
	//	class NormalSprite : public GameObject;
	//	用途: 配置スプライト(Normalスプライト)
	//--------------------------------------------------------------------------------------
	//構築と破棄
	NormalSprite::NormalSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr), m_StartPos(StartPos){
	}
	NormalSprite::~NormalSprite(){}

	//初期化
	void NormalSprite::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(1.0f, 1.0f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"STAGE2_TX");

		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_LeftBottomZeroPlusUpY);

		PtrSprite->SetRasterizerState(RasterizerState::CullNone);

		SetAlphaActive(true);

	}

	void NormalSprite::Rifmotion(){

		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetScale(1.0f, 1.0f, 1.0f);

	}

	//--------------------------------------------------------------------------------------
	//	class HardSprite : public GameObject;
	//	用途: 配置スプライト(Hardスプライト)
	//--------------------------------------------------------------------------------------
	//構築と破棄
	HardSprite::HardSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr), m_StartPos(StartPos){
	}
	HardSprite::~HardSprite(){}

	//初期化
	void HardSprite::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(1.0f, 1.0f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"STAGE3_TX");

		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_LeftBottomZeroPlusUpY);

		PtrSprite->SetRasterizerState(RasterizerState::CullNone);

		SetAlphaActive(true);
	}

	void HardSprite::Rifmotion(){

		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetScale(1.0f, 1.0f, 1.0f);

	}


	//--------------------------------------------------------------------------------------
	//	class EasySprite : public GameObject;
	//	用途: 配置スプライト(Easyスプライト)
	//--------------------------------------------------------------------------------------
	//構築と破棄
	EasySprite::EasySprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr), m_StartPos(StartPos){
	}
	EasySprite::~EasySprite(){}

	//初期化
	void EasySprite::OnCreate(){

		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(1.0f, 1.0f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"STAGE1_TX");

		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_LeftBottomZeroPlusUpY);

		PtrSprite->SetRasterizerState(RasterizerState::CullNone);

		SetAlphaActive(true);

		//BGMの登録
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"BUTTON");

	}

	void EasySprite::OnUpdate(){
	

	}

	void EasySprite::Start(){
		
		//SEの再生
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->Start(L"BUTTON", 0, 0.5f);

	
	}

	void EasySprite::Rifmotion(){
	
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetScale(1.0f,1.0f,1.0f);

	}


	//--------------------------------------------------------------------------------------
	//	class MenuSprite : public GameObject;
	//	用途: 配置スプライト（メニュースプライト）
	//--------------------------------------------------------------------------------------
	//構築と破棄
	MenuSprite::MenuSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr), m_StartPos(StartPos){
	}
	MenuSprite::~MenuSprite(){}

	//初期化
	void MenuSprite::OnCreate(){

		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(5.0f, 2.0f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"Menu_TX");

		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_LeftBottomZeroPlusUpY);

		PtrSprite->SetRasterizerState(RasterizerState::CullNone);

		SetAlphaActive(true);


	}



	//--------------------------------------------------------------------------------------
	//	class ClearSprite : public GameObject;
	//	用途: 配置スプライト(ゲームクリアスプライト)
	//--------------------------------------------------------------------------------------
	//構築と破棄
	ClearSprite::ClearSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr), m_StartPos(StartPos){
	}
	ClearSprite::~ClearSprite(){}

	//初期化
	void ClearSprite::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(9.0f, 2.5f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"CREAR_TX");

		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_LeftBottomZeroPlusUpY);
		PtrSprite->SetRasterizerState(RasterizerState::CullNone);

		SetAlphaActive(true);
	}


	//--------------------------------------------------------------------------------------
	//	class OverSprite : public GameObject;
	//	用途: 配置スプライト(ゲームオーバースプライト)
	//--------------------------------------------------------------------------------------
	//構築と破棄
	OverSprite::OverSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr), m_StartPos(StartPos){
	}
	OverSprite::~OverSprite(){}

	//初期化
	void OverSprite::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(9.0f, 3.0f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"OVER_TX");

		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_LeftBottomZeroPlusUpY);
		PtrSprite->SetRasterizerState(RasterizerState::CullNone);

		SetAlphaActive(true);
	}


	//--------------------------------------------------------------------------------------
	//	class TitelSprite : public GameObject;
	//	用途: 配置スプライト(タイトルスプライト)
	//--------------------------------------------------------------------------------------
	//構築と破棄
	TitelSprite::TitelSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr), m_StartPos(StartPos){
	}
	TitelSprite::~TitelSprite(){}

	//初期化
	void TitelSprite::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(9.0f, 3.0f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"TITEL_TX");

		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_LeftBottomZeroPlusUpY);
		PtrSprite->SetRasterizerState(RasterizerState::CullNone);

		SetAlphaActive(true);
	}

	//--------------------------------------------------------------------------------------
	//	class StartSprite : public GameObject;
	//	用途: 配置オブジェクト(ゲームスタートスプライト)
	//--------------------------------------------------------------------------------------
	//構築と破棄
	StartSprite::StartSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr),
		m_StartPos(StartPos),
		m_color(1, 1, 1, 1),
		m_Max(1.0f),
		m_Min(0.00f),
		m_Speed(0.015f)
	{
	}
	StartSprite::~StartSprite(){}

	//初期化
	void StartSprite::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(500.0f, 100.0f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1,1,1,1));
		PtrSprite->SetTextureResource(L"START_TX");

		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_LeftBottomZeroPlusUpY);
		PtrSprite->SetRasterizerState(RasterizerState::CullNone);

		SetAlphaActive(true);
		//BGMの登録
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"BUTTON");

	}

	void StartSprite::OnUpdate(){

		auto PtrSprite = GetComponent < PCTSpriteDraw >();
		//原点左下
		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_LeftBottomZeroPlusUpY);


		vector<VertexPositionColorTexture> Virtex =
		{
			//左上頂点
			VertexPositionColorTexture(
			Vector3(-0.5f, 2.0f, 0),
			m_color,
			Vector2(0, 0)
			),
			//右上頂点
			VertexPositionColorTexture(
			Vector3(0.5f, 2.0f, 0),
			m_color,
			Vector2(1.0f, 0)
			),
			//左下頂点
			VertexPositionColorTexture(
			Vector3(-0.5f, 0.0f, 0),
			m_color,
			Vector2(0, 1.0f)
			),
			//右下頂点
			VertexPositionColorTexture(
			Vector3(0.5f, 0.0f, 0),
			m_color,
			Vector2(1.0f, 1.0f)
			),
		};
		auto ptr = PtrSprite->GetMeshResource();
		ptr->UpdateVirtexBuffer(Virtex);

		if (m_color.w >= m_Min){
			m_color.w -= m_Speed;
		}
		else if (m_color.w < m_Min){
			m_color.w = m_Max;

		}
	}

	void StartSprite::SetSpeed(float speed){
		//SEの再生
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->Start(L"BUTTON", 0, 1.0f);

		m_Speed = speed;
	}

	//--------------------------------------------------------------------------------------
	//	class AlretSprite : public GameObject;
	//	用途: 警告スプライト
	//--------------------------------------------------------------------------------------
	//構築と破棄
	AlretSprite::AlretSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr),
		m_StartPos(StartPos),
		m_HP(3),
		m_color(1, 1, 1, 1),
		m_Max(1.0f),
		m_Min(0.00f),
		m_Speed(0.015f)
	{}
	AlretSprite::~AlretSprite(){}

	//初期化
	void AlretSprite::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(1280.0f, 404.0f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1, 1, 1, 1));
		PtrSprite->SetTextureResource(L"ALERT_TX");

		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_LeftBottomZeroPlusUpY);
		PtrSprite->SetRasterizerState(RasterizerState::CullNone);

		SetAlphaActive(true);
		//ステートマシンの構築
		m_StateMachine = make_shared< StateMachine<AlretSprite> >(GetThis<AlretSprite>());
		//最初のステートをDefaultStateに設定
		m_StateMachine->SetCurrentState(NomalState::Instance());
		//DefaultStateの初期化実行を行う
		m_StateMachine->GetCurrentState()->Enter(GetThis<AlretSprite>());

	}

	void AlretSprite::OnUpdate(){

		auto PtrSprite = GetComponent < PCTSpriteDraw >();
		//原点左下
		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_LeftBottomZeroPlusUpY);


		vector<VertexPositionColorTexture> Virtex =
		{
			//左上頂点
			VertexPositionColorTexture(
			Vector3(-0.5f, 2.0f, 0),
			m_color,
			Vector2(0, 0)
			),
			//右上頂点
			VertexPositionColorTexture(
			Vector3(0.5f, 2.0f, 0),
			m_color,
			Vector2(1.0f, 0)
			),
			//左下頂点
			VertexPositionColorTexture(
			Vector3(-0.5f, 0.0f, 0),
			m_color,
			Vector2(0, 1.0f)
			),
			//右下頂点
			VertexPositionColorTexture(
			Vector3(0.5f, 0.0f, 0),
			m_color,
			Vector2(1.0f, 1.0f)
			),
		};
		auto ptr = PtrSprite->GetMeshResource();
		ptr->UpdateVirtexBuffer(Virtex);

		if (m_color.w >= m_Min){
			m_color.w -= m_Speed;
		}
		else if (m_color.w < m_Min){
			m_color.w = m_Max;

		}
	}
	void AlretSprite::CState(){
		GetStateMachine()->ChangeState(FlashingState::Instance());
	}

	void AlretSprite::RState(){
		GetStateMachine()->ChangeState(NomalState::Instance());
	}

	void AlretSprite::SetHP(int hp){

		m_HP = hp;
	}
	void AlretSprite::SetSpeed(float speed){
		m_Speed = speed;
	}

	//--------------------------------------------------------------------------------------
	//	class NomalState : public ObjState<AlretSprite>;
	//	用途: 通常ステート
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<NomalState> NomalState::Instance(){
		static shared_ptr<NomalState> instance;
		if (!instance){
			instance = shared_ptr<NomalState>(new NomalState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void NomalState::Enter(const shared_ptr<AlretSprite>& Obj){
		Obj->SetDrawActive(false);
	}
	//ステート実行中に毎ターン呼ばれる関数
	void NomalState::Execute(const shared_ptr<AlretSprite>& Obj){
		Obj->SetDrawActive(false);

	}
	//ステートにから抜けるときに呼ばれる関数
	void NomalState::Exit(const shared_ptr<AlretSprite>& Obj){
		//何もしない

	}

	//--------------------------------------------------------------------------------------
	//	class FlashingState : public ObjState<AlretSprite>;
	//	用途: 点滅ステート(スプライトを表示させ点滅させる)
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<FlashingState > FlashingState::Instance(){
		static shared_ptr<FlashingState > instance;
		if (!instance){
			instance = shared_ptr<FlashingState >(new FlashingState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void FlashingState::Enter(const shared_ptr<AlretSprite>& Obj){
		Obj->SetDrawActive(true);
	}
	//ステート実行中に毎ターン呼ばれる関数
	void FlashingState::Execute(const shared_ptr<AlretSprite>& Obj){

	}
	//ステートにから抜けるときに呼ばれる関数
	void FlashingState::Exit(const shared_ptr<AlretSprite>& Obj){
	}



	//--------------------------------------------------------------------------------------
	//	class ReTuroSprite : public GameObject;
	//	用途: 『ReTutoSprite』ゲームの最初に出る注意事項のスプライトの表示
	//--------------------------------------------------------------------------------------
	//構築と破棄
	ReTuroSprite::ReTuroSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr),
		m_StartPos(StartPos),
		m_color(1, 1, 1, 1),
		m_Max(1.0f),
		m_Min(0.00f),
		m_Speed(0.02f)
	{
	}
	ReTuroSprite::~ReTuroSprite(){}

	//初期化
	void ReTuroSprite::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos.x, m_StartPos.y + 500.0f, m_StartPos.z);
		PtrTransform->SetScale(1100.0f, 40.0f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1, 1, 1, 1));
		PtrSprite->SetTextureResource(L"RETUTO_TX");

		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_LeftBottomZeroPlusUpY);
		PtrSprite->SetRasterizerState(RasterizerState::CullNone);

		SetAlphaActive(true);

		Flash_Count = 0;

	}

	void ReTuroSprite::OnUpdate(){

		auto PtrSprite = GetComponent < PCTSpriteDraw >();
		//原点左下
		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_LeftBottomZeroPlusUpY);
		Flash_Count++;

		vector<VertexPositionColorTexture> Virtex =
		{
			//左上頂点
			VertexPositionColorTexture(
			Vector3(-0.5f, 2.0f, 0),
			m_color,
			Vector2(0, 0)
			),
			//右上頂点
			VertexPositionColorTexture(
			Vector3(0.5f, 2.0f, 0),
			m_color,
			Vector2(1.0f, 0)
			),
			//左下頂点
			VertexPositionColorTexture(
			Vector3(-0.5f, 0.0f, 0),
			m_color,
			Vector2(0, 1.0f)
			),
			//右下頂点
			VertexPositionColorTexture(
			Vector3(0.5f, 0.0f, 0),
			m_color,
			Vector2(1.0f, 1.0f)
			),
		};
		auto ptr = PtrSprite->GetMeshResource();
		ptr->UpdateVirtexBuffer(Virtex);

		/*--------------------点滅処理-------------------------*/

		//点滅するタイミング(点滅するまでの時間の設定)
		if (Flash_Count >= 250)
		{
			if (m_color.w >= m_Min){
				m_color.w -= m_Speed;

			}
			else if (m_color.w < m_Min){
				m_color.w = m_Max;

			}
			//消えるタイミング（点滅してから消えるまでの時間の設定）
			if (Flash_Count >= 300)
			{
				PtrSprite->SetDrawActive(false);
			}
		}
	}


	//--------------------------------------------------------------------------------------
	//class MultiEfect : public MultiParticle;
	//用途: 複数のスパーククラス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	MultiEfect::MultiEfect(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr)
	{}
	MultiEfect::~MultiEfect(){}

	void MultiEfect::InsertSpark(const Vector3& Pos){
		auto ParticlePtr = InsertParticle(7);
		ParticlePtr->SetEmitterPos(Pos);
		ParticlePtr->SetTextureResource(L"LIGHTNING_TX");
		ParticlePtr->SetMaxTime(1.5f);
		vector<ParticleSprite>& pSpriteVec = ParticlePtr->GetParticleSpriteVec();
		for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()){
			rParticleSprite.m_LocalPos.x = Util::RandZeroToOne() * 0.2f - 0.5f;
			rParticleSprite.m_LocalPos.y = Util::RandZeroToOne() * 0.1f + 0.3f;
			rParticleSprite.m_LocalPos.z = Util::RandZeroToOne() * 0.2f - 0.5f;
			//各パーティクルの移動速度を指定
			rParticleSprite.m_Velocity = Vector3(
				rParticleSprite.m_LocalPos.x * 2.0f,
				rParticleSprite.m_LocalPos.y * 2.0f,
				rParticleSprite.m_LocalPos.z * 2.0f
				);
			//色の指定
			rParticleSprite.m_Color = Color4(1.0f, 1.0f, 0.0f, 0.5f);

		}
	}



	//--------------------------------------------------------------------------------------
	//class MultiSpark : public MultiParticle;
	//用途: 複数のスパーククラス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	MultiSpark::MultiSpark(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr)
	{}
	MultiSpark::~MultiSpark(){}

	void MultiSpark::InsertSpark(const Vector3& Pos){
		auto ParticlePtr = InsertParticle(7);
		ParticlePtr->SetEmitterPos(Pos);
		ParticlePtr->SetTextureResource(L"SPARK_TX");
		ParticlePtr->SetMaxTime(0.5f);
		vector<ParticleSprite>& pSpriteVec = ParticlePtr->GetParticleSpriteVec();
		for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()){
			rParticleSprite.m_LocalPos.x = Util::RandZeroToOne() * 0.2f - 0.5f;
			rParticleSprite.m_LocalPos.y = Util::RandZeroToOne() * 0.1f + 0.3f;
			rParticleSprite.m_LocalPos.z = Util::RandZeroToOne() * 0.2f - 0.5f;
			//各パーティクルの移動速度を指定
			rParticleSprite.m_Velocity = Vector3(
				rParticleSprite.m_LocalPos.x * 5.0f,
				rParticleSprite.m_LocalPos.y * 5.0f,
				rParticleSprite.m_LocalPos.z * 5.0f
				);
			//色の指定
			rParticleSprite.m_Color = Color4(1.0f, 1.0f, 0.0f, 0.5f);

		}
	}


	//--------------------------------------------------------------------------------------
	//class MultiSpark_jr : public MultiParticle;
	//用途: 複数の小規模なスパーククラス
	//--------------------------------------------------------------------------------------

	//構築と破棄
	MultiSpark_jr::MultiSpark_jr(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr)
	{}
	MultiSpark_jr::~MultiSpark_jr(){}

	//ハートを生成します。
	void MultiSpark_jr::InsertSpark(const Vector3& Pos, UINT num){
		auto ParticlePtr = InsertParticle(num);
		ParticlePtr->SetEmitterPos(Vector3(0, 0, 0));
		ParticlePtr->SetTextureResource(L"Dameage_Enemy_TX");
		ParticlePtr->SetMaxTime(10.0f);
		vector<ParticleSprite>& pSpriteVec = ParticlePtr->GetParticleSpriteVec();
		for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()){
			//各パーティクルの初期座標を少しずらす
			rParticleSprite.m_LocalPos.x = Util::RandZeroToOne() * 2.0f;
			rParticleSprite.m_LocalPos.z = Util::RandZeroToOne() * 2.0f;

			float HalfOne_x = Util::DivProbability(2);// 1/2の確率でtrueを返す
			float HalfOne_z = Util::DivProbability(2);// 1/2の確率でtrueを返す

			if (HalfOne_x){ rParticleSprite.m_LocalPos.x = 0 - Util::RandZeroToOne() * 2.0f; }
			else{
				rParticleSprite.m_LocalPos.x = Util::RandZeroToOne() * 2.0f;
			}

			rParticleSprite.m_LocalPos.y = Util::RandZeroToOne() * 2.0f;


			if (HalfOne_z){ rParticleSprite.m_LocalPos.z = 0 - Util::RandZeroToOne() * 2.0f; }
			else{
				rParticleSprite.m_LocalPos.z = Util::RandZeroToOne() * 2.0f;
			}

			//各パーティクルの移動速度を指定
			rParticleSprite.m_Velocity = Vector3(
				rParticleSprite.m_LocalPos.x * 0.1f,
				rParticleSprite.m_LocalPos.y * 0.1f,
				rParticleSprite.m_LocalPos.z * 0.1f
				);
			//各パーティクルの座標を、渡された座標からの相対位置にする。
			rParticleSprite.m_LocalPos += Pos;
			//各パーティクルの大きさを指定
			rParticleSprite.m_LocalScale = Vector3(1, 1, 1) * 1.0f;
			//色の指定
			Color4 color(1, 1, 1, 1);
			rParticleSprite.m_Color = color;
		}
	}

	void MultiSpark_jr::OnUpdate(){
		//前回のターンからの時間
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		//カメラの座標を取得
		auto PtrCamera = GetStage()->GetCamera(0);
		auto CameraPos = PtrCamera->GetEye();

		bool isExplosion = false;
		auto CntrVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntrVec[0].wButtons & XINPUT_GAMEPAD_B){
			isExplosion = true;
		}

		for (auto ParticlePtr : GetParticleVec()){
			//トータル時間に加算
			ParticlePtr->AddTotalTime(ElapsedTime);
			for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()){
				//アクティブで無かったら、次のループへ進む
				if (!rParticleSprite.m_Active){
					continue;
				}

				//移動速度（目的の位置とパーティクルの位置の差）
				Vector3 Vel = CameraPos - rParticleSprite.m_LocalPos;
				if (Vector3EX::Length(Vel) > 10.0f){
					rParticleSprite.m_Active = false;
					continue;
				}
				if (Vector3EX::Length(Vel) > 4.0f){
					Vel.Zero();
				}
				if (isExplosion){
					Vel += CameraPos - rParticleSprite.m_LocalPos;
				}
				Vel.Normalize();
				Vel *= -1;
				Vel += (Vel - rParticleSprite.m_Velocity) * ElapsedTime;
				//移動速度の更新
				rParticleSprite.m_Velocity += Vel;
				//移動速度に従って移動させる
				rParticleSprite.m_LocalPos += rParticleSprite.m_Velocity * ElapsedTime;
				////色を変化させる(だんだん透明になる)
				rParticleSprite.m_Color.w *= 0.95f;
				//
				rParticleSprite.m_LocalScale *= 0.97;
				if (ParticlePtr->GetTotalTime() >= ParticlePtr->GetMaxTime()){
					//制限時間になったら、フラグを下げる
					rParticleSprite.m_Active = false;
				}
			}
		}

	}

	//--------------------------------------------------------------------------------------
	//class MultiSpark_Heart : public MultiParticle;
	//用途: 複数の小規模なハートのスパーククラス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	MultiSpark_Heart::MultiSpark_Heart(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr)
	{}
	MultiSpark_Heart::~MultiSpark_Heart(){}

	//ハートを生成します。
	void MultiSpark_Heart::InsertSpark(const Vector3& Pos, UINT num){
		auto ParticlePtr = InsertParticle(num);
		ParticlePtr->SetEmitterPos(Vector3(0, 0, 0));
		ParticlePtr->SetTextureResource(L"heart_a_TX");
		ParticlePtr->SetMaxTime(5.0f);
		vector<ParticleSprite>& pSpriteVec = ParticlePtr->GetParticleSpriteVec();
		for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()){
			//各パーティクルの初期座標を少しずらす
			float HalfOne_x = Util::DivProbability(2);// 1/2の確率でtrueを返す
			float HalfOne_z = Util::DivProbability(2);// 1/2の確率でtrueを返す
			if (HalfOne_x){ rParticleSprite.m_LocalPos.x = 0 - Util::RandZeroToOne() * 0.1f; }
			else{
				rParticleSprite.m_LocalPos.x = Util::RandZeroToOne() * 0.1f;
			}

			rParticleSprite.m_LocalPos.y = Util::RandZeroToOne() * 0.1f;


			if (HalfOne_z){ rParticleSprite.m_LocalPos.z = 0 - Util::RandZeroToOne() * 0.1f; }
			else{
				rParticleSprite.m_LocalPos.z = Util::RandZeroToOne() * 0.1f;
			}
			//各パーティクルの移動速度を指定
			rParticleSprite.m_Velocity = Vector3(
				rParticleSprite.m_LocalPos.x * 20.0f,
				rParticleSprite.m_LocalPos.y * 20.0f,
				rParticleSprite.m_LocalPos.z * 20.0f
				);
			//各パーティクルの座標を、渡された座標からの相対位置にする。
			rParticleSprite.m_LocalPos += Pos;
			//各パーティクルの大きさを指定
			rParticleSprite.m_LocalScale = Vector3(1, 1, 1) * 0.5f;
			//色の指定
			Color4 color(1, 1, 1, 1);
			rParticleSprite.m_Color = color;
		}
	}

	void MultiSpark_Heart::OnUpdate(){
		//前回のターンからの時間
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		//カメラの座標を取得
		auto PtrCamera = GetStage()->GetCamera(0);
		auto CameraPos = PtrCamera->GetEye();
		//	auto player = GetStage()->GetSharedGameObject<EnemyBoss>(L"EnemyBoss");
		//	CameraPos = player->GetComponent<Transform>()->GetPosition();

		bool isExplosion = false;
		auto CntrVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntrVec[0].wButtons & XINPUT_GAMEPAD_B){
			isExplosion = true;
		}

		for (auto ParticlePtr : GetParticleVec()){
			//トータル時間に加算
			ParticlePtr->AddTotalTime(ElapsedTime);
			for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()){
				//アクティブで無かったら、次のループへ進む
				if (!rParticleSprite.m_Active){
					continue;
				}

				//移動速度（目的の位置とパーティクルの位置の差）
				Vector3 Vel = CameraPos - rParticleSprite.m_LocalPos;
				if (Vector3EX::Length(Vel) > 10.0f){
					rParticleSprite.m_Active = false;
					continue;
				}
				if (Vector3EX::Length(Vel) > 4.0f){
					Vel.Zero();
				}
				if (isExplosion){
					Vel += CameraPos - rParticleSprite.m_LocalPos;
				}
				Vel.Normalize();
				Vel *= -1;
				Vel += (Vel - rParticleSprite.m_Velocity) * ElapsedTime;
				//移動速度の更新
				rParticleSprite.m_Velocity += Vel;
				//移動速度に従って移動させる
				rParticleSprite.m_LocalPos += rParticleSprite.m_Velocity * ElapsedTime;
				////色を変化させる(だんだん透明になる)
				rParticleSprite.m_Color.w *= 0.95f;
				if (ParticlePtr->GetTotalTime() >= ParticlePtr->GetMaxTime()){
					//制限時間になったら、フラグを下げる
					rParticleSprite.m_Active = false;
				}
			}
		}

	}

	//--------------------------------------------------------------------------------------
	//class MultiSpark_Circle : public MultiParticle;
	//用途: 複数の小規模なCircleスパーククラス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	MultiSpark_Circle::MultiSpark_Circle(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr)
	{}
	MultiSpark_Circle::~MultiSpark_Circle(){}

	void MultiSpark_Circle::InsertSpark(const Vector3& Pos){
		auto ParticlePtr = InsertParticle(1);
		ParticlePtr->SetEmitterPos(Pos);
		ParticlePtr->SetTextureResource(L"WAVE_TX");
		ParticlePtr->SetMaxTime(0.1f);
		ParticlePtr->SetDrawOption(Particle::DrawOption::Normal);

		//各パーティクルの初期位置を設定？
		vector<ParticleSprite>& pSpriteVec = ParticlePtr->GetParticleSpriteVec();
		for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()){
			//rParticleSprite.m_LocalPos.x = Util::RandZeroToOne();
			rParticleSprite.m_LocalPos.y = Util::RandZeroToOne() - 0.5;
			//rParticleSprite.m_LocalPos.z = Util::RandZeroToOne();

			//パーティクルの角度を変更
			float Pitch = (90.0f / 180.0f * XM_PI);
			//	rParticleSprite.m_LocalQt.RotationRollPitchYaw(90.0f/180.0f * XM_PI, 0.0f, 0.0f);
			rParticleSprite.m_LocalQt.RotationRollPitchYaw(Pitch, 0.0f, 0.0f);

			//各パーティクルの移動速度を指定
			rParticleSprite.m_Velocity = Vector3(
				rParticleSprite.m_LocalPos.x,
				rParticleSprite.m_LocalPos.y,
				rParticleSprite.m_LocalPos.z
				);
			//色の指定
			rParticleSprite.m_Color = Color4(1.0f, 1.0f, 1.0f, 0.5f);

		}
	}

	//--------------------------------------------------------------------------------------
	//class MultiSpark_STAN : public MultiParticle;
	//用途: 複数の小規模な黄色いCircleのスパーククラス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	MultiSpark_STAN::MultiSpark_STAN(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr)
	{}
	MultiSpark_STAN::~MultiSpark_STAN(){}

	//ハートを生成します。
	void MultiSpark_STAN::InsertSpark(const Vector3& Pos, UINT num){
		auto ParticlePtr = InsertParticle(num);
		ParticlePtr->SetEmitterPos(Vector3(0, 0, 0));
		ParticlePtr->SetTextureResource(L"LIGHTNING_TX");
		ParticlePtr->SetMaxTime(5.0f);
		vector<ParticleSprite>& pSpriteVec = ParticlePtr->GetParticleSpriteVec();
		for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()){
			//各パーティクルの初期座標を少しずらす
			float HalfOne_x = Util::DivProbability(2);// 1/2の確率でtrueを返す
			float HalfOne_z = Util::DivProbability(2);// 1/2の確率でtrueを返す
			if (HalfOne_x){ rParticleSprite.m_LocalPos.x = 0 - Util::RandZeroToOne() * 0.1f; }
			else{
				rParticleSprite.m_LocalPos.x = Util::RandZeroToOne() * 0.1f;
			}

			rParticleSprite.m_LocalPos.y = Util::RandZeroToOne() * 0.1f;


			if (HalfOne_z){ rParticleSprite.m_LocalPos.z = 0 - Util::RandZeroToOne() * 0.1f; }
			else{
				rParticleSprite.m_LocalPos.z = Util::RandZeroToOne() * 0.1f;
			}
			//各パーティクルの移動速度を指定
			rParticleSprite.m_Velocity = Vector3(
				rParticleSprite.m_LocalPos.x * 20.0f,
				rParticleSprite.m_LocalPos.y * 20.0f,
				rParticleSprite.m_LocalPos.z * 20.0f
				);
			//各パーティクルの座標を、渡された座標からの相対位置にする。
			rParticleSprite.m_LocalPos += Pos;
			//各パーティクルの大きさを指定
			rParticleSprite.m_LocalScale = Vector3(1, 1, 1) * 0.5f;
			//色の指定
			Color4 color(1, 1, 1, 1);
			rParticleSprite.m_Color = color;
		}
	}

	void MultiSpark_STAN::OnUpdate(){
		//前回のターンからの時間
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		//カメラの座標を取得
		auto PtrCamera = GetStage()->GetCamera(0);
		auto CameraPos = PtrCamera->GetEye();
		//	auto player = GetStage()->GetSharedGameObject<EnemyBoss>(L"EnemyBoss");
		//	CameraPos = player->GetComponent<Transform>()->GetPosition();

		bool isExplosion = false;
		auto CntrVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntrVec[0].wButtons & XINPUT_GAMEPAD_B){
			isExplosion = true;
		}

		for (auto ParticlePtr : GetParticleVec()){
			//トータル時間に加算
			ParticlePtr->AddTotalTime(ElapsedTime);
			for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()){
				//アクティブで無かったら、次のループへ進む
				if (!rParticleSprite.m_Active){
					continue;
				}

				//移動速度（目的の位置とパーティクルの位置の差）
				Vector3 Vel = CameraPos - rParticleSprite.m_LocalPos;
				if (Vector3EX::Length(Vel) > 10.0f){
					rParticleSprite.m_Active = false;
					continue;
				}
				if (Vector3EX::Length(Vel) > 4.0f){
					Vel.Zero();
				}
				if (isExplosion){
					Vel += CameraPos - rParticleSprite.m_LocalPos;
				}
				Vel.Normalize();
				Vel *= -1;
				Vel += (Vel - rParticleSprite.m_Velocity) * ElapsedTime;
				//移動速度の更新
				rParticleSprite.m_Velocity += Vel;
				//移動速度に従って移動させる
				rParticleSprite.m_LocalPos += rParticleSprite.m_Velocity * ElapsedTime;
				////色を変化させる(だんだん透明になる)
				rParticleSprite.m_Color.w *= 0.95f;
				if (ParticlePtr->GetTotalTime() >= ParticlePtr->GetMaxTime()){
					//制限時間になったら、フラグを下げる
					rParticleSprite.m_Active = false;
				}
			}
		}

	}

	//--------------------------------------------------------------------------------------
	//class MultiCross : public MultiParticle;
	//用途: 干渉点に出るエフェクト
	//--------------------------------------------------------------------------------------
	//構築と破棄
	MultiCross::MultiCross(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr)
	{}
	MultiCross::~MultiCross(){}

	void MultiCross::InsertSpark(const Vector3& Pos){
		auto ParticlePtr = InsertParticle(1);
		ParticlePtr->SetEmitterPos(Pos);
		ParticlePtr->SetTextureResource(L"EFECT_TX");
		ParticlePtr->SetMaxTime(0.5f);
		vector<ParticleSprite>& pSpriteVec = ParticlePtr->GetParticleSpriteVec();
		for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()){

			//色の指定
			rParticleSprite.m_Color = Color4(1.0f, 1.0f, 1.0f, 0.3f);
		}
	}
	void MultiCross::OnUpdate()
	{
		float ElapsedTime = App::GetApp()->GetElapsedTime();

		for (auto ParticlePtr : GetParticleVec()){
			//トータル時間に加算
			ParticlePtr->AddTotalTime(ElapsedTime);
			for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()){
				//アクティブで無かったら、次のループへ進む
				if (!rParticleSprite.m_Active){
					continue;
				}

				if (ParticlePtr->GetTotalTime() >= ParticlePtr->GetMaxTime()){
					//制限時間になったら、フラグを下げる
					rParticleSprite.m_Active = false;
				}
			}

		}
	}

	//-------------------------------------------------------------------------------------------------------------------------------------------
	//	class GoToMenuSprite : public GameObject;
	//	用途: 配置オブジェクト(ゲームクリア、オーバー時に出すメニューに戻るスプライト)
	//--------------------------------------------------------------------------------------
	//構築と破棄
	GoToMenuSprite::GoToMenuSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr), m_StartPos(StartPos){
	}
	GoToMenuSprite::~GoToMenuSprite(){}

	//初期化
	void GoToMenuSprite::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(1.0f, 1.0f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"GOtoMENU_TX");

		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_LeftBottomZeroPlusUpY);
		PtrSprite->SetRasterizerState(RasterizerState::CullNone);

		SetAlphaActive(true);

	}
	//更新
	void GoToMenuSprite::OnUpdate()
	{

	}
	//--------------------------------------------------------------------------------------
	//	class GoToTitleSprite : public GameObject;
	//	用途: 配置オブジェクト(ゲームクリア、オーバー時に出すタイトルに戻るスプライト)
	//--------------------------------------------------------------------------------------
	//構築と破棄
	GoToTitleSprite::GoToTitleSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr), m_StartPos(StartPos){
	}
	GoToTitleSprite::~GoToTitleSprite(){}

	//初期化
	void GoToTitleSprite::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(1.0f, 1.0f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"GOtoTITLE_TX");

		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_LeftBottomZeroPlusUpY);
		PtrSprite->SetRasterizerState(RasterizerState::CullNone);

		SetAlphaActive(true);
	}

	//--------------------------------------------------------------------------------------
	//	class ScoreSprite : public GameObject;
	//	用途: 配置スプライト(Scoreスプライト)
	//--------------------------------------------------------------------------------------
	//構築と破棄
	ScoreSprite::ScoreSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr), m_StartPos(StartPos){
	}
	ScoreSprite::~ScoreSprite(){}

	//初期化
	void ScoreSprite::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(1.5f, 1.0f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"SCORE_TX");

		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_LeftBottomZeroPlusUpY);
		PtrSprite->SetRasterizerState(RasterizerState::CullNone);

		SetAlphaActive(true);
	}
	
	//--------------------------------------------------------------------------------------
	//	class TIMESprite : public GameObject;
	//	用途: 配置スプライト(Timeスプライト)
	//--------------------------------------------------------------------------------------
	//構築と破棄
	TIMESprite::TIMESprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos,const Vector3& StartScale) :
		GameObject(StagePtr), m_StartPos(StartPos),m_StartScale(StartScale){
	}
	TIMESprite::~TIMESprite(){}
	//初期化
	void TIMESprite::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos); 
		PtrTransform->SetScale(m_StartScale);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"TIME_TX");

		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_LeftBottomZeroPlusUpY);
		PtrSprite->SetRasterizerState(RasterizerState::CullNone);

		SetAlphaActive(true);
	}

	//--------------------------------------------------------------------------------------
	//	class EnemyUISprite : public GameObject;
	//	用途: 配置スプライト(ゲーム画面に出す「Enemy」スプライト)
	//--------------------------------------------------------------------------------------
	//構築と破棄
	EnemyUISprite::EnemyUISprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr), m_StartPos(StartPos){
	}
	EnemyUISprite::~EnemyUISprite(){}
	//初期化
	void EnemyUISprite::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(0.8f, 0.6f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"ENEMYUI_TX");

		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_LeftBottomZeroPlusUpY);
		PtrSprite->SetRasterizerState(RasterizerState::CullNone);

		SetAlphaActive(true);
	}



	//--------------------------------------------------------------------------------------
	//	class RANKSprite : public GameObject;
	//	用途: 配置スプライト(ゲームクリア画面にだす「Rank」スプライト)
	//--------------------------------------------------------------------------------------
	//構築と破棄
	RANKSprite::RANKSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr), m_StartPos(StartPos){
	}
	RANKSprite::~RANKSprite(){}

	//初期化
	void RANKSprite::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(1.5f, 1.2f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"RANK_TX");

		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_LeftBottomZeroPlusUpY);
		PtrSprite->SetRasterizerState(RasterizerState::CullNone);

		SetAlphaActive(true);
	}

	//--------------------------------------------------------------------------------------
	//	class RANK_A_Sprite : public GameObject;
	//	用途: 配置スプライト(ランクAスプライト)
	//--------------------------------------------------------------------------------------
	//構築と破棄
	RANK_ASprite::RANK_ASprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr), m_StartPos(StartPos){
	}
	RANK_ASprite::~RANK_ASprite(){}

	//初期化
	void RANK_ASprite::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(1.5f, 1.0f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"RANK_A_TX");

		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_LeftBottomZeroPlusUpY);
		PtrSprite->SetRasterizerState(RasterizerState::CullNone);

		SetAlphaActive(true);
	}

	//--------------------------------------------------------------------------------------
	//	class RANK_B_Sprite : public GameObject;
	//	用途: 配置スプライト(ランクBスプライト)
	//--------------------------------------------------------------------------------------
	//構築と破棄
	RANK_BSprite::RANK_BSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr), m_StartPos(StartPos){
	}
	RANK_BSprite::~RANK_BSprite(){}

	//初期化
	void RANK_BSprite::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(1.5f, 1.0f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"RANK_B_TX");

		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_LeftBottomZeroPlusUpY);
		PtrSprite->SetRasterizerState(RasterizerState::CullNone);

		SetAlphaActive(true);
	}

	//--------------------------------------------------------------------------------------
	//	class RANK_C_Sprite : public GameObject;
	//	用途: 配置スプライト(ランクCスプライト)
	//--------------------------------------------------------------------------------------
	//構築と破棄
	RANK_CSprite::RANK_CSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr), m_StartPos(StartPos){
	}
	RANK_CSprite::~RANK_CSprite(){}

	//初期化
	void RANK_CSprite::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(1.5f, 1.0f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"RANK_C_TX");

		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_LeftBottomZeroPlusUpY);
		PtrSprite->SetRasterizerState(RasterizerState::CullNone);

		SetAlphaActive(true);
	}

	//--------------------------------------------------------------------------------------
	//	class ToNextSprite : public GameObject;
	//	用途: 配置スプライト(チュートリアルスプライト『次へ』)
	//--------------------------------------------------------------------------------------
	//構築と破棄
	ToNextSprite::ToNextSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr),
		m_StartPos(StartPos),
		m_color(1, 1, 1, 1),
		m_Max(1.0f),
		m_Min(0.00f),
		m_Speed(0.015f)
	{
	}
	ToNextSprite::~ToNextSprite(){}

	//初期化
	void ToNextSprite::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos.x + 500.0f, m_StartPos.y + 150.0f, m_StartPos.z);
		PtrTransform->SetScale(250.0f, 50.0f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1, 1, 1, 1));
		PtrSprite->SetTextureResource(L"NEXT_TX");

		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_LeftBottomZeroPlusUpY);
		PtrSprite->SetRasterizerState(RasterizerState::CullNone);

		SetAlphaActive(true);

	}

	void ToNextSprite::OnUpdate(){

		auto PtrSprite = GetComponent < PCTSpriteDraw >();
		//原点左下
		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_LeftBottomZeroPlusUpY);


		vector<VertexPositionColorTexture> Virtex =
		{
			//左上頂点
			VertexPositionColorTexture(
			Vector3(-0.5f, 2.0f, 0),
			m_color,
			Vector2(0, 0)
			),
			//右上頂点
			VertexPositionColorTexture(
			Vector3(0.5f, 2.0f, 0),
			m_color,
			Vector2(1.0f, 0)
			),
			//左下頂点
			VertexPositionColorTexture(
			Vector3(-0.5f, 0.0f, 0),
			m_color,
			Vector2(0, 1.0f)
			),
			//右下頂点
			VertexPositionColorTexture(
			Vector3(0.5f, 0.0f, 0),
			m_color,
			Vector2(1.0f, 1.0f)
			),
		};
		auto ptr = PtrSprite->GetMeshResource();
		ptr->UpdateVirtexBuffer(Virtex);

		if (m_color.w >= m_Min){
			m_color.w -= m_Speed;
		}
		else if (m_color.w < m_Min){
			m_color.w = m_Max;

		}
	}


	//--------------------------------------------------------------------------------------
	//	class ToPrevSprite : public GameObject;
	//	用途: 配置スプライト(チュートリアルスプライト『戻る』)
	//--------------------------------------------------------------------------------------
	//構築と破棄
	ToPrevSprite::ToPrevSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr),
		m_StartPos(StartPos),
		m_color(1, 1, 1, 1),
		m_Max(1.0f),
		m_Min(0.00f),
		m_Speed(0.015f)
	{
	}
	ToPrevSprite::~ToPrevSprite(){}

	//初期化
	void ToPrevSprite::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos.x - 500.0, m_StartPos.y + 150.0f, m_StartPos.z);
		PtrTransform->SetScale(250.0f, 50.0f, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1, 1, 1, 1));
		PtrSprite->SetTextureResource(L"PREV_TX");

		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_LeftBottomZeroPlusUpY);
		PtrSprite->SetRasterizerState(RasterizerState::CullNone);

		SetAlphaActive(true);

	}

	void ToPrevSprite::OnUpdate(){

		auto PtrSprite = GetComponent < PCTSpriteDraw >();
		//原点左下
		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_LeftBottomZeroPlusUpY);


		vector<VertexPositionColorTexture> Virtex =
		{
			//左上頂点
			VertexPositionColorTexture(
			Vector3(-0.5f, 2.0f, 0),
			m_color,
			Vector2(0, 0)
			),
			//右上頂点
			VertexPositionColorTexture(
			Vector3(0.5f, 2.0f, 0),
			m_color,
			Vector2(1.0f, 0)
			),
			//左下頂点
			VertexPositionColorTexture(
			Vector3(-0.5f, 0.0f, 0),
			m_color,
			Vector2(0, 1.0f)
			),
			//右下頂点
			VertexPositionColorTexture(
			Vector3(0.5f, 0.0f, 0),
			m_color,
			Vector2(1.0f, 1.0f)
			),
		};
		auto ptr = PtrSprite->GetMeshResource();
		ptr->UpdateVirtexBuffer(Virtex);

		if (m_color.w >= m_Min){
			m_color.w -= m_Speed;
		}
		else if (m_color.w < m_Min){
			m_color.w = m_Max;

		}
	}

	//--------------------------------------------------------------------------------------
	//	class ToTutorialSprite : public GameObject;
	//	用途: 配置オブジェクト
	//--------------------------------------------------------------------------------------
	//構築と破棄
	ToTutorialSprite::ToTutorialSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr), m_StartPos(StartPos){
	}
	ToTutorialSprite::~ToTutorialSprite(){}

	//初期化
	void ToTutorialSprite::OnCreate(){
		//画像切り替えのために使う関数

		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(Vector3(m_StartPos.x - 10.0f, m_StartPos.y - 85.0f, m_StartPos.z));
		PtrTransform->SetScale(10.0f, 6.0f, 9.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"PLAYRULE_TX");

		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_LeftBottomZeroPlusUpY);
		PtrSprite->SetRasterizerState(RasterizerState::CullNone);

		SetAlphaActive(true);
	}

	bool ToTutorialSprite::ChangeSprite()
	{
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();

		if (CntlVec[0].bConnected){

			//Aボタンが押されたら.
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A)
			{
				PtrSprite->SetTextureResource(L"ITEMRULE_TX");
				return true;
			}

			//Bボタンが押されたら
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_B)
			{
				PtrSprite->SetTextureResource(L"PLAYRULE_TX");
				return true;
			}
		}
		return false;
	}

	void ToTutorialSprite::OnUpdate()
	{
		ChangeSprite();
	}

	//tuika
	//
	//--------------------------------------------------------------------------------------
	//	class HPBAR_MAX : public GameObject;
	//	用途: 配置オブジェクト(HPバーマックス状態)
	//--------------------------------------------------------------------------------------
	//構築と破棄
	HPBAR_MAX::HPBAR_MAX(shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr), m_StartPos(StartPos){
	}
	HPBAR_MAX::~HPBAR_MAX(){}

	//初期化
	void HPBAR_MAX::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(1.50f, 0.30f, 0.750f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"BAR_MAX_TX");

		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_LeftBottomZeroPlusUpY);
		PtrSprite->SetRasterizerState(RasterizerState::CullNone);

		SetAlphaActive(true);
	}

	//--------------------------------------------------------------------------------------
	//	class HPBAR_MID : public GameObject;
	//	用途: 配置オブジェクト(HPバーマックス状態)
	//--------------------------------------------------------------------------------------
	//構築と破棄
	HPBAR_MID::HPBAR_MID(shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr), m_StartPos(StartPos){
	}
	HPBAR_MID::~HPBAR_MID(){}

	//初期化
	void HPBAR_MID::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(1.50f, 0.30f, 0.750f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"BAR_MID_TX");

		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_LeftBottomZeroPlusUpY);
		PtrSprite->SetRasterizerState(RasterizerState::CullNone);

		SetAlphaActive(true);
	}
	//--------------------------------------------------------------------------------------
	//	class HPBAR_LAST : public GameObject;
	//	用途: 配置オブジェクト(HPバーマックス状態)
	//--------------------------------------------------------------------------------------
	//構築と破棄
	HPBAR_LAST::HPBAR_LAST(shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr), m_StartPos(StartPos){
	}
	HPBAR_LAST::~HPBAR_LAST(){}

	//初期化
	void HPBAR_LAST::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(1.50f, 0.30f, 0.750f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"BAR_LAST_TX");

		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_LeftBottomZeroPlusUpY);
		PtrSprite->SetRasterizerState(RasterizerState::CullNone);

		SetAlphaActive(true);
	}

	//--------------------------------------------------------------------------------------
	//	class ThxSprite : public GameObject;
	//	用途: 配置スプライト(Thxスプライト)
	//--------------------------------------------------------------------------------------
	//構築と破棄
	ThxSprite::ThxSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr), m_StartPos(StartPos){
	}
	ThxSprite::~ThxSprite(){}

	//初期化
	void ThxSprite::OnCreate(){

		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(6.0f, 6.0f, 7.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//スプライトをつける
		//auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		m_Alpha = 1;
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1.0f, 1.0f, 1.0f, m_Alpha));
		PtrSprite->SetTextureResource(L"THX_TX");

		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_LeftBottomZeroPlusUpY);

		PtrSprite->SetRasterizerState(RasterizerState::CullNone);

		SetAlphaActive(true);

		

	}

	void ThxSprite::OnUpdate(){
		auto PtrTransform = AddComponent<Transform>();
		
		auto PtrPos = PtrTransform->GetPosition();
		if (PtrPos.y>380)
		{
			PtrPos.y -= 30;
		}
		else if (PtrPos.y <= 380)
		{
			PtrPos.y = 380.0f;
		}
		PtrTransform->SetPosition(PtrPos);
	}

	//--------------------------------------------------------------------------------------
	//	class BlindSprite : public GameObject;
	//	用途: 配置スプライト(Thxスプライト)
	//--------------------------------------------------------------------------------------
	//構築と破棄
	BlindSprite::BlindSprite(shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr), m_StartPos(StartPos){
	}
	BlindSprite::~BlindSprite(){}

	//初期化
	void BlindSprite::OnCreate(){

		auto PtrTransform = AddComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(11.0f, 7.0f, 7.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);

		//スプライトをつける
		auto PtrSprite = AddComponent<PCTSpriteDraw>(Vector2(128.0f, 128.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"Blind_TX");

		PtrSprite->SetSpriteCoordinate(SpriteCoordinate::m_LeftBottomZeroPlusUpY);

		PtrSprite->SetRasterizerState(RasterizerState::CullNone);

		SetAlphaActive(true);


	}
}
//endof  basedx11
