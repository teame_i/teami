#include "stdafx.h"
#include "Project.h"

namespace basedx11
{
	//-----------------------------------------------------------------------
	//  class MotionManager
	//   用途　エネミーの行動関数を管理する。
	//-----------------------------------------------------------------------

	MotionManager::MotionManager(const shared_ptr<Stage>& StagePtr) :
		//MotionManager::MotionManager()
		GameObject(StagePtr)
	{
	}
	MotionManager::~MotionManager(){}


	void MotionManager::CallMotion(int num ,const shared_ptr<GameObject>& obj)
	{
		int MotionCall = num;

		if (MotionCall < 0 || 2 <= MotionCall)
		{
			MotionCall = 0;
		}

		switch (MotionCall)
		{
		case 0:
		{
			WaitMotion(obj);
			break;
		}
		case 1:
		{
			MoveMotion(obj);
			break;
		}
		//case 2:
		//{
		//	//	SonarAttack();
		//	break;
		//}
		}

	}

	////////////////////
	//モーション関数群//
	////////////////////


	//行動関数-待機
	void MotionManager::WaitMotion(const shared_ptr<GameObject>& obj)
	{
		//動き(主に慣性)を止める
		auto SeekPtr = obj->GetComponent<SeekSteering>();
		SeekPtr->SetUpdateActive(false);
		auto Ptr = obj->GetComponent<Rigidbody>();
		Ptr->SetVelocity(0, 0, 0);
	}

	//行動関数-移動
	void MotionManager::MoveMotion(const shared_ptr<GameObject>& obj)
	{
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player", false);
		auto PlayerPos = PlayerPtr->GetComponent<Transform>()->GetPosition();
		auto PtrGravity = obj->GetComponent<Gravity>();
		auto PtrTransform = obj->GetComponent<Transform>();
		auto PtrScale = obj->GetComponent<Transform>()->GetScale();
		//プレイヤーを追いかける
		auto SeekPtr = obj->GetComponent<SeekSteering>();
		PlayerPos.y = PtrGravity->GetBaseY();
		SeekPtr->SetTargetPosition(PlayerPos);
		SeekPtr->SetUpdateActive(true);
	}

	
	//行動関数-その場で音波攻撃
	void MotionManager::SonarAttack()
	{
	//攻撃間隔計測

	AttackWait += ElapsedTime;
	if ((int)AttackWait / 5 == 1){
	//動き(主に慣性)を止める
	auto SeekPtr = GetComponent<SeekSteering>();
	SeekPtr->SetUpdateActive(false);
	auto Ptr = GetComponent<Rigidbody>();
	Ptr->SetVelocity(0, 0, 0);

	//波を出す処理
	auto PtrTrans = GetComponent<Transform>();
	//グループ内に空きがあればそのオブジェクトを再利用
	//そうでなければ新規に作成
	auto Group = GetStage()->GetSharedObjectGroup(L"Wave_Enemy");
	//auto Group = GetStage()->GetSharedObjectGroup(L"Wave");
	auto WaveVec = Group->GetGroupVector();
	//SEの再生
	auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
	pMultiSoundEffect->Start(L"Soner", 0, 0.5f);

	for (auto Ptr : WaveVec){
	//Ptrはweak_ptrなので有効性チェックが必要
	if (!Ptr.expired()){
	auto ShellPtr = dynamic_pointer_cast<Wave_Enemy>(Ptr.lock());
	//auto ShellPtr = dynamic_pointer_cast<Wave>(Ptr.lock());
	if (ShellPtr){
	//UpdateとDrawを行っていない物があるならそれを使う
	if ((!ShellPtr->IsUpdateActive()) && (!ShellPtr->IsDrawActive())){
	ShellPtr->Refresh(PtrTrans->GetPosition());
	m_wave = Ptr.lock();

	return;
	}
	}
	}
	}
	//ここまで来たら空きがなかったことになる
	//波の追加
	auto Sh = GetStage()->AddGameObject<Wave_Enemy>(PtrTrans->GetPosition());
	//auto Sh = GetStage()->AddGameObject<Wave>(PtrTrans->GetPosition());
	m_wave = Sh;
	AttackWait = 0;
	//グループに追加
	Group->IntoGroup(Sh);
	}

	}
	

}