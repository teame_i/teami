#include "stdafx.h"
#include "Project.h"

namespace basedx11{

	//--------------------------------------------------------------------------------------
	//	class Scene : public SceneBase;
	//	用途: シーンクラス
	//--------------------------------------------------------------------------------------
	void Scene::OnCreate(){
		try{
			//最初のアクティブステージの設定
			ResetActiveStage<StareStage>();
			//ResetActiveStage<TitelStage>();
			//ResetActiveStage<GameStage>();
			//ResetActiveStage<TutorialStage>();
			//ResetActiveStage<GameClearStage>();
			//ResetActiveStage<GameOverStage>();

		}
		catch (...){
			throw;
		}
	}

	void Scene::OnEvent(const shared_ptr<Event>& event)
	{	
		if (event->m_MsgStr == L"stareStage"){
			ResetActiveStage<StareStage>();
		}

		if (event->m_MsgStr == L"ToTitle"){
			ResetActiveStage<TitelStage>();
		}
		if (event->m_MsgStr == L"ToMenu"){
			ResetActiveStage<MenuStage>();
		}
		if (event->m_MsgStr == L"ToGame"){

			ResetActiveStage<GameStage>();
		}
		if (event->m_MsgStr == L"ToClear"){
			ResetActiveStage<GameClearStage>();
		}
		if (event->m_MsgStr == L"ToOver"){
			ResetActiveStage<GameOverStage>();

		}		
		if (event->m_MsgStr == L"ToTotriul"){
			ResetActiveStage<TutorialStage>();
		}

	}
}
//end basedx11
