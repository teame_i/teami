#include "stdafx.h"
#include "Project.h"

namespace basedx11{

	//--------------------------------------------------------------------------------------
	//	class Wave : public GameObject;
	//	用途: 波
	//--------------------------------------------------------------------------------------
	//構築と破棄
	Wave::Wave(const shared_ptr<Stage>& StagePtr,
		const Vector3& Position
		) :
		GameObject(StagePtr),
		m_Position(Position),
		m_NowScale(0.1f, 0.25f, 0.1f),
		m_Rotation(20.4f,0.0f,0.0f),
		m_Radius(0.0f),
		m_IsTimer(false)
	{
	}
	Wave::~Wave(){}

	//初期化
	void Wave::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();

		PtrTransform->SetScale(m_NowScale);
		PtrTransform->SetPosition(m_Position);
		PtrTransform->SetRotation(m_Rotation);

		//反発を実装する場合はRigidbodyをつける
		auto PtrRegid = AddComponent<Rigidbody>();

		Timer = 0;

		//透明処理
		SetAlphaActive(true);
	}

	void Wave::Refresh(const Vector3& Position){
		SetUpdateActive(true);
		SetDrawActive(true);
		m_Position = Position;
		//Transform取得
		auto Ptr = GetComponent<Transform>();
		Ptr->SetScale(m_NowScale);
		Ptr->SetPosition(m_Position);
		m_Radius = 0.0f ;
		m_IsTimer = false;
		m_Death = false;
		Timer = 0 ;
	}

	void Wave::OnUpdate(){

		float ElapsedTime = App::GetApp()->GetElapsedTime();
		auto PtrTransform = AddComponent<Transform>();

		//範囲が10以下なら拡大していく
		if (m_Radius <= 10.0f){
			WavePower = 0.5f;
			SetWavePower(WavePower);
			//半径拡大処理
			WaveUpde = true;
			m_Radius += ElapsedTime * 4.0f;
			PtrTransform->SetScale(m_Radius, m_Radius, m_Radius);

		}
		else
		{

			//m_IsTimerがtrueなら生存時間をカウント開始する
			if (m_IsTimer){
				WavePower -= 0.002f;
				SetWavePower(WavePower);
					Timer += ElapsedTime;
					if (WavePower <= 0){
						WavePower = 0;
						SetWavePower(WavePower);

					}
			}
		}
		if (Timer >= 1.5f){
		WaveUpde = false;
		}
		//タイマーが4.0fになったら消す
		if (Timer >= 4.0f){
			SetUpdateActive(false);
			SetDrawActive(false);

		}
		if (m_Death){
				SetUpdateActive(false);
				SetDrawActive(false);
			
		}

	}

	//--------------------------------------------------------------------------------------
	//	class Wave_Enemy : public GameObject;
	//	用途: 敵の波
	//--------------------------------------------------------------------------------------
	//構築と破棄
	Wave_Enemy::Wave_Enemy(const shared_ptr<Stage>& StagePtr,
		const Vector3& Position
		) :
		GameObject(StagePtr),
		m_Position(Position),
		m_NowScale(0.1f, 0.25f, 0.1f),
		m_Rotation(20.4f, 0.0f, 0.0f),
		m_Radius(0.0f),
		m_IsTimer(false)
	{
	}
	Wave_Enemy::~Wave_Enemy(){}

	//初期化
	void Wave_Enemy::OnCreate(){
		auto PtrTransform = AddComponent<Transform>();

		PtrTransform->SetScale(m_NowScale);
		PtrTransform->SetPosition(m_Position);
		PtrTransform->SetRotation(m_Rotation);

		//反発を実装する場合はRigidbodyをつける
		auto PtrRegid = AddComponent<Rigidbody>();

		Timer = 0;

		//透明処理
		SetAlphaActive(true);
	}

	void Wave_Enemy::Refresh(const Vector3& Position){
		SetUpdateActive(true);
		SetDrawActive(true);
		m_Position = Position;
		//Transform取得
		auto Ptr = GetComponent<Transform>();
		Ptr->SetScale(m_NowScale);
		Ptr->SetPosition(m_Position);
		m_Radius = 0.0f;
		m_IsTimer = false;
		Timer = 0;
		//描画コンポーネント
		//auto PtrDraw = GetComponent<PNTStaticDraw>();
		//PtrDraw->SetDiffuse(Color4(1.0f, 1.0f, 0, 1.0f));
	}

	void Wave_Enemy::OnUpdate(){

		float ElapsedTime = App::GetApp()->GetElapsedTime();
		auto PtrTransform = AddComponent<Transform>();

		//PtrTransform->SetScale(m_Radius, m_Radius, m_Radius);
		//範囲が10以下なら拡大していく
		if (m_Radius <= 5.0f){

			//半径拡大処理
			m_Radius += ElapsedTime * 4.0f;
			PtrTransform->SetScale(m_Radius, m_Radius, m_Radius);

		}
		else
		{
			//m_IsTimerがtrueなら生存時間をカウント開始する
			if (m_IsTimer){
				Timer += ElapsedTime;
			}
		}
		//タイマーが4.0fになったら消す
		if (Timer >= 2.0f){
			SetUpdateActive(false);
			SetDrawActive(false);

		}

	}
}
//endof  basedx11